package com.crskdev.rxmvicore.usecase

import com.crskdev.rxmvicore.env.Completed
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.util.simpleName
import io.reactivex.Observable
import io.reactivex.ObservableTransformer

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
interface UseCase {

    fun execute(): ObservableTransformer<in Request, out Response>

    operator fun invoke() = execute()

}

class EchoUseCase(private val response: (Request) -> Response) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer { up -> up.map { response(it) } }
}

abstract class CancelableUseCase : UseCase {

    override fun execute(): ObservableTransformer<Request, Response> =
            ObservableTransformer {
                it.switchMap {
                    if (it.cancel) {
                        Completed()
                                .apply {
                                    src = this@CancelableUseCase.simpleName()
                                }
                                .toObservable()
                    } else {
                        Observable.defer { response(it) }
                    }
                }
            }

    abstract fun response(request: Request): Observable<Response>
}