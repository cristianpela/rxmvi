@file:Suppress("unused", "UNUSED_PARAMETER")

package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.Message
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.Disposable

/**
 * MVIController API operations
 * Created by Cristian Pela on 06.03.2018.
 */
interface MVIStreamOps<S : State> {

    companion object {
        fun <S : State> start(mviStreamOps: MVIStreamOps<S>) = mviStreamOps.apply { start() }
    }

    fun bindIntents(intentStream: Observable<Intent>): Disposable

    fun observeState(): Observable<S>

    fun observeErrors(): Observable<Throwable>

    fun observeMessages(): Observable<Message<*>>

    fun start()

}

@JvmOverloads
fun <S : State> MVIStreamOps<*>.decorate(
        stateDecorator: ObservableTransformer<State, S>,
        errorDecorator: ObservableTransformer<Throwable, Throwable> = ObservableTransformer { up -> up },
        messageDecorator: ObservableTransformer<Message<*>, Message<*>> = ObservableTransformer { up -> up }
): MVIStreamOps<S> {

    return object : MVIStreamOps<S> {

        override fun bindIntents(intentStream: Observable<Intent>): Disposable = this@decorate.bindIntents(intentStream)

        override fun observeErrors(): Observable<Throwable> = this@decorate.observeErrors()

        override fun observeMessages(): Observable<Message<*>> = this@decorate.observeMessages()

        override fun observeState(): Observable<S> = this@decorate.observeState().compose(stateDecorator)

        override fun start() = this@decorate.start()

    }

}