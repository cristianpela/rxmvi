@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.usecase.UseCase
import com.crskdev.rxmvicore.util.Loggy
import com.crskdev.rxmvicore.util.simpleName
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.rxkotlin.cast
import io.reactivex.rxkotlin.ofType
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

/**
 * Core class for the MVI pattern. After creation one must call [MVIController.start]
 * Created by Cristian Pela on 06.03.2018.
 */
abstract class MVIController<S : State> : MVIStreamOps<S> {

    private val intentProxy: Subject<Intent> = PublishSubject.create<Intent>().toSerialized()
    private val stateProxy: Subject<S> = BehaviorSubject.create<S>().toSerialized()
    private val errorProxy: Subject<Throwable> = PublishSubject.create<Throwable>().toSerialized()
    private val messageProxy: Subject<Message<*>> = PublishSubject.create<Message<*>>().toSerialized()


    /**
     * Used when one wants to sync the view state with the controller's accumulated state
     * Example: when we state has a property that can be changed via editText,
     * after some time we need to update the controller acc state with that text, before
     * sending an intent that will change the current accumulated
     */
    protected val syncStateUseCase = object: UseCase {
        override fun execute(): ObservableTransformer<in Request, out Response> =
                ObservableTransformer {
                    it.cast<SyncStateRequest>().map {
                        SyncStateResponse(it.state)<SyncStateResponse>()
                    }
                }

    }

    /**
     * starts the state, error, message streams
     */
    override fun start() {
        val publishCycle = intentProxy.map(intentToRequestMapper())
                .flatMap {
                    if (it is CompositeRequest) {
                        Observable.fromArray(*it.requests)
                    } else {
                        Observable.just(it)
                    }
                }
                .filter { it != NoRequest }
                .publish { Observable.merge(dispatchRequests(it)) }
                //share for a new split
                .publish()

        //start the state stream
        publishCycle.ofType<StatefulResponse>()
                //reduce state from response
                .scan(defaultStateCarrier(), reducer())
                .map { it.getState() }
                //diffed state reduce
                .scan(DiffedState.init<S>(), { acc, curr -> acc.next(curr, diffState()) })
                .skip(1) // skip the initial diffed state
                .map { it.obtain() }
                .subscribe(stateProxy)

        //start the error stream
        publishCycle.ofType<ErrorResponse>()
                .map { it.throwable }
                .subscribe(errorProxy)

        //start the message stream
        publishCycle.ofType<MessageResponse>()
                .map { it.message }
                .subscribe(messageProxy)

        publishCycle.autoConnect(-1) // immediate connection
    }


    private fun dispatchRequests(requestObservable: Observable<Request>): List<Observable<Response>> {
        val dispatcher = Dispatcher(requestObservable)
        dispatcher.useCaseDispatcher()
        return dispatcher.observables
    }

    override fun bindIntents(intentStream: Observable<Intent>): Disposable =
            intentStream.subscribe { intentProxy.onNext(it) }

    override fun observeState(): Observable<S> = stateProxy
            .doOnSubscribe { Loggy.d("MVIController Terminal Client subscribed to controller [state stream]") }

    override fun observeErrors(): Observable<Throwable> = errorProxy
            .doOnSubscribe { Loggy.d("MVIController Terminal Client subscribed to controller [error stream]") }

    override fun observeMessages(): Observable<Message<*>> = messageProxy
            .doOnSubscribe { Loggy.d("MVIController Terminal Client subscribed to controller [message stream]") }

    abstract fun intentToRequestMapper(): Function<Intent, Request>

    abstract fun reducer(): BiFunction<Carrier<S>, StatefulResponse, Carrier<S>>


    abstract fun defaultStateCarrier(): Carrier<S>


    abstract fun Dispatcher.useCaseDispatcher()

    /**
     * The overridden behavior must return state with values that are different than the previous.
     *
     * Otherwise, if one the state value is the same with the correspondent from the new state,
     *
     * the new state value will be null.
     *
     * By default this function is doing nothing, just passing the new state down the stream.
     *
     * You must override it using the above mentioned rules
     */
    //TODO right now this will not work as it should. Must introduce some special intents for first time state
    open fun diffState(): (oldState: S?, newState: S) -> S = { _, newState -> newState }
}


class Dispatcher(@PublishedApi internal val requestObservable: Observable<out Request>) {

    @PublishedApi
    internal val observables: MutableList<Observable<Response>> = mutableListOf()

    inline fun <reified R : Request> req() = R::class.java

    infix fun List<Class<out Request>>.multiInteractWith(useCase: UseCase) {
        Observable
                .merge(this.map { requestObservable.ofType(it) })
                .doOnNext { Loggy.d("MVIController: Request ${it.simpleName()} dispatched on ${Thread.currentThread()}") }
                .compose(useCase.execute())
    }

    infix fun List<Class<out Request>>.multiInteractWithOnce(useCase: UseCase) {
        Observable
                .merge(this.map {
                    requestObservable.ofType(it)
                            .distinctUntilChanged { _, curr -> !curr.cancel }
                })
                .doOnNext { Loggy.d("MVIController: Request ${it.simpleName()} dispatched on ${Thread.currentThread()}") }
                .compose(useCase.execute())
    }

    infix fun Class<out Request>.interactWith(useCase: UseCase) {
        observables.add(
                requestObservable
                        .ofType(this)
                        .doOnNext { Loggy.d("MVIController: Request ${it.simpleName()} dispatched on ${Thread.currentThread()}") }
                        .compose(useCase.execute()))
    }

    infix fun Class<out Request>.interactWithOnce(useCase: UseCase) {
        observables.add(
                requestObservable
                        .ofType(this)
                        .distinctUntilChanged { _, curr -> !curr.cancel }
                        .doOnNext { Loggy.d("MVIController: Request ${it.simpleName()} dispatched on ${Thread.currentThread()}") }
                        .compose(useCase.execute()))
    }

}
