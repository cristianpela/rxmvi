package com.crskdev.rxmvicore.presentation

import java.util.*

/**
 * State and it's carrier
 * Created by Cristian Pela on 05.03.2018.
 */
interface State {
    val status: Status
}

enum class Status {
    IN_FLIGHT, COMPLETED, IDLE
}

interface Action {}
object NONE : Action

class NoState private constructor(override val status: Status) : State {
    constructor() : this(Status.IDLE)
}

@Suppress("MemberVisibilityCanBePrivate")
class DiffedState<S : State?> @PublishedApi internal constructor(
        @PublishedApi internal val oldState: S?,
        private val diffedState: S?) {

    companion object {
        fun <S : State?> init(initialState: S? = null): DiffedState<S> = DiffedState(initialState, initialState)
    }

    inline fun next(state: S, crossinline diff: (oldState: S?, newState: S) -> S): DiffedState<S> {
        val diffed = diff(this.oldState, state)
        return DiffedState(state, diffed)
    }

    fun obtain(): S = diffedState ?: throw IllegalStateException("State must not be NULL!")
}

class Carrier<S : State> private constructor(
        private val state: S,
        private val statusTracker: MutableMap<String, Deque<Status>>,
        private val accumulator: (acc: S, curr: S, correctStatus: Status, action: Action) -> S) {

    companion object {
        fun <S : State> init(state: S,
                             accumulator: (acc: S, curr: S, correctStatus: Status, action: Action) -> S): Carrier<S> =
                Carrier(state, mutableMapOf(), accumulator)
    }

    /**
     * determine next status tracker state -
     * The rule is simple: push only if [State.status] [Status.IN_FLIGHT] and pop when [Status.COMPLETED] and
     * there is an [Status.IN_FLIGHT] on stack from previous tracker state for the same [source]
     * @param source  source of state
     * @param state  current state
     */
    private fun nextStatusTracker(source: String, state: S): MutableMap<String, Deque<Status>> {
        if (statusTracker.containsKey(source)) {
            with(statusTracker[source]!!) {
                peek()?.let {
                    if (it == Status.IN_FLIGHT && state.status == Status.COMPLETED) {
                        pop()
                        Unit
                    } else if (state.status == Status.IN_FLIGHT) {
                        push(state.status)
                    }
                } ?: if (state.status == Status.IN_FLIGHT) statusTracker
                        .apply { push(state.status) }
            }
        } else if (state.status == Status.IN_FLIGHT) {
            statusTracker[source] = ArrayDeque<Status>().apply { push(state.status) }
        }
        return statusTracker
    }

    private fun getStatus(): Status {
        with(statusTracker.values) {
            return if (isEmpty() || map { it.isEmpty() }.reduce { acc, curr -> acc && curr }) {
                Status.IDLE
            } else if (map { it.firstOrNull { it == Status.IN_FLIGHT }?.let { true } == true }
                            .reduce { acc, curr -> acc || curr }) {
                Status.IN_FLIGHT
            } else {
                Status.COMPLETED
            }
        }
    }

    fun getState(): S = state

    fun next(source: String?, newState: S, action: Action = NONE): Carrier<S> {
        val st = nextStatusTracker(source ?: "", newState)
        val status = getStatus()
        return Carrier(accumulator(this.state, newState, status, action), st, accumulator)
    }
}