@file:Suppress("unused")

package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.Message
import io.reactivex.Observable

/**
 * Interface for render the state
 * Created by Cristian Pela on 07.03.2018.
 */
interface IntentRenderer<in S : State> {
    fun renderState(state: S)
    fun renderMessages(message: Message<*>)
    fun renderErrors(err: Throwable)
    fun provideIntentStreams(): List<Observable<Intent>>
    fun onAllowViewsToPrepare()
    fun onAllowSendIntent(intentSender: IntentSender, isFirstTime: Boolean)
    fun onArgumentsPassed(args: Map<String, Any>, isFirstTime: Boolean)
}

interface IntentSender {
    fun sendIntent(intent: Intent)
}