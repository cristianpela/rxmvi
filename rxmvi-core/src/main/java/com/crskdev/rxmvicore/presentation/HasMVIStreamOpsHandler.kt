@file:Suppress("unused")

package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.env.CancelIntent
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.util.Loggy
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject

/**
 *
 * Created by Cristian Pela on 07.03.2018.
 */
class HasMVIStreamOpsHandler<out S : State>(private val observeScheduler: Scheduler) {

    private var intentSender = PublishSubject.create<Intent>()

    private var areStreamsSubscribed = false

    private val pendingIntents = mutableListOf<Intent>()

    private val intentSenderInterface = object : IntentSender {
        override fun sendIntent(intent: Intent) = intentSender.onNext(intent)
    }

    private var disposables = CompositeDisposable()

    fun allowRendererToPrepareViews(intentRenderer: IntentRenderer<S>): Unit = intentRenderer.onAllowViewsToPrepare()// we make sure we have the views prepared before subscribing to them events

    fun subscribeStreams(hasMVIStreamOps: HasMVIStreamOps<*>, intentRenderer: IntentRenderer<S>, arguments: Map<String, Any> = mutableMapOf(), isFirstTime: Boolean) {

        Loggy.d("HasMVIStreamOpsHandler subscribe render streams for $intentRenderer")

        disposables += hasMVIStreamOps.mviStreamOps.observeState()
                .observeOn(observeScheduler).subscribe { state ->
                    @Suppress("UNCHECKED_CAST")
                    intentRenderer.renderState(state as S)
                    //bind intent streams only once after state stream is subscribed
                    if (!areStreamsSubscribed) {
                        disposables += hasMVIStreamOps.mviStreamOps
                                .bindIntents(Observable.merge(
                                        listOf(intentSender) + intentRenderer.provideIntentStreams()))
                        areStreamsSubscribed = true
                        intentRenderer.onAllowSendIntent(intentSenderInterface, isFirstTime)
                        intentRenderer.onArgumentsPassed(arguments, isFirstTime)
                        //send pending intents
                        pendingIntents.forEach { intentSender.onNext(it) }
                        pendingIntents.clear()
                    }
                }

        disposables += hasMVIStreamOps.mviStreamOps.observeMessages()
                .observeOn(observeScheduler).subscribe { message ->
                    intentRenderer.renderMessages(message)
                }

        disposables += hasMVIStreamOps.mviStreamOps.observeErrors()
                .observeOn(observeScheduler).subscribe { err ->
                    intentRenderer.renderErrors(err)
                }
    }

    fun disposeStreams(intentRenderer: IntentRenderer<S>) {
        Loggy.d("HasMVIStreamOpsHandler dispose streams for $intentRenderer")
        // preemptive send a cancel intent, in case the domain presentation want's to process canceling
        // and thus avoiding leaking subscriptions from hot observables or subjects
        intentSender.onNext(CancelIntent)
        areStreamsSubscribed = false
        pendingIntents.clear()
        disposables.clear()
    }

    /**
     * assure that any intent that is trying to be send before onAllowSendIntent will be
     * queued up in pendingIntens
     */
    fun sendIntentMaybePending(intent: Intent) {
        if (areStreamsSubscribed) {
            intentSender.onNext(intent)
        } else {
            pendingIntents.add(intent)
        }
    }


}