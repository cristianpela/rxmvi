package com.crskdev.rxmvicore.presentation

interface HasMVIStreamOps<out Ops : MVIStreamOps<*>> {
    val mviStreamOps: Ops
}
