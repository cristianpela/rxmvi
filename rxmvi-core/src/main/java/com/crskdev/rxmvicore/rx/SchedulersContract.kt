@file:Suppress("unused")

package com.crskdev.rxmvicore.rx

import io.reactivex.Scheduler

/**
 * Adapter for rx - schedulers
 * Created by Cristian Pela on 06.03.2018.
 */
interface SchedulersContract {

    companion object {
        fun create(main: Scheduler, io: Scheduler, computation: Scheduler): SchedulersContract =
                object : SchedulersContract {
                    override fun main(): Scheduler = main

                    override fun io(): Scheduler = io

                    override fun computation(): Scheduler = computation
                }
    }


    fun main(): Scheduler

    fun io(): Scheduler

    fun computation(): Scheduler

}