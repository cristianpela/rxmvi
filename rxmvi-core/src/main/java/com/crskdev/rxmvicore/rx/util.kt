@file:Suppress("unused")

package com.crskdev.rxmvicore.rx

import com.crskdev.rxmvicore.util.Loggy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.functions.Function
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalArgumentException
import java.util.concurrent.TimeUnit

/**
 * Reactive extensions
 * Created by Cristian Pela on 06.03.2018.
 */

fun <T> T.toObservable() = Observable.just(this)
        ?: throw IllegalArgumentException("Must not be null")

// error back - off helpers

/**
 * [based on article](http://kevinmarlow.me/better-networking-with-rxjava-and-retrofit-on-android)
 * @param retries number of retries when an error occurs
 * @param initialDelay initial delay after error occurs before starting the retry
 * @param unit time unit default [TimeUnit.SECONDS]
 * @param skip list of error type for which will no retry
 * @param delayScheduler scheduler of the backoff cycle/timer
 */
fun errorBackOffFlowable(retries: Int = 4, initialDelay: Int = 5, unit: TimeUnit = TimeUnit.SECONDS,
                         skip: List<Class<out Throwable>>,
                         delayScheduler: Scheduler = Schedulers.computation()):
        Function<in Flowable<out Throwable>, Flowable<*>> {

    assert(retries >= 1 && initialDelay >= 1) {
        "Retries and initial delay must be larger than 1. Provided $retries and $initialDelay"
    }

    val backOffCompletedCode = -1

    return Function { errors ->
        errors.zipWith(Flowable.range(1, retries + 1),
                BiFunction<Throwable, Int, Pair<Throwable, Int>> { err, i ->
                    if (i == retries || err.javaClass in skip)
                        err to backOffCompletedCode
                    else err to i
                })
                .flatMap {
                    if (it.second == backOffCompletedCode)
                        Flowable.error(it.first)
                    else {
                        Flowable.timer(delayStrategy(initialDelay, unit, it), unit, delayScheduler)
                    }
                }

    }
}


/**
 * [based on article](http://kevinmarlow.me/better-networking-with-rxjava-and-retrofit-on-android)
 * @param retries number of retries when an error occurs
 * @param initialDelay initial delay after error occurs before starting the retry
 * @param unit time unit default [TimeUnit.SECONDS]
 * @param skip list of error type for which will no retry
 * @param delayScheduler scheduler of the backoff cycle/timer
  */
fun errorBackOffObservable(retries: Int = 4, initialDelay: Int = 5, unit: TimeUnit = TimeUnit.SECONDS,
                           skip: List<Class<out Throwable>>,
                           delayScheduler: Scheduler = Schedulers.computation()):
        Function<in Observable<out Throwable>, Observable<*>> {

    assert(retries >= 1 && initialDelay >= 1) {
        "Retries and initial delay must be larger than 1. Provided $retries and $initialDelay"
    }

    val backOffCompletedCode = -1

    return Function { errors ->
        errors.zipWith(Observable.range(1, retries + 1),
                BiFunction<Throwable, Int, Pair<Throwable, Int>> { err, i ->
                    if (i == retries || err.javaClass in skip)
                        err to backOffCompletedCode
                    else err to i
                })
                .flatMap {
                    if (it.second == backOffCompletedCode)
                        Observable.error(it.first)
                    else {
                        Observable.timer(delayStrategy(initialDelay, unit, it), unit, delayScheduler)
                    }
                }

    }
}

private fun delayStrategy(initialDelay: Int, unit: TimeUnit, pair: Pair<Throwable, Int>): Long =
        (initialDelay * pair.second.toDouble()).toLong().also {
            Loggy.d("Back-Off Retry Delay: $it $unit for ${pair.first}:${pair.first.message}")
        }
