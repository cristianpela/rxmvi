@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package com.crskdev.rxmvicore.util

/**
 * .
 * Created by Cristian Pela on 12.03.2018.
 */
data class Page(val filter: Filterable, val size: Int = 10, val current: Int = 0,
                val extra: Any? = null) {

    init {
        if (current < 0) {
            throw IllegalArgumentException("Current number must be at least 0")
        }
        if (size < 1) {
            throw IllegalArgumentException("Page size must be at least 1")
        }
    }

    fun next(): Page = Page(filter, size, current + 1)

    fun jumpTo(pageNumber: Int): Page =
            if (current < 1) {
                throw IllegalArgumentException("Current number must be at least 1")
            } else
                Page(filter, current = pageNumber)

    fun newFilter(filter: Filterable, size: Int = 10, extra: Any? = null) = Page(filter, size, extra = extra)

    fun totalItems() = current * size

    fun reset(filter: Filterable) =  Page(filter, size)

}

interface Filterable