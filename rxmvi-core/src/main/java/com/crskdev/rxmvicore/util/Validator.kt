package com.crskdev.rxmvicore.util

import com.crskdev.rxmvicore.env.ResourceIDMessage

/**
 * Validates an entity
 * Created by Cristian Pela on 07.03.2018.
 */
abstract class Validator<E> {

    fun <R> check(item: E, either: R? = null): Result<R> {
        val model = model(item)
        return Result(either, model.isValid(), ResourceIDMessage(*model.okMessages.toTypedArray()),
                ResourceIDMessage(*model.errMessages.toTypedArray()))
    }

    abstract fun model(item: E): ValidationModel<E>

    class Result<R>(private val result: R?,
                    private val isValid: Boolean,
                    private val okMessages: ResourceIDMessage,
                    private val errMessages: ResourceIDMessage) {

        fun onPass(action: (okMessages: ResourceIDMessage) -> R): Result<R> {
            val newResult = if (isValid) action(this.okMessages) else result
            return Result(newResult, isValid, okMessages, errMessages)
        }

        fun onFail(action: (errMessages: ResourceIDMessage) -> R): Result<R> {
            val newResult = if (!isValid) action(this.errMessages) else result
            return Result(newResult, isValid, okMessages, errMessages)
        }

        fun get(): R? = result
                ?: throw IllegalStateException("Invalid state. " +
                        "Make sure the unwind result is set.\n" +
                        "This happens when you're not executing callbacks(onFail or onError) " +
                        "or you're calling a callback that is not covering the check case.\n" +
                        "Best way to avoid this error is to provide an either result, " +
                        "or(recommended) call both onFail() and onPass()")

    }

    abstract class ValidationModel<E>(private val item: E) {

        internal val okMessages = mutableListOf<ResourceIDMessage.Res>()

        internal val errMessages = mutableListOf<ResourceIDMessage.Res>()

        abstract fun isValid(): Boolean

        protected fun addOkMessageRes(msg: ResourceIDMessage.Res) = okMessages.add(msg)

        protected fun addErrMessageRes(msg: ResourceIDMessage.Res) = errMessages.add(msg)
    }
}
