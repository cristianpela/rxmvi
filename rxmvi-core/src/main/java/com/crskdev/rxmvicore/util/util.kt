package com.crskdev.rxmvicore.util

/**
 * extension utilities
 * Created by Cristian Pela on 04.03.2018.
 */

fun Any.simpleName(): String = this::class.java.simpleName

/**
 *Favor the [right] item to be picked if it's different than [left].</br>
 * If [acceptNull] flag is set, then the [right] can picked if it is null
 * @param left
 * @param right
 * @param acceptNull
 * @return right item if passes the conditions
 */
fun <T> pickRight(left: T?, right: T?, acceptNull: Boolean = false): T? =
        if (left != right) {
            if (acceptNull && right == null || !acceptNull && right != null) {
                right
            } else {
                left
            }
        } else {
            left
        }

/**
 * Picks the right item if it's different than left, null otherwise
 */
fun <T> pickRightWhenDiff(left: T?, right: T?): T? =
        if (left != right) {
            right
        } else null

fun <T> T.singletonList() = listOf(this)

inline fun Boolean.applyIf(block: Boolean.() -> Unit): Boolean =
        takeIf { this }?.apply(block) ?: this

inline fun <reified V> Any.cast() = this as V