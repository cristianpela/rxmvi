@file:Suppress("unused")

package com.crskdev.rxmvicore.env

import com.crskdev.rxmvicore.rx.toObservable
import io.reactivex.Completable

/**
 * A wrapper around a POJO. Also it must have a source indicator
 * Created by Cristian Pela on 04.03.2018.
 */
abstract class Response {

    @PublishedApi
    internal var src: String? = null

    inline operator fun <reified T> invoke(): Response {
        src = T::class.simpleName
        return this
    }

    fun source(): String? = src

}

abstract class StatefulResponse : Response()

class InFlight : StatefulResponse()

class Completed : StatefulResponse()


open class MessageResponse(val message: Message<*>) : Response() {
    companion object {
        fun ok(): MessageResponse = MessageResponse(OKMessage)
        fun withId(id: Int, vararg args: Any): MessageResponse = MessageResponse(ResourceIDMessage.of(id, *args))
        fun withIds(array: Array<ResourceIDMessage.Res>): MessageResponse = MessageResponse(ResourceIDMessage(*array))
    }
}

inline fun <reified T> Completable.ok(message: ResourceIDMessage? = null) =
        this.andThen((message?.let { MessageResponse(message).apply { src = T::class.toString() } }
                ?: MessageResponse.ok().apply { src = T::class.toString() }).toObservable())!!

inline fun <reified T> Completable.ok(id: Int, vararg args: Any) =
        this.andThen(MessageResponse.withId(id, *args).apply { src = T::class.toString() }.toObservable())

inline fun <reified T> Completable.complete() = this.andThen(Completed()
        .apply { src = T::class.toString() }.toObservable())!!

open class ErrorResponse(val throwable: Throwable) : Response() {
    companion object {
        fun withMessageId(msg: ResourceIDMessage) = ErrorResponse(ResourceIDError(msg))
        fun withId(id: Int, vararg args: Any) = ErrorResponse(ResourceIDError(id, *args))
        fun withIds(array: Array<ResourceIDMessage.Res>) = ErrorResponse(ResourceIDError(ResourceIDMessage(*array)))
    }
}




