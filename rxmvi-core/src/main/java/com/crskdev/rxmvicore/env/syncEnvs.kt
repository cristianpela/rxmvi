package com.crskdev.rxmvicore.env

/**
 * Created by Cristian Pela on 02.04.2018.
 */
class SyncStateIntent(val state: Any) : Intent()
class SyncStateRequest(val state: Any) : Request(cancel = false)
class SyncStateResponse(val state: Any) : StatefulResponse()