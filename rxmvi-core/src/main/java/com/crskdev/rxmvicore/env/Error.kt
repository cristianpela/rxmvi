@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package com.crskdev.rxmvicore.env

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
class ResourceIDError(wrapped: Throwable, val idMessage: ResourceIDMessage) :
        Throwable(wrapped) {
    constructor(idMessage: ResourceIDMessage) : this(NoStackTraceError, idMessage)
    constructor(resourceId: Int, vararg args: Any) : this(NoStackTraceError, ResourceIDMessage.of(resourceId, args))
    constructor(wrapped: Throwable, resourceId: Int, vararg args: Any) : this(wrapped, ResourceIDMessage.of(resourceId, args))
}

object NoStackTraceError : Throwable()