package com.crskdev.rxmvicore.env

/**
 * Wrapper for messages type objects
 * Created by Cristian Pela on 06.03.2018.
 */
open class Message<out D>(val data: D)

object OKMessage : Message<Unit>(Unit)

//class ResourceIDMessage(val id: Int, vararg val args: Any) : Message<Int>(id)

open class ResourceIDMessage(vararg resources: Res) : Message<Array<out ResourceIDMessage.Res>>(resources) {

    companion object {
        fun of(id: Int, vararg args: Any) = ResourceIDMessage(Res(id, args))
    }

    class Res(val id: Int, vararg val args: Any)
}

class StringMessage(val content: String) : Message<String>(content)