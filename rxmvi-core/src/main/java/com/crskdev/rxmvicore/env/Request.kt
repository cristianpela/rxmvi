@file:Suppress("unused")

package com.crskdev.rxmvicore.env

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
abstract class Request(val cancel: Boolean = false)

object NoRequest: Request(false)

class CompositeRequest(vararg val requests: Request): Request(false)