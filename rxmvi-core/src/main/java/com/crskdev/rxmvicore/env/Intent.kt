@file:Suppress("unused")

package com.crskdev.rxmvicore.env

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
abstract class Intent(val cancel: Boolean = false)

object CancelIntent : Intent(true)

object NoIntent: Intent()