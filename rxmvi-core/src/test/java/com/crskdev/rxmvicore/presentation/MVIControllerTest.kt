package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.presentation.MVIControllerTest.Companion.message
import com.crskdev.rxmvicore.presentation.Status.*
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.usecase.UseCase
import com.crskdev.rxmvicore.util.assertEq
import com.crskdev.rxmvicore.util.pickRight
import com.crskdev.rxmvicore.util.pickRightWhenDiff
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import io.reactivex.rxkotlin.cast
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 06.03.2018.
 */
class MVIControllerTest : Spek({

    describe("testing the mviStreamOps mvi stream") {

        lateinit var useCase: DummyUseCase
        lateinit var controller: MVIController<DummyState>

        it("should have the correct state reduced") {
            useCase = DummyUseCase(InFlight()<DummyUseCase>(),
                    DummyResponse("a")<DummyUseCase>(),
                    DummyErrorResponse(err)<DummyUseCase>(),
                    DummyMessageResponse()<DummyUseCase>())
            controller = DummyMVIController(useCase).apply { start() }

            val test = Observable.merge(
                    controller.observeState(),
                    controller.observeMessages(),
                    controller.observeErrors()
            ).test()
            controller.bindIntents(DummyIntent().toObservable())

            test.values().assertEq(
                    listOf(
                            DummyState(IDLE),
                            DummyState(IN_FLIGHT),
                            DummyState(IDLE, "a"),
                            err,
                            message
                    )
            )
        }

        it("should have state stream diffed states") {
            useCase = DummyUseCase(
                    InFlight()<DummyUseCase>()
                    ,DummyResponse("a")<DummyUseCase>()
                    ,DummyResponse("a")<DummyUseCase>()
                    ,DummyResponse("b")<DummyUseCase>()
                    ,DummyResponse("b")<DummyUseCase>()
                    ,DummyResponse("c")<DummyUseCase>()
            )
            controller = DiffedDummyMVIController(useCase ).apply { start() }

            val test = Observable.merge(
                    controller.observeState(),
                    controller.observeMessages(),
                    controller.observeErrors()
            ).test()
            controller.bindIntents(DummyIntent().toObservable())

            test.values().assertEq(
                    listOf(
                            DummyState(IDLE, null)
                            ,DummyState(IN_FLIGHT, null)
                            ,DummyState(IDLE, "a")
                            ,DummyState(IDLE, null)
                            ,DummyState(IDLE, "b")
                            ,DummyState(IDLE, null)
                            ,DummyState(IDLE, "c")
                    ))
        }
    }
}) {
    companion object {
        val err = Error("Error")
        val message = Message("Hello")
    }
}


open class DummyMVIController(private val useCase: DummyUseCase) : MVIController<DummyState>() {

    override fun Dispatcher.useCaseDispatcher() {
        req<DummyRequest>() interactWith useCase
    }

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is DummyIntent -> DummyRequest()
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<Carrier<DummyState>, StatefulResponse, Carrier<DummyState>> =
            BiFunction { carrier, response ->
                when (response) {
                    is InFlight -> carrier.next(response.source(), DummyState(IN_FLIGHT), NONE)
                    is Completed -> carrier.next(response.source(), DummyState(COMPLETED), NONE)
                    is DummyResponse -> carrier.next(response.source(), DummyState(COMPLETED, response.data), NONE)
                    else -> carrier
                }
            }

    override fun defaultStateCarrier(): Carrier<DummyState> =
            Carrier.init(DummyState(IDLE)) { acc, curr, status, _ ->
                DummyState(status, pickRight(acc.data, curr.data))
            }

//    override fun useCaseDispatcher(requestObservable: Observable<Request>): List<Observable<Response>> =
//            dispatchRequest<DummyRequest>(requestObservable, useCase).singletonList()

}

class DiffedDummyMVIController(useCase: DummyUseCase) :
        DummyMVIController(useCase) {

    override fun diffState(): (oldState: DummyState?, newState: DummyState) -> DummyState = { old, new ->
        DummyState(new.status, pickRightWhenDiff(old?.data, new.data))
    }
}


class DummyUseCase(private vararg val responses: Response) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<DummyRequest>().switchMap {
                    Observable.fromArray(*responses)
                }
            }
}

class DummyMessageResponse : MessageResponse(message)
class DummyErrorResponse(throwable: Throwable) : ErrorResponse(throwable)
class DummyResponse(val data: String) : StatefulResponse()
class DummyIntent : Intent(false)
class DummyRequest : Request(false)
