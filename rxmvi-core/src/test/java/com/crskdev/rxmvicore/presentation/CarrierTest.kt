package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.presentation.Status.*
import com.crskdev.rxmvicore.util.assertEq
import com.crskdev.rxmvicore.util.pickRight
import io.reactivex.Observable
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 05.03.2018.
 */
class CarrierTest : Spek({

    describe("cycle of carrier status") {

        val src1 = "Src1"
        val src2 = "Src2"
        lateinit var carrier: Carrier<DummyState>

        beforeEachTest {
            carrier = Carrier.init(DummyState(IDLE)) { acc, curr, correctStatus, _ ->
                DummyState(correctStatus, pickRight(acc.data, curr.data))
            }
        }
        it("should return state based on different source statuses over cycles") {
            carrier.next(src1, DummyState(IN_FLIGHT))
                    .next(src2, DummyState(IN_FLIGHT))
                    .next(src1, DummyState(COMPLETED, "a"))
                    .next(src2, DummyState(COMPLETED, "b"))
                    .next(src2, DummyState(COMPLETED, "c"))
                    .getState()
                    .assertEq(DummyState(IDLE, "c"))
        }
        it("should return idle if passing completed and is no in flight") {
            carrier.next(src1, DummyState(COMPLETED)).getState().assertEq(DummyState(IDLE))
        }
        it("should return state based on different source statuses over cycles in rx context") {
            Observable
                    .fromArray(
                            src1 to DummyState(IN_FLIGHT),
                            src2 to DummyState(IN_FLIGHT),
                            src1 to DummyState(COMPLETED, "a"),
                            src2 to DummyState(COMPLETED, "b"),
                            src2 to DummyState(COMPLETED, "c")
                    )
                    .scan(carrier) { acc, curr ->
                        acc.next(curr.first, curr.second)
                    }
                    .map { it.getState() }
                    .test()
                    .values()
                    .assertEq(listOf(
                            DummyState(IDLE),
                            DummyState(IN_FLIGHT),
                            DummyState(IN_FLIGHT),
                            DummyState(IN_FLIGHT, "a"),
                            DummyState(IDLE, "b"),
                            DummyState(IDLE, "c")
                    ))
        }
    }


})

data class DummyState(
        override val status: Status,
        val data: String? = null
) : State