package com.crskdev.rxmvicore.presentation

import com.crskdev.rxmvicore.presentation.Status.*
import com.crskdev.rxmvicore.util.assertEq
import com.crskdev.rxmvicore.util.pickRightWhenDiff
import io.reactivex.Observable
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 07.03.2018.
 */
class DiffedStateTest : Spek({
    describe("diffed state testing") {
        it("should return correct diffed state at the end") {
            DiffedState.init(DummyState(IDLE))
                    .next(DummyState(IDLE, "a")) { old, new ->
                        DummyState(new.status, pickRightWhenDiff(old?.data, new.data))
                    }
                    .obtain()
                    .assertEq(DummyState(IDLE, "a"))
        }
        it("should return null diffed state at the end when same state is applied") {
            DiffedState.init(DummyState(IDLE, "a"))
                    .next(DummyState(IDLE, "a")) { old, new ->
                        DummyState(new.status, pickRightWhenDiff(old?.data, new.data))
                    }
                    .obtain()
                    .assertEq(DummyState(IDLE, null))
        }
        it("should return correct diffed state in rx context") {
            Observable.fromArray(
                    DummyState(IDLE, "a"),
                    DummyState(IN_FLIGHT, "a"),
                    DummyState(COMPLETED, "b"),
                    DummyState(IDLE, "c"),
                    DummyState(IDLE, "c"),
                    DummyState(IDLE, "d")
            ).scan(DiffedState.init<DummyState>(), { acc, curr ->
                acc.next(curr) { old, new ->
                    DummyState(new.status, pickRightWhenDiff(old?.data, new.data))
                }
            }).skip(1)
                    .map { it.obtain() }
                    .test()
                    .assertNoErrors()
                    .values()
                    .assertEq(listOf(
                            DummyState(IDLE, "a"),
                            DummyState(IN_FLIGHT, null),
                            DummyState(COMPLETED, "b"),
                            DummyState(IDLE, "c"),
                            DummyState(IDLE, null),
                            DummyState(IDLE, "d")
                    ))
        }
    }
})