package com.crskdev.rxmvicore

import com.crskdev.rxmvicore.util.assertEq
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class ExampleUnitTest: Spek({
   describe("example spek test"){
       it("should add correct"){
           (2 + 2).assertEq(4)
       }
   }
})