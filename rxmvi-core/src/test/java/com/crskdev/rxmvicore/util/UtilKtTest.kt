package com.crskdev.rxmvicore.util

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 06.03.2018.
 */
class UtilKtTest: Spek( {
   describe("utility tests"){
       it("should pick the correct item"){
           pickRight(1, 2).assertEq(2)
           pickRight(1, null).assertEq(1)
           pickRight(1, null, true).assertEq(null)
           pickRight(null, null).assertEq(null)
           pickRight(1, 1).assertEq(1)
       }
       it("should pick the correct item when diffed"){
           pickRightWhenDiff(null, null).assertEq(null)
           pickRightWhenDiff(null, 2).assertEq(2)
           pickRightWhenDiff(1, 2).assertEq(2)
           pickRightWhenDiff(1, 1).assertEq(null)
       }
   }

})