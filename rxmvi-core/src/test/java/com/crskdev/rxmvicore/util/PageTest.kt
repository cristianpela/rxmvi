package com.crskdev.rxmvicore.util

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.Test

import org.junit.Assert.*
import kotlin.test.assertFailsWith

/**
 * Created by Cristian Pela on 19.04.2018.
 */
class PageTest : Spek({
    describe("page testing") {
        it("should give the right total items paged until now") {
            Page(MockFilter1, 10).next().next()
                    .apply { totalItems().assertEq(2 * 10) }
                    .newFilter(MockFilter2)
                    .next()
                    .apply {
                        totalItems().assertEq(10)
                        filter.assertEq(MockFilter2)
                    }
        }
        it("should fail if page number is lesser than 0"){
            assertFailsWith<IllegalArgumentException> {
                Page(MockFilter1, 10, -1)
            }
        }
        it("should fail if page size is lesser than 1"){
            assertFailsWith<IllegalArgumentException> {
                Page(MockFilter1, 0)
            }
        }
    }
})

object MockFilter1 : Filterable
object MockFilter2 : Filterable