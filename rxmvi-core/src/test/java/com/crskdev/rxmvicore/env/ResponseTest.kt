package com.crskdev.rxmvicore.env

import com.crskdev.rxmvicore.util.assertEq
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.Test

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
class ResponseTest : Spek({

    describe("basic source description test") {
        it("should have simple name for provided source when response is generic") {
            object : Response() {}<Src>().src.assertEq("Src")
        }
        it("should have simple name when it deals withn in flight, completed response type") {
            InFlight()<Src>().src.assertEq("Src")
            Completed()<Src>().src.assertEq("Src")
        }
    }
})

class Src