package com.crskdev.rxmvicore.usecase

import com.crskdev.rxmvicore.env.Completed
import com.crskdev.rxmvicore.env.InFlight
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.util.TestSchedulers
import com.crskdev.rxmvicore.util.assertEq
import com.crskdev.rxmvicore.util.simpleName
import io.reactivex.Observable
import io.reactivex.Scheduler
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
class CancelableUseCaseTest : Spek({

    describe("unsubscribe the inner stream when a cancel request is dispatched ") {

        val (requestScheduler) = TestSchedulers()
        val useCase = DummyCancelableUseCase(requestScheduler)
        val test = Observable
                .just<Request>(StopRequest)
                .delay(4, TimeUnit.SECONDS, requestScheduler)
                .startWith(StartRequest)
                .compose(useCase.execute())
                .test()

        it("should cancel the stream have an in flight and completed flag along the streamed values") {
            requestScheduler.advanceTimeBy(4, TimeUnit.SECONDS)
            test.values()
                    .map {
                        when (it) {
                            is InFlight,
                            is Completed -> -1L
                            is DummyResponse -> it.data
                            else -> throw Exception()
                        }
                    }
                    .assertEq(listOf(-1L, 0L, 1L, 2L, -1L))
        }

        it("should have the same source tag") {
            requestScheduler.advanceTimeBy(4, TimeUnit.SECONDS)
            test.values()
                    .map { it.source() }
                    .reduce { acc, curr -> acc + curr }
                    .assertEq(useCase.simpleName().repeat(5))
        }
    }


})

object StartRequest : Request(false)
object StopRequest : Request(true)

class DummyCancelableUseCase(private val scheduler: Scheduler) : CancelableUseCase() {

    override fun response(request: Request): Observable<Response> =
            Observable.interval(1, TimeUnit.SECONDS, scheduler)
                    .map { DummyResponse(it)<DummyCancelableUseCase>() }
                    .startWith(InFlight()<DummyCancelableUseCase>())

}

data class DummyResponse(val data: Long) : Response()