package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.env.FetchTODOsResponse
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.CancelableUseCase
import com.crskdev.rxmvicore.util.Page
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Cristian Pela on 27.03.2018.
 */
class WatchChangeFilterUseCase
@Inject constructor(
        @RepoProxy
        private val repository: TODORepository,
        private val filterPersistor: TODOFilterPersistor) : CancelableUseCase() {

    override fun response(request: Request): Observable<Response> =
            filterPersistor
                    .observerFilterChanges(TODOFilter.NOT_COMPLETED)
                    .switchMap { f ->
                        repository.getAll(f, true)
                                .map { FetchTODOsResponse(f, it)<WatchChangeFilterUseCase>() }
                                .toObservable()
                    }
}