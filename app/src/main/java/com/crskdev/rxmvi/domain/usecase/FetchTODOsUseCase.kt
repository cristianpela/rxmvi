package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.PAGE_FILTER_UNKNOWN
import com.crskdev.rxmvi.domain.RES_MSG_FETCH_EMPTY_TODOS
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.env.FetchTODOsRequest
import com.crskdev.rxmvi.domain.env.FetchTODOsResponse
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.MessageResponse
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.usecase.UseCase
import com.crskdev.rxmvicore.util.Page
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Simple fetch interactor
 * Created by Cristian Pela on 08.03.2018.
 */
class FetchTODOsUseCase
@Inject constructor(
        private val filterPersistor: TODOFilterPersistor,
        @RepoProxy private val repository: TODORepository
        /*,private val resourceIDLocator: ResourceIDLocator*/) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<FetchTODOsRequest>()
                        .switchMap { req ->
                            if (req.filter == TODOFilter.UNKNOWN) {
                                filterPersistor
                                        .load(TODOFilter.ALL)
                                        .map { FetchTODOsRequest(it, req.fromStart) }
                                        .toObservable()
                                        .compose(getAllTransform())
                            } else {
                                getAll(req.filter, req.fromStart)
                            }
                        }
            }


    private fun getAllTransform(): ObservableTransformer<FetchTODOsRequest, out Response> =
            ObservableTransformer {
                it.switchMap { getAll(it.filter, it.fromStart) }
            }

    private fun getAll(filter: TODOFilter, fromStart: Boolean): Observable<out Response> =
            repository.getAll(filter, fromStart)
                    .map { FetchTODOsResponse(filter, it)<FetchTODOsUseCase>() }
                    .toObservable()
    // meh no more message if there no to-dos
//                    .switchIfEmpty(MessageResponse.withId(
//                            resourceIDLocator.lookUp(RES_MSG_FETCH_EMPTY_TODOS))
//                            .toObservable())
}
