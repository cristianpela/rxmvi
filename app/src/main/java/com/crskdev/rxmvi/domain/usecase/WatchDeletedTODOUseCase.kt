package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.WatchTODODeletedResponse
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.CancelableUseCase
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Cristian Pela on 16.04.2018.
 */
class WatchDeletedTODOUseCase
@Inject constructor(@RepoProxy private val repository: TODORepository) : CancelableUseCase() {

    override fun response(request: Request): Observable<Response> =
            repository.observeDeleted().map {
                WatchTODODeletedResponse(it)<WatchDeletedTODOUseCase>()
            }
}