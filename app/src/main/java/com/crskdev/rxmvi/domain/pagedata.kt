package com.crskdev.rxmvi.domain

import com.crskdev.rxmvicore.util.Filterable
import com.crskdev.rxmvicore.util.Page

/**
 * .
 * Created by Cristian Pela on 12.03.2018.
 */
enum class TODOFilter: Filterable {
    UNKNOWN,
    ALL,
    COMPLETED,
    NOT_COMPLETED,
    ABOUT_DUE,
    PASSED_DUE,
    NO_DUE
}

const val PAGE_SIZE: Int = 10
val PAGE_FILTER_UNKNOWN: Page = Page(TODOFilter.UNKNOWN, PAGE_SIZE)