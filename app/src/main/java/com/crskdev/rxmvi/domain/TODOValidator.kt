package com.crskdev.rxmvi.domain

import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.platform.TimeProvider
import com.crskdev.rxmvi.util.isEmptyOrBlank
import com.crskdev.rxmvicore.env.ResourceIDMessage.Res
import com.crskdev.rxmvicore.util.Validator
import com.crskdev.rxmvicore.util.applyIf
import javax.inject.Inject

/**
 * Validates a [TODOEntity]
 * Created by Cristian Pela on 08.03.2018.
 */
class TODOValidator
@Inject constructor(private val resLocator: ResourceIDLocator,
                    private val timeProvider: TimeProvider) : Validator<TODOEntity>() {


    override fun model(item: TODOEntity): ValidationModel<TODOEntity> =
            object : ValidationModel<TODOEntity>(item) {
                override fun isValid(): Boolean {

                    //TODO chaining is bugged
//                        with(item) {
//                            author.isEmptyOrBlank().applyIf {
//                                addErrMessageRes(Res(resLocator.lookUp(RES_ERR_EMPTY_AUTHOR)))
//                            }.or(title.isEmptyOrBlank().applyIf {
//                                addErrMessageRes(Res(resLocator.lookUp(RES_ERR_EMPTY_TITLE)))
//                            }).or(dueDate != null && dueDate <= 0).applyIf {
//                                addErrMessageRes(Res(resLocator.lookUp(RES_ERR_DUE_DATE_INVALID)))
//                            }.or((dueDate != null && timeProvider.currentTimeMillis() >= dueDate && dueDate > 0).applyIf {
//                                addErrMessageRes(Res(resLocator.lookUp(RES_ERR_DUE_DATE_BEFORE_NOW)))
//                            }).not().applyIf {
//                                addOkMessageRes(Res(resLocator.lookUp(RES_OK)))
//                            }
//                        }


                    var isValid = true
                    with(item) {
                        if (author.isEmptyOrBlank()) {
                            addErrMessageRes(Res(resLocator.lookUp(RES_ERR_EMPTY_AUTHOR)))
                            isValid = false
                        }
                        if (title.toString().isEmptyOrBlank()) {
                            addErrMessageRes(Res(resLocator.lookUp(RES_ERR_EMPTY_TITLE)))
                            isValid = false
                        }
                        if (dueDate != null) {
                            if (dueDate <= 0L) {
                                addErrMessageRes(Res(resLocator.lookUp(RES_ERR_DUE_DATE_INVALID)))
                                isValid = false
                            }else if(timeProvider.currentTimeMillis() >= dueDate){
                                addErrMessageRes(Res(resLocator.lookUp(RES_ERR_DUE_DATE_BEFORE_NOW)))
                                isValid = false
                            }

                        }
                    }

                    if(isValid){
                        addOkMessageRes(Res(resLocator.lookUp(RES_OK)))
                    }

                    return isValid
                }
            }

}