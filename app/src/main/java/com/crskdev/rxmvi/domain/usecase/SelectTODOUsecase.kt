package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_ERR_FETCH_EMPTY_SINGLE_TODO
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.env.SelectTODORequest
import com.crskdev.rxmvi.domain.env.SelectTODOResponse
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.ErrorResponse
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
class SelectTODOUsecase
@Inject constructor(
        @RepoProxy private val repository: TODORepository,
        private val resourceIDLocator: ResourceIDLocator
) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<SelectTODORequest>().switchMap {
                    repository.getOne(it.id)
                            .map { SelectTODOResponse(it)<SelectTODOUsecase>() }
                            .toObservable()
                            .onErrorReturnItem(ErrorResponse.withId(resourceIDLocator
                                    .lookUp(RES_ERR_FETCH_EMPTY_SINGLE_TODO)))
                }
            }
}