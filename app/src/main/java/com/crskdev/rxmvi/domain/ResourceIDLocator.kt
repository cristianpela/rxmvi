package com.crskdev.rxmvi.domain

/**
 * maps domain agnostic resource ids to the the framework real resource ids
 * Created by Cristian Pela on 08.03.2018.
 */
class ResourceIDLocator(private val resources: Map<ResInt, Int>) {

    companion object {
        const val NO_RESOURCE: ResInt = -1
    }

    fun lookUp(domainResourceID: ResInt) =
            resources[domainResourceID] ?: NO_RESOURCE

}