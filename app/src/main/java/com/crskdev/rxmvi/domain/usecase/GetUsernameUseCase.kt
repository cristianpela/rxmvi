package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_ERR_USERNAME_NOT_FOUND
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.env.GetUsernameRequest
import com.crskdev.rxmvi.domain.env.GetUsernameResponse
import com.crskdev.rxmvi.domain.gateway.UserService
import com.crskdev.rxmvicore.env.ErrorResponse
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Created by Cristian Pela on 27.03.2018.
 */
class GetUsernameUseCase
@Inject constructor(private val userService: UserService,
                    private val resourceIDLocator: ResourceIDLocator) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<GetUsernameRequest>().switchMap {
                    userService.getUser()
                            .toObservable()
                            .map { GetUsernameResponse(it) as Response }
                            .onErrorReturnItem(ErrorResponse
                                    .withId(resourceIDLocator.lookUp(RES_ERR_USERNAME_NOT_FOUND)))
                }
            }
}