package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.CompleteTODORequest
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.env.complete
import com.crskdev.rxmvicore.env.ok
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 14.03.2018.
 */
class CompleteTODOUseCase
@Inject constructor(@RepoProxy private val repository: TODORepository) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<CompleteTODORequest>().flatMap {
                    repository.complete(it.id, it.complete).complete<CompleteTODOUseCase>()
                }
            }
}