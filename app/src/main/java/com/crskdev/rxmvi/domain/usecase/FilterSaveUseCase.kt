package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.SaveFilterRequest
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.env.ok
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Saves a filter after session is over
 * Created by Cristian Pela on 13.03.2018.
 */
class FilterSaveUseCase
@Inject constructor(private val filterPersistor: TODOFilterPersistor): UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<SaveFilterRequest>().switchMap {
                    filterPersistor.save(it.filter).ok<CompleteTODOUseCase>()
                }
            }
}