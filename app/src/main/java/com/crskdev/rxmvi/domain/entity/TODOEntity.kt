package com.crskdev.rxmvi.domain.entity

/**
 * .
 * Created by Cristian Pela on 07.03.2018.
 */
data class TODOEntity(
        val id: Long = NO_ID,
        val creationDate: Long,
        val dueDate: Long? = null,
        val author: String,
        val title: CharSequence,
        val detail: CharSequence? = null,
        val completed: Boolean = false) {
    companion object {
        const val NO_ID = -1L
        val EMPTY = TODOEntity(NO_ID, -1, null, "", "", null)
    }
}