@file:Suppress("unused")

package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_MSG_TODO_ADD
import com.crskdev.rxmvi.domain.RES_MSG_TODO_UPDATE
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.TODOValidator
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.env.UpsertTODORequest
import com.crskdev.rxmvi.domain.env.SelectTODOResponse
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvi.presentation.upsert.Mode
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 07.03.2018.
 */
class UpsertTODOUseCase
@Inject constructor(@RepoProxy private val repository: TODORepository,
                    private val validator: TODOValidator,
                    private val resourceIDLocator: ResourceIDLocator) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<UpsertTODORequest>().flatMap { req ->
                    validator.check<Observable<out Response>>(req.todo)
                            .onPass {
                                repository.upsert(req.todo).flatMapObservable {
                                    val returnTodo = if (req.mode == Mode.ADD)
                                        //keep the author though
                                        TODOEntity.EMPTY.copy(author = req.todo.author)
                                    else
                                        req.todo.copy(id = it)
                                    val okMessage = if (req.mode == Mode.ADD)
                                        MessageResponse.withId(resourceIDLocator.lookUp(RES_MSG_TODO_ADD))
                                    else
                                        MessageResponse.withId(resourceIDLocator.lookUp(RES_MSG_TODO_UPDATE))
                                    Observable
                                            .fromArray(
                                                    SelectTODOResponse(returnTodo)<UpsertTODOUseCase>(), okMessage)
                                }
                            }
                            .onFail {
                                ErrorResponse.withMessageId(it).toObservable()
                            }.get()
                }
            }

}