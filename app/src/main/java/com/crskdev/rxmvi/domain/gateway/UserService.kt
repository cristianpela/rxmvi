package com.crskdev.rxmvi.domain.gateway

import io.reactivex.Completable
import io.reactivex.Single

interface UserService {

    companion object {
        val ERROR = Error("Mobile account not found")
    }

    fun getUser(): Single<String>

    fun saveUser(username: String): Completable

}
