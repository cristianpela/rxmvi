package com.crskdev.rxmvi.domain.gateway

import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvicore.util.Page
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 07.03.2018.
 */
interface TODORepository {

    companion object {
        val WITHIN_DUE_TIME: Long = TimeUnit.HOURS.toMillis(24)
    }

    /**
     * retrieves a HEADER(w/out description) list of TODOEntities
     */
    fun getAll(filter: TODOFilter, fromStart: Boolean, size: Int = 10, includeDetail: Boolean = false): Single<List<TODOEntity>>

    /**
     * get a FULL TodoEntity
     */
    fun getOne(id: Long): Single<TODOEntity>

    /**
     * upsert or update a to-do
     */
    fun upsert(todo: TODOEntity): Single<Long>

    /**
     * Completes a to=do with id.
     */
    fun complete(id: Long, complete: Boolean): Completable

    /**
     * deletes a to-do
     */
    fun delete(id: Long): Single<Long>

    /**
     * observes changes in the to-do database. Emits the latest changed to-do
     */
    fun observe(): Observable<TODOEntity>

    /**
     * observes deleted to-do ids
     */
    fun observeDeleted(): Observable<List<Long>>

    /**
     * observes changes in the to-do database. Emits the aggregation results for each filter
     */
    fun observeAggregator(): Observable<EnumMap<TODOFilter, Int>>

    /**
     * Emits the aggregation results for each filter
     */
    fun getAggregation(): Single<EnumMap<TODOFilter, Int>>

    /**
     * searches a certain text in title = search is case ignored
     */
    fun search(textLike: String, fromStart: Boolean): Single<List<TODOEntity>>

    /**
     * observe changes done to an item from search to-do list.
     *
     * For example when the item title is changed and the search term is not in the title
     * anymore, the item will be removed from list
     */
    fun observeChangesInSearch(textLike: String): Observable<TODOEntity>

}


interface ClearableTODORepository : TODORepository {

    fun clear()

    fun refresh(data: List<TODOEntity>)

}