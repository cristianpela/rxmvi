package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.ChangeFilterRequest
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.env.complete
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Created by Cristian Pela on 27.03.2018.
 */
class ChangeFilterUseCase
@Inject constructor(private val filterPersistor: TODOFilterPersistor) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<ChangeFilterRequest>()
                        .switchMap {
                            filterPersistor
                                    .save(it.filter)
                                    .complete<ChangeFilterUseCase>()
                        }
            }

}