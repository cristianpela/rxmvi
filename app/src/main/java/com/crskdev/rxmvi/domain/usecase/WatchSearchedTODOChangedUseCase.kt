package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.WatchSearchTODORequest
import com.crskdev.rxmvi.domain.env.WatchTODOResponse
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.CancelableUseCase
import com.crskdev.rxmvicore.util.cast
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Cristian Pela on 20.04.2018.
 */
class WatchSearchedTODOChangedUseCase
@Inject constructor(@RepoProxy private val repository: TODORepository) : CancelableUseCase() {

    override fun response(request: Request): Observable<Response> {
        val r = request.cast<WatchSearchTODORequest>()
        return repository
                .observeChangesInSearch(r.textLike)
                .map { WatchTODOResponse(it)<WatchDeletedTODOUseCase>() }
    }

}