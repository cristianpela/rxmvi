package com.crskdev.rxmvi.domain

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Cristian Pela on 01.04.2018.
 */
@Singleton
class DateFormatter(format: String) {

    @Inject constructor() : this(FORMAT)

    companion object {
        const val FORMAT: String = "yyyy-MM-dd HH:mm"
    }

    private val dateFormat = SimpleDateFormat(format, Locale.getDefault())


    /**
     * @param now: time reference
     * @param time: the time that will be formatted relative to [now]
     * @param completed: if completed return the completed color resource
     */
    fun dueFormat(now: Long, time: Long?, completed: Boolean = false): DueDateFormatted {
        val color = if (completed) {
            RES_COLOR_TODO_COMPLETED
        } else {
            time?.let {
                when {
                    now >= it -> RES_COLOR_TODO_PASSED_DUE
                    it - now <= TimeUnit.HOURS.toMillis(24) -> RES_COLOR_TODO_ALMOST_DUE
                    else -> RES_COLOR_TODO_NO_DUE
                }
            } ?: RES_COLOR_TODO_NO_DUE
        }
        val isAboutDue = color == RES_COLOR_TODO_ALMOST_DUE
        val formatted = time?.let { dateFormat.format(time) } ?: ""
        return DueDateFormatted(time, formatted, color, isAboutDue)
    }

    fun format(now: Long): String = if (now > -1L) dateFormat.format(now) else ""

}


/**
 * Created by Cristian Pela on 01.04.2018.
 */
data class DueDateFormatted(val time: Long?, val formatted: String, val color: ResInt, val isAboutDue: Boolean)