package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_MSG_TODO_DELETED
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.env.DeleteTODOMessage
import com.crskdev.rxmvi.domain.env.DeleteTODORequest
import com.crskdev.rxmvi.domain.env.DeleteTODOResponse
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.ErrorResponse
import com.crskdev.rxmvicore.env.MessageResponse
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Created by Cristian Pela on 16.04.2018.
 */
class DeleteTODOUseCase
@Inject constructor(
        @RepoProxy private val repository: TODORepository,
        private val resourceIDLocator: ResourceIDLocator

) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<DeleteTODORequest>().switchMap {
                    repository.delete(it.id)
                            .toObservable()
                            .flatMap {
                                DeleteTODOResponse(it)<DeleteTODOUseCase>().toObservable()
                                        .mergeWith(MessageResponse(DeleteTODOMessage(resourceIDLocator.lookUp(RES_MSG_TODO_DELETED), it))
                                                .toObservable())
                            }
                            .onErrorReturnItem(ErrorResponse(Error("Could not delete todo requested todo")))
                }
            }
}
