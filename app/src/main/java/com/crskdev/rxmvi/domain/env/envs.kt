package com.crskdev.rxmvi.domain.env

import com.crskdev.rxmvi.domain.DueDateFormatted
import com.crskdev.rxmvi.domain.ResInt
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.presentation.upsert.Mode
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.util.Page
import java.util.*

/**
 * .
 * Created by Cristian Pela on 07.03.2018.
 */
class FetchTODOsIntent(val filter: TODOFilter, val fromStart: Boolean) : Intent()
class SearchTODOIntent(val textLike:String, val fromStart: Boolean): Intent()
class UpsertTODOIntent(val todo: TODOModelViewEntity, val mode: Mode = Mode.ADD) : Intent()
class SelectTODOIntent(val id: Long) : Intent()
class DeleteTODOIntent(val id: Long) : Intent()
class SaveFilterIntent(val filter: TODOFilter) : Intent()
class ChangeFilterIntent(val filter: TODOFilter) : Intent()
class CompleteTODOIntent(val id: Long, val complete: Boolean) : Intent()
object WatchTODOIntent : Intent()
object WatchTODODeletedIntent : Intent()
object WatchAggregateIntent : Intent()
object WatchChangeFilterIntent : Intent()
object GetUsernameIntent : Intent()
class SaveUsernameIntent(val username: String) : Intent()
class SelectDueDateIntent(val date: Long, val isCompleted: Boolean = false) : Intent()
object GoBackIntent: Intent()


class FetchTODOsRequest(val filter: TODOFilter, val fromStart: Boolean) : Request()
class SearchTODORequest(val textLike:String, val fromStart: Boolean): Request()
class WatchSearchTODORequest(val textLike:String, cancel: Boolean): Request(cancel)
class UpsertTODORequest(val todo: TODOEntity, val mode: Mode = Mode.ADD) : Request()
class DeleteTODORequest(val id: Long) : Request()
class SelectTODORequest(val id: Long) : Request()
class SaveFilterRequest(val filter: TODOFilter) : Request()
class ChangeFilterRequest(val filter: TODOFilter) : Request()
class CompleteTODORequest(val id: Long, val complete: Boolean) : Request()
class WatchTODORequest(cancel: Boolean) : Request(cancel)
class WatchTODODeletedRequest(cancel: Boolean) : Request(cancel)
class WatchAggregateRequest(cancel: Boolean) : Request(cancel)
class WatchChangeFilterRequest(cancel: Boolean) : Request(cancel)
object GetUsernameRequest : Request(false)
class SaveUsernameRequest(val username: String) : Request(false)
class SelectDueDateRequest(val date: Long, val isCompleted: Boolean = false) : Request(false)
object GoBackRequest: Request()

class FetchTODOsResponse(val filter: TODOFilter, val todos: List<TODOEntity>) : StatefulResponse()
class SearchTODOResponse(val textLike: String, val todos: List<TODOEntity>): StatefulResponse()
class DeleteTODOResponse(val id: Long) : StatefulResponse()
class SelectTODOResponse(val todo: TODOEntity) : StatefulResponse()
class WatchTODOResponse(val todo: TODOEntity) : StatefulResponse()
class WatchTODODeletedResponse(val ids: List<Long>) : StatefulResponse()
class WatchAggregateResponse(val aggregates: EnumMap<TODOFilter, Int>) : StatefulResponse()
class GetUsernameResponse(val username: String) : StatefulResponse()
class SelectDueDateResponse(val dateFormatted: DueDateFormatted) : StatefulResponse()

class DeleteTODOMessage(resId: ResInt, todoId: Long) : ResourceIDMessage(Res(resId, todoId))
object GoBackMessage: Message<Unit>(Unit)

