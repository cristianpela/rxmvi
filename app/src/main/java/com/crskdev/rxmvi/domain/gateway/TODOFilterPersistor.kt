package com.crskdev.rxmvi.domain.gateway

import com.crskdev.rxmvi.domain.TODOFilter
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Persistence for current filter
 * Created by Cristian Pela on 13.03.2018.
 */
interface TODOFilterPersistor {

    fun load(defaultForEmpty: TODOFilter = TODOFilter.UNKNOWN): Single<TODOFilter>
            = Single.just(defaultForEmpty)

    fun save(filter: TODOFilter): Completable = Completable.complete()

    fun observerFilterChanges(defaultForEmpty: TODOFilter): Observable<TODOFilter>
}