package com.crskdev.rxmvi.domain

/**
 * Resource Domain IDs aggregator
 * Created by Cristian Pela on 08.03.2018.
 */

//ERRORS
const val RES_ERR_EMPTY_AUTHOR : ResInt = 300
const val RES_ERR_EMPTY_TITLE : ResInt = 301
const val RES_ERR_EMPTY_CREATION_DATE : ResInt = 302
const val RES_ERR_DUE_DATE_BEFORE_NOW : ResInt = 303
const val RES_ERR_DUE_DATE_INVALID : ResInt = 304
const val RES_ERR_FETCH_EMPTY_SINGLE_TODO : ResInt = 305
const val RES_ERR_USERNAME_NOT_FOUND : ResInt = 306

//MESSAGES
const val RES_OK : ResInt = 200
const val RES_MSG_TODO_ADD : ResInt = 201
const val RES_MSG_TODO_UPDATE : ResInt = 202
const val RES_MSG_FETCH_EMPTY_TODOS : ResInt = 203
const val RES_MSG_USERNAME_SAVED : ResInt = 204
const val RES_MSG_TODO_DELETED : ResInt = 205

//COLORS
const val RES_COLOR_TODO_COMPLETED : ResInt = 500
const val RES_COLOR_TODO_PASSED_DUE : ResInt = 501
const val RES_COLOR_TODO_ALMOST_DUE : ResInt = 502
const val RES_COLOR_TODO_NO_DUE : ResInt = 503

fun getAllResourcesIDs(): Array<ResInt> =
    Class.forName("com.crskdev.rxmvi.domain.ResourceIDsKt")
            .declaredFields.map { it.getInt(null) }.toTypedArray()

typealias ResInt = Int


