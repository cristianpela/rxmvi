package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_ERR_EMPTY_AUTHOR
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.env.GetUsernameResponse
import com.crskdev.rxmvi.domain.env.SaveUsernameRequest
import com.crskdev.rxmvi.domain.gateway.UserService
import com.crskdev.rxmvi.util.isEmptyOrBlank
import com.crskdev.rxmvicore.env.ErrorResponse
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 28.03.2018.
 */
class SaveUsernameUseCase
@Inject constructor(
        private val userService: UserService,
        private val resourceIDLocator: ResourceIDLocator) : UseCase {

    override fun execute(): ObservableTransformer<in Request, out Response> = ObservableTransformer {
        it.cast<SaveUsernameRequest>().switchMap {
            if (it.username.isEmptyOrBlank()) {
                ErrorResponse.withId(resourceIDLocator.lookUp(RES_ERR_EMPTY_AUTHOR))<SaveUsernameUseCase>().toObservable()
            } else {
                userService.saveUser(it.username)
                        .andThen(GetUsernameResponse(it.username).toObservable())
            }
        }
    }

}