package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.SearchTODORequest
import com.crskdev.rxmvi.domain.env.SearchTODOResponse
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Created by Cristian Pela on 20.04.2018.
 */
class SearchTODOUseCase
@Inject constructor(@RepoProxy private val repository: TODORepository) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<SearchTODORequest>().switchMap { r->
                    repository.search(r.textLike, r.fromStart)
                            .toObservable()
                            .map { SearchTODOResponse(r.textLike, it)<SearchTODOUseCase>() }
                }
            }
}