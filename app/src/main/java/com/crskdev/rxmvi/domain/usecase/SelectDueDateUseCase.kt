package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.DateFormatter
import com.crskdev.rxmvi.domain.env.SelectDueDateRequest
import com.crskdev.rxmvi.domain.env.SelectDueDateResponse
import com.crskdev.rxmvi.platform.TimeProvider
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast
import javax.inject.Inject

/**
 * Created by Cristian Pela on 01.04.2018.
 */
class SelectDueDateUseCase @Inject constructor(
        private val timeProvider: TimeProvider,
        private val dateFormatter: DateFormatter
) : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<SelectDueDateRequest>().map {
                    val now = timeProvider.currentTimeMillis()
                    val dateDueDateFormatted = dateFormatter.dueFormat(now, it.date, it.isCompleted)
                    SelectDueDateResponse(dateDueDateFormatted)
                }
            }

}