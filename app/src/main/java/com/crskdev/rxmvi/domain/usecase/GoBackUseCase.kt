package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.GoBackMessage
import com.crskdev.rxmvi.domain.env.GoBackRequest
import com.crskdev.rxmvicore.env.MessageResponse
import com.crskdev.rxmvicore.env.Request
import com.crskdev.rxmvicore.env.Response
import com.crskdev.rxmvicore.usecase.UseCase
import io.reactivex.ObservableTransformer
import io.reactivex.rxkotlin.cast

/**
 * Created by Cristian Pela on 18.04.2018.
 */
object GoBackUseCase : UseCase {
    override fun execute(): ObservableTransformer<in Request, out Response> =
            ObservableTransformer {
                it.cast<GoBackRequest>().map { MessageResponse(GoBackMessage) }
            }
}