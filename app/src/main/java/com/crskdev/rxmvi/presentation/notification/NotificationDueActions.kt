package com.crskdev.rxmvi.presentation.notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.crskdev.rxmvi.platform.app.conductor.activity.ConductorActivity

/**
 * Created by Cristian Pela on 25.04.2018.
 */
object NotificationDueActions {

    private const val DUE_ACTION_AUTHORITY = "com.crskdev.rxmvi"

    const val DUE_ACTION_COMPLETE = "$DUE_ACTION_AUTHORITY.ACTION_DUE_COMPLETE"

    const val DUE_ACTION_DELETE = "$DUE_ACTION_AUTHORITY.ACTION_DUE_DELETE"

    const val DUE_ACTION_SHOW_ALL = "$DUE_ACTION_AUTHORITY.ACTION_DUE_SHOW_ALL"

    const val DUE_EXTRA_TODO_ID = "$DUE_ACTION_AUTHORITY.DUE_EXTRA_TODO_ID"

    const val DUE_EXTRA_NOTIFICATION_ID = "$DUE_ACTION_AUTHORITY.DUE_EXTRA_NOTIFICATION_ID"

    fun complete(context: Context, todoId: Long, notificationId: Int): PendingIntent {
        val intent = Intent(context, NotificationDueActionProcessor::class.java)
                .setAction(DUE_ACTION_COMPLETE)
                .putExtra(DUE_EXTRA_TODO_ID, todoId)
                .putExtra(DUE_EXTRA_NOTIFICATION_ID, notificationId)
        return PendingIntent.getBroadcast(context, 4002, intent, 0)
    }

    fun delete(context: Context, todoId: Long, notificationId: Int): PendingIntent {
        val intent = Intent(context, NotificationDueActionProcessor::class.java)
                .setAction(DUE_ACTION_DELETE)
                .putExtra(DUE_EXTRA_TODO_ID, todoId)
                .putExtra(DUE_EXTRA_NOTIFICATION_ID, notificationId)
        return PendingIntent.getBroadcast(context, 4003, intent, 0)
    }

    fun showAll(context: Context, notificationId: Int): PendingIntent {
        val intent = Intent(context, ConductorActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .setAction(DUE_ACTION_SHOW_ALL)
                .putExtra(DUE_EXTRA_NOTIFICATION_ID, notificationId)
        return PendingIntent
                .getActivity(context, 4001, intent, 0)
    }


}