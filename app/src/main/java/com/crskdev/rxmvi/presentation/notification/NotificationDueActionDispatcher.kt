package com.crskdev.rxmvi.presentation.notification

import com.crskdev.rxmvi.domain.gateway.TODORepository

/**
 * Created by Cristian Pela on 24.04.2018.
 */
interface NotificationDueActionDispatcher {

    fun complete(todoId: Long)

    fun delete(todoId: Long)
}

class NotificationDueActionDispatcherImpl(private val repository: TODORepository) : NotificationDueActionDispatcher {

    override fun complete(todoId: Long) {
        repository.complete(todoId, true).subscribe()
    }

    override fun delete(todoId: Long) {
        repository.delete(todoId).subscribe()
    }

}