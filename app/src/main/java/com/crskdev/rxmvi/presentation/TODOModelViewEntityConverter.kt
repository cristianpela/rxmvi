package com.crskdev.rxmvi.presentation

import com.crskdev.rxmvi.domain.DateFormatter
import com.crskdev.rxmvi.domain.DueDateFormatted
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.platform.TimeProvider
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
class TODOModelViewEntityConverter
@Inject constructor(private val timeProvider: TimeProvider,
                    private val resourceIDLocator: ResourceIDLocator,
                    private val dateFormatter: DateFormatter) {

    fun updateDue(viewEntity: TODOModelViewEntity, dueFormatted: DueDateFormatted): TODOModelViewEntity {
        return viewEntity.copy(
                dueDate = dueFormatted.time,
                dueDateFormatted = dueFormatted.formatted,
                dueDateColor = resourceIDLocator.lookUp(dueFormatted.color),
                isAboutDue = dueFormatted.isAboutDue)
    }

    fun reCheckDueDateIfNeeded(viewEntity: TODOModelViewEntity): TODOModelViewEntity =
            if (viewEntity.dueDate != null) {
                val dueFormatted = dateFormatter.dueFormat(timeProvider.currentTimeMillis(), viewEntity.dueDate, viewEntity.completed)
                updateDue(viewEntity, dueFormatted)
            } else
                viewEntity


    fun convert(entity: TODOEntity): TODOModelViewEntity {

        val now = timeProvider.currentTimeMillis()

        val dueDateFormatted = dateFormatter.dueFormat(now, entity.dueDate, entity.completed)

        val dueDateColor = resourceIDLocator.lookUp(dueDateFormatted.color)

        val isAboutDue = dueDateFormatted.isAboutDue
        val dueFormatted = dueDateFormatted.formatted

        val creationFormatted = dateFormatter.format(entity.creationDate)

        return TODOModelViewEntity(
                entity.id,
                creationFormatted,
                entity.creationDate,
                dueFormatted,
                entity.dueDate != null,
                entity.dueDate,
                isAboutDue,
                dueDateColor,
                entity.author,
                entity.title,
                entity.detail,
                entity.completed)
    }

    fun revert(viewEntity: TODOModelViewEntity): TODOEntity =
            TODOEntity(
                    viewEntity.id,
                    viewEntity.creationDate,
                    if (viewEntity.hasDue) {
                        viewEntity.dueDate ?: -1
                    } else null,
                    viewEntity.author,
                    viewEntity.title,
                    viewEntity.detail,
                    viewEntity.completed)
}