package com.crskdev.rxmvi.presentation.notification

import android.content.Context
import com.crskdev.rxmvi.platform.app.appComponent
import com.evernote.android.job.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 * Created by Cristian Pela on 24.04.2018.
 */
class NotificationDueJob : Job() {
    @Inject
    lateinit var view: NotificationDueMVP.NotificationDueView

    @Inject
    lateinit var jobManager: NotificationDueJobManager

    override fun onRunJob(params: Params): Result {
        val result = FutureNotificationDueJobResult(Thread.currentThread().name)

        context.appComponent().notificationDueComponentBuilder()
                .bindNotificationDueJobResult(result)
                .build()
                .inject(this)

        val completed = result.waitForComplete()

        if (params.tag == TAG_INITIAL && completed) {
            //we have the initial job ran, now we schedule the periodic job run from there
            jobManager.schedulePeriodic()
        }

        return if (completed) Result.SUCCESS else Result.FAILURE
    }

    companion object {
        internal const val TAG_INITIAL = "NotificationDueJob_Initial"
        internal const val TAG_PERIODIC = "NotificationDueJob_Periodic"
    }

}

private class NotificationDueJobCreator : JobCreator {
    override fun create(tag: String): Job? =
            when (tag) {
                NotificationDueJob.TAG_INITIAL,
                NotificationDueJob.TAG_PERIODIC -> NotificationDueJob()
                else -> null
            }
}


class NotificationDueJobManager(context: Context) {

    private val manager = JobManager.create(context)
            .apply { addJobCreator(NotificationDueJobCreator()) }

    fun cancelAll(): Int = manager.let {
        val cancelAllForInitial = it.cancelAllForTag(NotificationDueJob.TAG_INITIAL)
        val cancelAllPeriodic = it.cancelAllForTag(NotificationDueJob.TAG_PERIODIC)
        return cancelAllForInitial + cancelAllPeriodic
    }

    private fun hasRunningJobs(tag: String): Boolean = manager.getAllJobsForTag(tag).isNotEmpty()

    fun schedule() {
        if (!hasRunningJobs(NotificationDueJob.TAG_INITIAL)) {
            //schedule now when the app starts after o a kill or restart
            DailyJob.startNowOnce(JobRequest.Builder(NotificationDueJob.TAG_INITIAL))
            //schedule between 1am and 6am
            DailyJob.schedule(JobRequest.Builder(NotificationDueJob.TAG_INITIAL),
                    TimeUnit.HOURS.toMillis(1),
                    TimeUnit.HOURS.toMillis(6))
        }

    }

    fun schedulePeriodic(interval: Long = TimeUnit.MINUTES.toMillis(15),
                         flexMs: Long = TimeUnit.MINUTES.toMillis(5)) {
        if (!hasRunningJobs(NotificationDueJob.TAG_PERIODIC)) {
            JobRequest.Builder(NotificationDueJob.TAG_PERIODIC)
                    .setPeriodic(interval, flexMs)
                    .build()
                    .schedule()
        }
    }
}
