package com.crskdev.rxmvi.presentation.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvicore.util.cast
import io.reactivex.functions.BiFunction
import java.util.*
import javax.inject.Inject

/**
 * Created by Cristian Pela on 24.04.2018.
 */
interface NotificationDueMVP {

    interface NotificationDueView {
        /**
         * Displays the most first to-do which is in due range and the number of to=dos, besides first one,
         * which are in due range
         */
        fun display(todo: TODOModelViewEntity, moreNumber: Int)

    }

    interface NotificationDuePresenter {
        fun attach(view: NotificationDueView)
    }


}


class NotificationDueViewImpl
@Inject constructor(
        private val context: Context,
        private val presenter: NotificationDueMVP.NotificationDuePresenter) : NotificationDueMVP.NotificationDueView {


    companion object {
        private const val NOTIFICATION_ID = 2018
        private const val NOTIFICATION_DUE_CHANNEL_ID = "NOTIFICATION_DUE_CHANNEL_ID"
    }

    @Inject
    fun postInject() {
        presenter.attach(this)
    }

    override fun display(todo: TODOModelViewEntity, moreNumber: Int) {
        todo.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel = NotificationChannel(NOTIFICATION_DUE_CHANNEL_ID,
                        "Due TO-DO", NotificationManager.IMPORTANCE_DEFAULT)
                context.getSystemService(Context.NOTIFICATION_SERVICE)
                        .cast<NotificationManager>()
                        .createNotificationChannel(notificationChannel)
            }
            val notification = NotificationCompat.Builder(context, NOTIFICATION_DUE_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_event_note_black_24dp)
                    .setContentTitle(it.title)
                    .setContentText(it.detail)
                    .let {
                        if (moreNumber > 0)
                            it.setSubText(context.getString(R.string.notification_due_more, moreNumber))
                                    .setNumber(moreNumber)
                        else
                            it
                    }
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true)
                    .setContentIntent(NotificationDueActions.showAll(context, NOTIFICATION_ID))
                    .addAction(R.drawable.ic_event_note_black_24dp, context.getString(R.string.notification_due_complete),
                            NotificationDueActions.complete(context, it.id, NOTIFICATION_ID))
                    .addAction(R.drawable.ic_event_note_black_24dp, context.getString(R.string.notification_due_delete),
                            NotificationDueActions.delete(context, it.id, NOTIFICATION_ID))
                    .build()
            NotificationManagerCompat.from(context).notify(NOTIFICATION_ID, notification)
        }

    }
}


class NotificationDuePresenterImpl
@Inject constructor(
        private val jobResult: NotificationDueJobResult,
        private val repository: TODORepository,
        private val converter: TODOModelViewEntityConverter) : NotificationDueMVP.NotificationDuePresenter {

    private var view: NotificationDueMVP.NotificationDueView? = null

    override fun attach(view: NotificationDueMVP.NotificationDueView) {
        this.view = view
        repository
                .getAll(TODOFilter.ABOUT_DUE, true, 1, true)
                .zipWith(repository.getAggregation(),
                        BiFunction<List<TODOEntity>, EnumMap<TODOFilter, Int>, Pair<TODOModelViewEntity?, Int>> { left, right ->
                            val model = left.firstOrNull()?.let { converter.convert(it) }
                            val number = right[TODOFilter.ABOUT_DUE] ?: 0
                            model to number - 1 // exclude current model from "more" number
                        })
                .subscribe({ pair ->
                    pair.first?.let {
                        view.display(it, pair.second)
                    }
                    jobResult.signal(true)
                }, { _ -> jobResult.signal(false) })
    }
}