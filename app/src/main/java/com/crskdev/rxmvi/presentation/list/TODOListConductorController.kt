@file:Suppress("MemberVisibilityCanBePrivate")

package com.crskdev.rxmvi.presentation.list

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuInflater
import android.view.ViewGroup
import android.widget.TextView
import com.bluelinelabs.conductor.ControllerChangeHandler
import com.bluelinelabs.conductor.ControllerChangeType
import com.bluelinelabs.conductor.RouterTransaction
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.platform.app.conductor.controller.MVIConductorController
import com.crskdev.rxmvi.presentation.aggregate.AggregatorConductorController
import com.crskdev.rxmvi.presentation.search.TODOSearchConductorController
import com.crskdev.rxmvi.presentation.upsert.add.AddTODOConductorController
import com.crskdev.rxmvi.util.EndlessRecyclerViewScrollListener
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.presentation.IntentSender
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import com.crskdev.rxmvicore.util.cast
import com.crskdev.rxmvicore.util.singletonList
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named


/**
 * .
 * Created by Cristian Pela on 16.03.2018.
 */
class TODOListConductorController(args: Bundle? = null)
    : MVIConductorController<TODOListState, MVIStreamOps<TODOListState>>(args) {

    companion object {
        const val KEY_FILTER = "KEY_FILTER"
    }

    @Inject
    @field:[Named("ListTODOMVIStreamOps")]
    lateinit var injectedStreamOps: MVIStreamOps<TODOListState>

    override val mviStreamOps: MVIStreamOps<TODOListState> by lazy {
        MVIStreamOps.start(injectedStreamOps)
    }

    override fun injectMe() {
        injector()?.component()?.inject(this)
    }

    override fun onAllowViewsToPrepare() {
        R.id.toolbar.v<Toolbar>()?.apply {
            inflateMenu(R.menu.menu_list_todo)
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.menu_filter_all ->
                        postSendIntent(ChangeFilterIntent(TODOFilter.ALL))
                                .let { true }
                    R.id.menu_filter_completed -> postSendIntent(ChangeFilterIntent(TODOFilter.COMPLETED))
                            .let { true }
                    R.id.menu_filter_uncompleted -> postSendIntent(ChangeFilterIntent(TODOFilter.NOT_COMPLETED))
                            .let { true }
                    R.id.menu_search -> TODOSearchConductorController.launch(router)
                            .let { true }
                    else -> false
                }
            }
        }
        R.id.recyclerTodos.v<RecyclerView>()?.apply {
            adapter = TODOListAdapter(router)
            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager, 2) {
                override fun onLoadMore(page: Int, totalItemsCount: Int) {
                    this@apply.tag?.run {
                        postSendIntent(FetchTODOsIntent(cast<TODOFilter>(), false))
                    }
                }

            })
        }
        R.id.fab.v<FloatingActionButton>()?.setOnClickListener {
            AddTODOConductorController.launch(router)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_list_todo, menu)
    }

    override fun onAllowSendIntent(intentSender: IntentSender, isFirstTime: Boolean) {

        val filter = if (isFirstTime)
            TODOFilter.values()[args.getInt(KEY_FILTER, TODOFilter.UNKNOWN.ordinal)]
        else TODOFilter.UNKNOWN

        intentSender.sendIntent(FetchTODOsIntent(filter, true))

        //send these intents each time the view is recreated
        //because the watchers are canceled when the view is destroyed
        intentSender.sendIntent(WatchTODOIntent)
        intentSender.sendIntent(WatchChangeFilterIntent)
        intentSender.sendIntent(WatchTODODeletedIntent)
    }

    override fun renderState(state: TODOListState) {
        R.id.textFilterTitle.v<TextView>()?.text =
                resources?.getStringArray(R.array.filters)
                        ?.get(state.filter.ordinal)
        state.todos?.let {
            R.id.recyclerTodos.v<RecyclerView>()?.apply {
                adapter?.cast<TODOListAdapter>()?.setTodos(it)
                tag = state.filter
            }

        }
    }

    override fun provideIntentStreams(): List<Observable<Intent>> =
            R.id.recyclerTodos.v<RecyclerView>()
                    ?.adapter
                    ?.cast<TODOListAdapter>()
                    ?.observeCompletion()
                    ?.singletonList()
                    ?: emptyList()

    override fun layout(): Int = R.layout.layout_todo_list

    override fun onChangeEnded(changeHandler: ControllerChangeHandler, changeType: ControllerChangeType) {
        //adding the aggregator component
        if (changeType == ControllerChangeType.PUSH_ENTER) {
            R.id.aggregatorContainer.v<ViewGroup>()?.let {
                val childRouter = getChildRouter(it)
                if (!childRouter.hasRootController()) {
                    childRouter.setRoot(RouterTransaction.with(AggregatorConductorController()))
                }
            }

        }
    }

}