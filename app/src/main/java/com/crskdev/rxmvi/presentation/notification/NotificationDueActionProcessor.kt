package com.crskdev.rxmvi.presentation.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationManagerCompat
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.platform.app.appComponent
import javax.inject.Inject

/**
 * Created by Cristian Pela on 25.04.2018.
 */
class NotificationDueActionProcessor : BroadcastReceiver() {

    @Inject
    lateinit var dispatcher: NotificationDueActionDispatcher

    override fun onReceive(context: Context, intent: Intent) {

        context.appComponent().notificationDueActionProcessor()
                .build()
                .inject(this)

        val todoId = intent.getLongExtra(NotificationDueActions.DUE_EXTRA_TODO_ID, TODOEntity.NO_ID)
        val notificationId = intent.getIntExtra(NotificationDueActions.DUE_EXTRA_NOTIFICATION_ID, -1)

        //hide notification
        if (notificationId != -1) {
            NotificationManagerCompat.from(context).cancel(notificationId)
        }

        when (intent.action) {
            NotificationDueActions.DUE_ACTION_COMPLETE -> {
                dispatcher.complete(todoId)
            }
            NotificationDueActions.DUE_ACTION_DELETE -> {
                dispatcher.delete(todoId)
            }
        }
    }
}