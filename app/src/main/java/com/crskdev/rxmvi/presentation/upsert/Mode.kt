package com.crskdev.rxmvi.presentation.upsert

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
enum class Mode {
    ADD, EDIT
}