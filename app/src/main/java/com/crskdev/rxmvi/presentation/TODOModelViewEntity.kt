package com.crskdev.rxmvi.presentation

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
@Parcelize
data class TODOModelViewEntity(
        val id: Long,
        val creationDateFrmt: String,
        val creationDate: Long,
        val dueDateFormatted: String,
        val hasDue: Boolean,
        val dueDate: Long?,
        val isAboutDue: Boolean,
        val dueDateColor: Int,
        val author: String,
        val title: CharSequence,
        val detail: CharSequence?,
        val completed: Boolean,
        val isEdited: Boolean = false) : Parcelable