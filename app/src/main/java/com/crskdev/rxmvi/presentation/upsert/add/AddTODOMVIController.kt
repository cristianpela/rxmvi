package com.crskdev.rxmvi.presentation.upsert.add

import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.domain.usecase.*
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOState
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.presentation.*
import com.crskdev.rxmvicore.util.cast
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import javax.inject.Inject

/**
 * Created by Cristian Pela on 28.03.2018.
 */
class AddTODOMVIController
@Inject constructor(
        private val upsertTODOUseCase: UpsertTODOUseCase,
        private val getUsernameUseCase: GetUsernameUseCase,
        private val saveUsernameUseCase: SaveUsernameUseCase,
        private val selectDueDateUseCase: SelectDueDateUseCase,
        private val converter: TODOModelViewEntityConverter
) : MVIController<UpsertTODOState>() {

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    GetUsernameIntent -> GetUsernameRequest
                    GoBackIntent -> GoBackRequest
                    is SaveUsernameIntent -> SaveUsernameRequest(it.username)
                    is UpsertTODOIntent -> UpsertTODORequest(it.todo.let { converter.revert(it) }, it.mode)
                    is SelectDueDateIntent -> SelectDueDateRequest(it.date, it.isCompleted)
                    is SyncStateIntent -> SyncStateRequest(it.state)
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<Carrier<UpsertTODOState>, StatefulResponse, Carrier<UpsertTODOState>> =
            BiFunction { carrier, response ->
                when (response) {
                    is SelectTODOResponse -> carrier.next(response.source(),
                            UpsertTODOState(Status.IDLE, response.todo.let { converter.convert(it).copy(isEdited = false) }), NONE)
                    is GetUsernameResponse -> carrier
                            .next(response.source(),
                                    UpsertTODOState(Status.IDLE,
                                            converter.convert(TODOEntity.EMPTY.copy(author = response.username))))
                    is SelectDueDateResponse -> {
                        carrier.next(response.source(),
                                UpsertTODOState(Status.IDLE, carrier.getState().todo?.let {
                                    converter.updateDue(it, response.dateFormatted).copy(isEdited = true)
                                }))
                    }
                    is SyncStateResponse ->
                        carrier.next(response.source(),
                                UpsertTODOState(Status.IDLE,
                                        response.state.cast<TODOModelViewEntity>().let {
                                            val isEdited = if (it.isEdited) true else it != carrier.getState().todo
                                            val copy = it.copy(isEdited = isEdited)
                                            converter.reCheckDueDateIfNeeded(copy)
                                        }), NONE)
                    else -> carrier
                }
            }

    override fun defaultStateCarrier(): Carrier<UpsertTODOState> =
            Carrier.init(UpsertTODOState(Status.IDLE)) { _, curr, _, _ -> curr }

    override fun Dispatcher.useCaseDispatcher() {
        req<GetUsernameRequest>() interactWith getUsernameUseCase
        req<SaveUsernameRequest>() interactWith saveUsernameUseCase
        req<UpsertTODORequest>() interactWith upsertTODOUseCase
        req<SelectDueDateRequest>() interactWith selectDueDateUseCase
        req<SyncStateRequest>() interactWith syncStateUseCase
        req<GoBackRequest>() interactWith GoBackUseCase
    }

}