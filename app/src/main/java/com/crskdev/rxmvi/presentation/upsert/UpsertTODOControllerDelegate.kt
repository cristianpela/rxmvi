package com.crskdev.rxmvi.presentation.upsert

import android.graphics.PorterDuff
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputLayout
import android.support.transition.ChangeBounds
import android.support.transition.TransitionManager
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.widget.CheckBox
import android.widget.DatePicker
import android.widget.ImageButton
import android.widget.TextView
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.platform.app.conductor.controller.MVIConductorController
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.util.*
import com.crskdev.rxmvicore.env.Message
import com.crskdev.rxmvicore.env.ResourceIDMessage
import com.crskdev.rxmvicore.env.SyncStateIntent
import com.crskdev.rxmvicore.util.cast
import com.crskdev.rxmvicore.util.pickRightWhenDiff
import java.util.*

/**
 * .
 * Created by Cristian Pela on 30.03.2018.
 */
class UpsertTODOControllerDelegate(
        private val controller: MVIConductorController<*, *>, private val mode: Mode) {

    private var model: TODOModelViewEntity?
        set(value) {
            with(controller) {
                view?.tag = value
            }
        }
        get() = with(controller) {
            view?.tag?.cast<TODOModelViewEntity>()
        }


    fun onAllowViewsToPrepare() {
        with(controller) {
            R.id.buttonCalendar.v<ImageButton>()?.apply {
                setColorFilter(context.getColorCompat(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
                setOnClickListener {
                    activity?.let { activity ->
                        AlertDialog.Builder(activity)
                                .setCancelable(true)
                                .create()
                                .apply dialog@{
                                    val datePicker = DatePicker(activity).apply {
                                        activity.hideSoftInputKeyboard()
                                        val (year, month, day) = Calendar.getInstance().let {
                                            Triple(it.get(Calendar.YEAR), it.get(Calendar.MONTH), it.get(Calendar.DAY_OF_MONTH))
                                        }
                                        init(year, month, day) { _, selYear, selMonth, selDay ->
                                            val time = timeFrom(selYear, selMonth + 1, selDay)
                                            gatherModel()?.let {
                                                postSendIntent(SyncStateIntent(it))
                                                postSendIntent(SelectDueDateIntent(time, it.completed))
                                            }
                                            this@dialog.dismiss()
                                        }
                                    }
                                    setView(datePicker)
                                }
                                .asController()
                                .show(router)
                    }
                }
            }

            R.id.toolbarUpsert.v<Toolbar>()?.apply {
                if (mode == Mode.EDIT) {
                    inflateMenu(R.menu.menu_edit_todo)
                    menu.findItem(R.id.menu_complete)
                            .actionView.cast<CheckBox>()
                            .setOnCheckedChangeListener { _, checked ->
                                gatherModel()?.let { postSendIntent(SyncStateIntent(it.copy(completed = checked))) }
                            }
                    menu.findItem(R.id.menu_delete).setOnMenuItemClickListener {
                        gatherModel()?.let { postSendIntent(DeleteTODOIntent(it.id)) }
                        true
                    }
                }
                setNavigationOnClickListener {
                    goBack()
                }
            }

            R.id.checkDueDate.v<CheckBox>()?.setOnCheckedChangeListener { _, checked ->
                activity?.hideSoftInputKeyboard()
                gatherModel()?.let { postSendIntent(SyncStateIntent(it.copy(hasDue = checked))) }
            }

            R.id.fabUpsert.v<FloatingActionButton>()?.apply {
                postDelayed({
                    show()
                }, 500)
                setOnClickListener {
                    activity?.hideSoftInputKeyboard()
                    activity?.currentFocus?.clearFocus()
                    gatherModel()?.let { postSendIntent(UpsertTODOIntent(it, mode)) }
                }
            }
        }
    }

    fun goBack() {
        with(controller) {
            gatherModel()?.let { postSendIntent(SyncStateIntent(it)) }
            postSendIntent(GoBackIntent)
        }
    }

    private fun showDueComponents(show: Boolean) {
        with(controller) {
            val layout = if (show) R.layout.layout_todo_upsert_due else R.layout.layout_todo_upsert
            val constraintSet = ConstraintSet().apply {
                clone(controller.activity, layout)
            }
            val constraintLayout = R.id.containerUpsert.v<ConstraintLayout>()!!
            TransitionManager.beginDelayedTransition(constraintLayout, ChangeBounds())
            constraintSet.applyTo(constraintLayout)
        }
    }

    fun layout(): Int = R.layout.layout_todo_upsert_coodinator

    fun renderState(state: UpsertTODOState) {
        state.todo?.let {
            with(controller) {
                //TODO the "pick" logic should be on the mvi controller see. [MVIController#diffState]
                val oldModel = model
                model = it

//                pickRightWhenDiff(oldModel?.title, model?.title)?.let {
                    R.id.textInputTitle.v<TextInputLayout>()?.editText?.apply {
                        setText(model?.title)
                    }
//                }
//
//                pickRightWhenDiff(oldModel?.detail, model?.detail)?.let {
                    R.id.editAddDetail.v<TextInputLayout>()?.editText?.apply {
                        setText(model?.detail)
                    }
//                }

                pickRightWhenDiff(oldModel?.completed, model?.completed)?.let {
                    R.id.toolbarUpsert.v<Toolbar>()
                            ?.menu
                            ?.findItem(R.id.menu_complete)
                            ?.actionView
                            ?.cast<CheckBox>()
                            ?.isChecked = it
                    R.id.containerUpsert.v<ConstraintLayout>()?.let { vg ->
                        for (c in 0..vg.childCount) {
                            vg.getChildAt(c)?.isEnabled = !it
                        }
                    }
                }

                pickRightWhenDiff(oldModel?.author, model?.author)?.let {
                    R.id.toolbarUpsert.v<Toolbar>()?.title = "By: $it"
                }

                pickRightWhenDiff(oldModel?.creationDateFrmt, model?.creationDateFrmt)?.let {
                    R.id.toolbarUpsert.v<Toolbar>()?.subtitle = it
                }

                pickRightWhenDiff(oldModel?.hasDue, model?.hasDue)?.let {
                    showDueComponents(it)
                    R.id.checkDueDate.v<CheckBox>()?.isChecked = it
                }

                pickRightWhenDiff(oldModel?.author, model?.author)?.let {
                    R.id.textAddAuthor.v<TextView>()?.text = it
                }

                pickRightWhenDiff(oldModel?.dueDateColor, model?.dueDateColor)?.let {
                    R.id.cardDueDate.v<CardView>()?.apply {
                        val backColor = context.getColorCompat(it)
                        setCardBackgroundColor(backColor)
                        getChildAt(0).cast<TextView>().let {
                            it.setBackgroundColor(backColor)
                            it.setTextColorInContrast(context.getColorCompat(R.color.colorSecondary))
                        }

                    }
                }

                pickRightWhenDiff(oldModel?.dueDateFormatted, model?.dueDateFormatted)?.let {
                    R.id.textDueDate.v<TextView>()?.apply {
                        text = it
                    }
                }

            }
        }
    }

    private fun showWarningDialogIfNotSaved() {
        with(controller) {
            activity?.hideSoftInputKeyboard()
            gatherModel()?.let { postSendIntent(SyncStateIntent(it as Any)) }
            if (model!!.isEdited) {
                activity?.let {
                    AlertDialog.Builder(it)
                            .setTitle(activity?.getString(R.string.alert_title_warning))
                            .setMessage(activity?.getString(R.string.warning_message_save_todo))
                            .setPositiveButton(android.R.string.ok) { d, _ ->
                                d.dismiss()
                                //pop next frame
                                view?.post { router.popCurrentController() }
                            }
                            .setNegativeButton(android.R.string.cancel) { d, _ ->
                                d.dismiss()
                            }.create()
                            .asController()
                            .show(router)
                }
            } else {
                router.popCurrentController()
            }
        }
    }

    fun renderErrors(errorRes: ResourceIDMessage.Res) {
        with(controller) {
            when (errorRes.id) {
                R.string.res_err_empty_title ->
                    R.id.textInputTitle.v<TextInputLayout>()?.error = getStringRes(errorRes.id, *errorRes.args)
                else ->
                    Snackbar.make(view!!, getStringRes(errorRes.id, *errorRes.args)!!, Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    fun renderMessage(message: Message<*>): Boolean {
        if (message === GoBackMessage) {
            showWarningDialogIfNotSaved()
            return true
        } else if (message is DeleteTODOMessage) {
            controller.router.popCurrentController()
            return true
        }
        return false
    }


    fun gatherModel(): TODOModelViewEntity? = with(controller) {
        model?.copy(
                title = R.id.textInputTitle.v<TextInputLayout>()?.editText?.text.toString(),
                detail = R.id.editAddDetail.v<TextInputLayout>()?.editText?.text?.toString()?.takeIf {
                    !it.isEmptyOrBlank()
                })
    }
}