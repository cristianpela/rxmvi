package com.crskdev.rxmvi.presentation.upsert

import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvicore.presentation.State
import com.crskdev.rxmvicore.presentation.Status

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
data class UpsertTODOState(override val status: Status = Status.IDLE,
                      val todo: TODOModelViewEntity? = null) : State