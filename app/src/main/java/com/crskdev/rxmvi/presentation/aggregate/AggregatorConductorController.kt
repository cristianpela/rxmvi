package com.crskdev.rxmvi.presentation.aggregate

import android.view.View
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.env.ChangeFilterIntent
import com.crskdev.rxmvi.domain.env.WatchAggregateIntent
import com.crskdev.rxmvi.platform.app.conductor.controller.MVIConductorController
import com.crskdev.rxmvi.util.CardChipView
import com.crskdev.rxmvi.util.id
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.presentation.IntentSender
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named


/**
 * .
 * Created by Cristian Pela on 26.03.2018.
 */
class AggregatorConductorController :
        MVIConductorController<AggregatorState, MVIStreamOps<AggregatorState>>(){


    @Inject
    @field:[Named("AggregatorMVIStreamOps")]
    lateinit var injectedStreamOps: MVIStreamOps<AggregatorState>

    override val mviStreamOps: MVIStreamOps<AggregatorState> by lazy { MVIStreamOps.start(injectedStreamOps) }

    override fun renderState(state: AggregatorState) {
        state.aggregation?.let {
            id<CardChipView>(R.id.chipPassedDue)?.text = it[TODOFilter.PASSED_DUE].toString()
            id<CardChipView>(R.id.chipAlmostDue)?.text = it[TODOFilter.ABOUT_DUE].toString()
            id<CardChipView>(R.id.chipCompleted)?.text = it[TODOFilter.COMPLETED].toString()
            id<CardChipView>(R.id.chipUncompleted)?.text = it[TODOFilter.NOT_COMPLETED].toString()
        }
    }

    override fun onAllowSendIntent(intentSender: IntentSender, isFirstTime: Boolean) {
        intentSender.sendIntent(WatchAggregateIntent)
    }

    override fun layout(): Int = R.layout.layout_todo_aggregator

    override fun injectMe() {
        injector()?.component()?.inject(this)
    }

    override fun provideIntentStreams(): List<Observable<Intent>> =
            listOf(filterClickIntent (id(R.id.chipPassedDue), TODOFilter.PASSED_DUE),
                    filterClickIntent(id(R.id.chipAlmostDue), TODOFilter.ABOUT_DUE),
                    filterClickIntent(id(R.id.chipCompleted), TODOFilter.COMPLETED),
                    filterClickIntent(id(R.id.chipUncompleted), TODOFilter.NOT_COMPLETED))


    private fun filterClickIntent(view: View?, filter: TODOFilter) =
            RxView.clicks(view!!).map { ChangeFilterIntent(filter) as Intent }

}