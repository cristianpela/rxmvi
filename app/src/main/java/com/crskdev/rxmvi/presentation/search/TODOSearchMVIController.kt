package com.crskdev.rxmvi.presentation.search

import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.domain.usecase.CompleteTODOUseCase
import com.crskdev.rxmvi.domain.usecase.SearchTODOUseCase
import com.crskdev.rxmvi.domain.usecase.WatchDeletedTODOUseCase
import com.crskdev.rxmvi.domain.usecase.WatchTODOUseCase
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvi.util.HighlightedCharSequence
import com.crskdev.rxmvi.util.SimpleHighlightedCharSequence
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.presentation.Carrier
import com.crskdev.rxmvicore.presentation.Dispatcher
import com.crskdev.rxmvicore.presentation.MVIController
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import javax.inject.Inject

/**
 * Created by Cristian Pela on 22.04.2018.
 */
class TODOSearchMVIController
@Inject constructor(
        private val searchTODOUseCase: SearchTODOUseCase,
        private val watchTODOUseCase: WatchTODOUseCase,
        private val watchTODODeletedUseCase: WatchDeletedTODOUseCase,
        private val completeTODOUseCase: CompleteTODOUseCase,
        private val converter: TODOModelViewEntityConverter
) : MVIController<SearchState>() {

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is CompleteTODOIntent -> CompleteTODORequest(it.id, it.complete)
                    is SearchTODOIntent -> CompositeRequest(
                            SearchTODORequest(it.textLike, it.fromStart),
                            WatchSearchTODORequest(it.textLike, false))
                    is WatchTODOIntent -> CompositeRequest(
                            WatchTODORequest(false),
                            WatchTODODeletedRequest(false))
                    is CancelIntent -> CompositeRequest(
                            WatchSearchTODORequest("", true),
                            WatchTODORequest(true),
                            WatchTODODeletedRequest(true))
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<Carrier<SearchState>, StatefulResponse, Carrier<SearchState>> =
            BiFunction { carrier, response ->
                when (response) {
                    is SearchTODOResponse -> {
                        val oldState = carrier.getState()
                        val responseTodos = response.todos.map { converter.convert(it) }
                        val todos = if (response.textLike != oldState.searchText) {
                            responseTodos
                        } else {
                            ((oldState.todos ?: emptyList()) + responseTodos).distinctBy { it.id }
                        }
                        carrier.next(response.source(), oldState.copy(
                                searchText = response.textLike,
                                todos = todos
                        ))
                    }
                    is WatchTODOResponse -> {
                        val oldState = carrier.getState()
                        val todo = response.todo
                                .let {
                                    it.copy(title = SimpleHighlightedCharSequence(it.title,
                                            oldState.searchText ?: ""))
                                }
                        val title = todo.title as HighlightedCharSequence
                        val isHighlighted = title.highlights().isNotEmpty()
                        val index = oldState.todos?.indexOfFirst { it.id == response.todo.id }
                                ?: -1
                        val todos =
                                if (isHighlighted) {
                                    oldState.todos?.let {
                                        if (index == -1) {
                                            it + todo.let { converter.convert(it) }
                                        } else {
                                            it.toMutableList().apply {
                                                set(index, todo.let { converter.convert(it) })
                                            }
                                        }
                                    }
                                } else {
                                    if (index >= 0) {
                                        oldState.todos?.toMutableList()?.apply { removeAt(index) }
                                    } else {
                                        oldState.todos
                                    }
                                }
                        carrier.next(response.source(), oldState.copy(todos = todos))
                    }
                    else -> carrier
                }
            }

    override fun defaultStateCarrier(): Carrier<SearchState> = Carrier.init(SearchState()) { _, curr, status, _ ->
        curr.copy(status = status)
    }

    override fun Dispatcher.useCaseDispatcher() {
        req<SearchTODORequest>() interactWith searchTODOUseCase
        req<WatchTODORequest>() interactWith watchTODOUseCase
        req<WatchTODODeletedRequest>() interactWith watchTODODeletedUseCase
        req<CompleteTODORequest>() interactWith completeTODOUseCase
    }
}

