package com.crskdev.rxmvi.presentation.search

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.env.SearchTODOIntent
import com.crskdev.rxmvi.domain.env.WatchTODOIntent
import com.crskdev.rxmvi.platform.app.conductor.controller.MVIConductorController
import com.crskdev.rxmvi.presentation.list.TODOListAdapter
import com.crskdev.rxmvi.util.EndlessRecyclerViewScrollListener
import com.crskdev.rxmvi.util.hideSoftInputKeyboard
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.presentation.IntentSender
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import com.crskdev.rxmvicore.util.cast
import com.crskdev.rxmvicore.util.singletonList
import com.jakewharton.rxbinding2.support.v7.widget.queryTextChangeEvents
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Cristian Pela on 20.04.2018.
 */
class TODOSearchConductorController : MVIConductorController<SearchState, MVIStreamOps<SearchState>>() {

    companion object {
        fun launch(router: Router) {
            val changeHandler = HorizontalChangeHandler()
            router.pushController(RouterTransaction
                    .with(TODOSearchConductorController())
                    .pushChangeHandler(changeHandler)
                    .popChangeHandler(changeHandler.copy())
            )
        }
    }


    override fun layout(): Int = R.layout.layout_todo_search

    override fun injectMe() {
        injector()?.component()?.inject(this)
    }

    private lateinit var adapter: TODOListAdapter

    private lateinit var searchView: SearchView

    override fun onAllowViewsToPrepare() {

        adapter = TODOListAdapter(router)

        R.id.toolbarSearch.v<Toolbar>()?.apply {
            inflateMenu(R.menu.menu_search_todo)
            setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
            setNavigationOnClickListener {
                activity?.hideSoftInputKeyboard()
                view?.post { router.popCurrentController() }
            }
            searchView = menu.findItem(R.id.search).actionView.cast<SearchView>().apply {
                isIconified = false
            }
        }

        R.id.recyclerTodos.v<RecyclerView>()?.apply {
            adapter = this@TODOSearchConductorController.adapter
            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager, 2) {
                override fun onLoadMore(page: Int, totalItemsCount: Int) {
                    postSendIntent(SearchTODOIntent(searchView.query.toString().trim(), false))
                }
            })
        }

    }

    override fun provideIntentStreams(): List<Observable<Intent>> =
            searchView.queryTextChangeEvents()
                    .debounce(150, TimeUnit.MILLISECONDS)
                    .map { SearchTODOIntent(it.queryText().toString().trim(), true) as Intent }
                    .singletonList() + adapter.observeCompletion()

    override fun renderState(state: SearchState) {
        state.todos?.apply {
            adapter.setTodos(this.map { it.highlight(view!!.context) })
        }

    }

    override fun onAllowSendIntent(intentSender: IntentSender, isFirstTime: Boolean) {
        intentSender.sendIntent(WatchTODOIntent)
    }

    @Inject
    @field:[Named("SearchTODOMVIStreamOps")]
    lateinit var injectedStreamOps: MVIStreamOps<SearchState>

    override val mviStreamOps: MVIStreamOps<SearchState> by lazy { MVIStreamOps.start(injectedStreamOps) }
}


