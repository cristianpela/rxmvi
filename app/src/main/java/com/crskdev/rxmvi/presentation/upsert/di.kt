package com.crskdev.rxmvi.presentation.upsert

import com.crskdev.rxmvi.presentation.upsert.add.AddTODOMVIController
import com.crskdev.rxmvi.presentation.upsert.edit.EditTODOMVIController
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import dagger.Binds
import dagger.Module
import javax.inject.Named

/**
 * .
 * Created by Cristian Pela on 17.03.2018.
 */
@Module
abstract class UpsertTODOModule{

    @Binds
    @Named("EditTODOMVIStreamOps")
    abstract fun provideEditTODOStreamOps(mviController: EditTODOMVIController): MVIStreamOps<UpsertTODOState>

    @Binds
    @Named("AddTODOMVIStreamOps")
    abstract fun provideAddTODOStreamOps(mviController: AddTODOMVIController): MVIStreamOps<UpsertTODOState>

}