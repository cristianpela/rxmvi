package com.crskdev.rxmvi.presentation.upsert.edit

import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.domain.usecase.*
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOState
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.presentation.*
import com.crskdev.rxmvicore.util.cast
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
class EditTODOMVIController
@Inject constructor(private val upsertTODOUseCase: UpsertTODOUseCase,
                    private val selectTODOUsecase: SelectTODOUsecase,
                    private val deleteTODOUseCase: DeleteTODOUseCase,
                    private val selectDueDateUseCase: SelectDueDateUseCase,
                    private val converter: TODOModelViewEntityConverter) : MVIController<UpsertTODOState>() {


    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    GoBackIntent -> GoBackRequest
                    is UpsertTODOIntent -> UpsertTODORequest(it.todo.let { converter.revert(it) }, it.mode)
                    is DeleteTODOIntent -> DeleteTODORequest(it.id)
                    is SelectTODOIntent -> SelectTODORequest(it.id)
                    is SelectDueDateIntent -> SelectDueDateRequest(it.date, it.isCompleted)
                    is SyncStateIntent -> SyncStateRequest(it.state)
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<Carrier<UpsertTODOState>, StatefulResponse, Carrier<UpsertTODOState>> =
            BiFunction { carrier, response ->
                when (response) {
                    is SelectTODOResponse -> carrier.next(response.source(),
                            UpsertTODOState(Status.IDLE, response.todo.let { converter.convert(it) }.copy(isEdited = false)), NONE)
                    is SelectDueDateResponse -> {
                        carrier.next(response.source(),
                                UpsertTODOState(Status.IDLE, carrier.getState().todo?.let {
                                    converter.updateDue(it, response.dateFormatted).copy(isEdited = true)
                                }), NONE)
                    }
                    is SyncStateResponse ->
                        carrier.next(response.source(),
                                UpsertTODOState(Status.IDLE,
                                        response.state.cast<TODOModelViewEntity>().let {
                                            val isEdited = if (it.isEdited) true else it != carrier.getState().todo
                                            val copy = it.copy(isEdited = isEdited)
                                            converter.reCheckDueDateIfNeeded(copy)
                                        }), NONE)
                    else -> carrier
                }
            }

    override fun defaultStateCarrier(): Carrier<UpsertTODOState> =
            Carrier.init(UpsertTODOState()) { _, curr, status, _ -> UpsertTODOState(status, curr.todo) }

    override fun Dispatcher.useCaseDispatcher() {
        req<UpsertTODORequest>() interactWith upsertTODOUseCase
        req<DeleteTODORequest>() interactWith deleteTODOUseCase
        req<SelectTODORequest>() interactWith selectTODOUsecase
        req<SelectDueDateRequest>() interactWith selectDueDateUseCase
        req<SyncStateRequest>() interactWith syncStateUseCase
        req<GoBackRequest>() interactWith GoBackUseCase
    }
}