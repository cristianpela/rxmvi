package com.crskdev.rxmvi.presentation.search

import com.crskdev.rxmvicore.presentation.MVIStreamOps
import dagger.Binds
import dagger.Module
import javax.inject.Named

/**
 * Created by Cristian Pela on 22.04.2018.
 */
@Module
abstract class TODOSearchModule {

    @Binds
    @Named("SearchTODOMVIStreamOps")
    abstract fun provideListTODOStreamOps(mviController: TODOSearchMVIController): MVIStreamOps<SearchState>

}