package com.crskdev.rxmvi.presentation.upsert.edit

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.crskdev.rxmvi.domain.env.DeleteTODOMessage
import com.crskdev.rxmvi.domain.env.SelectTODOIntent
import com.crskdev.rxmvi.platform.app.conductor.controller.MVIConductorController
import com.crskdev.rxmvi.presentation.upsert.Mode
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOControllerDelegate
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOState
import com.crskdev.rxmvi.util.bundleOf
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.Message
import com.crskdev.rxmvicore.env.ResourceIDError
import com.crskdev.rxmvicore.env.SyncStateIntent
import com.crskdev.rxmvicore.presentation.IntentSender
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import com.crskdev.rxmvicore.util.singletonList
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named

/**
 * .
 * Created by Cristian Pela on 16.03.2018.
 */
class EditTODOConductorController(args: Bundle? = null) :
        MVIConductorController<UpsertTODOState, MVIStreamOps<UpsertTODOState>>(args) {

    @Inject
    @field:[Named("EditTODOMVIStreamOps")]
    lateinit var injectedStreamOps: MVIStreamOps<UpsertTODOState>

    private val delegate = UpsertTODOControllerDelegate(this, Mode.EDIT)

    companion object {

        private const val KEY_TODO_ID = "KEY_TODO_ID"

        fun launch(router: Router, id: Long) {
            router.pushController(RouterTransaction
                    .with(EditTODOConductorController(bundleOf {
                        putLong(KEY_TODO_ID, id)
                    }))
                    .pushChangeHandler(HorizontalChangeHandler())
                    .popChangeHandler(HorizontalChangeHandler())
            )
        }
    }

    override fun onAllowViewsToPrepare() = delegate.onAllowViewsToPrepare()

    override val mviStreamOps: MVIStreamOps<UpsertTODOState>  by lazy {
        MVIStreamOps.start(injectedStreamOps)
    }

    override fun onAllowSendIntent(intentSender: IntentSender, isFirstTime: Boolean) {
    }

    override fun onArgumentsPassed(args: Map<String, Any>, isFirstTime: Boolean) {
        if (isFirstTime) {
            args[KEY_TODO_ID]?.let {
                postSendIntent(SelectTODOIntent(it as Long))
            }
        }
    }

    override fun renderState(state: UpsertTODOState) {
        delegate.renderState(state)

    }

    override fun layout(): Int = delegate.layout()

    override fun injectMe() {
        injector()?.component()?.inject(this)
    }

    override fun renderErrors(err: Throwable) {
        if (err is ResourceIDError) {
            err.idMessage.data.forEach {
                delegate.renderErrors(it)
            }
        } else {
            super.renderErrors(err)
        }
    }

    override fun renderMessages(message: Message<*>) {
        if(!delegate.renderMessage(message)){
            super.renderMessages(message)
        }
    }

    override fun onDestroyView(view: View) {
        postSendIntent(SyncStateIntent(delegate.gatherModel() as Any))
    }

    override fun handleBack(): Boolean {
        delegate.goBack()
        return true
    }
}