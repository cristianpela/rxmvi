package com.crskdev.rxmvi.presentation.search

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.util.HighlightedCharSequence
import com.crskdev.rxmvi.util.contrastWithBackgroundColor
import com.crskdev.rxmvi.util.getColorCompat

/**
 * Created by Cristian Pela on 20.04.2018.
 */
@Suppress("UnnecessaryVariable")
fun TODOModelViewEntity.highlight(
        context: Context): TODOModelViewEntity {

    val highlightTitle = if (title is HighlightedCharSequence) {

        val background = context.getColorCompat(dueDateColor)
        val highlightBg = context.getColorCompat(R.color.colorHighlight)
        val highlightBgInv = context.getColorCompat(R.color.colorHighlight_Inverse)

        val highlightFore = background
        val highlightInvFore = highlightBgInv

        val contrastBackground = contrastWithBackgroundColor(highlightBg, background, highlightBgInv)
        val contrastForeground = contrastWithBackgroundColor(highlightFore, contrastBackground, highlightInvFore)

        val spanTitle = SpannableStringBuilder(title)
        title.highlights().forEach {
            spanTitle.setSpan(ForegroundColorSpan(contrastForeground), it.start, it.end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            spanTitle.setSpan(BackgroundColorSpan(contrastBackground), it.start, it.end, Spannable.SPAN_INCLUSIVE_EXCLUSIVE)
        }
        spanTitle
    } else title

    return this.copy(title = highlightTitle)
}

