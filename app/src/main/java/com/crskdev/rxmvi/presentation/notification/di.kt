package com.crskdev.rxmvi.presentation.notification

import android.content.Context
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.domain.usecase.FetchTODOsUseCase
import com.crskdev.rxmvi.platform.app.RepoProxy
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.evernote.android.job.Job
import dagger.BindsInstance
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

/**
 * Created by Cristian Pela on 24.04.2018.
 */
@Subcomponent(modules = [(NotificationDueModule::class)])
interface NotificationDueComponent {

    fun inject(job: NotificationDueJob)

    @Subcomponent.Builder
    interface Builder {

        fun build(): NotificationDueComponent

        @BindsInstance
        fun bindNotificationDueJobResult(result: NotificationDueJobResult): Builder
    }

}


@Module
class NotificationDueModule {

    @Provides
    fun providePresenter(
            result: NotificationDueJobResult,
            @RepoProxy repository: TODORepository,
            converter: TODOModelViewEntityConverter
    ): NotificationDueMVP.NotificationDuePresenter =
            NotificationDuePresenterImpl(result, repository, converter)


    @Provides
    fun provideView(
            context: Context,
            presenter: NotificationDueMVP.NotificationDuePresenter
    ): NotificationDueMVP.NotificationDueView =
            NotificationDueViewImpl(context, presenter).apply { postInject() }
}


/// Processor DI
@Subcomponent(modules = [(NotificationDueProcessorModule::class)])
interface NotificationDueProcessorComponent {

    fun inject(processor: NotificationDueActionProcessor)

    @Subcomponent.Builder
    interface Builder {
        fun build(): NotificationDueProcessorComponent
    }
}

@Module
class NotificationDueProcessorModule {

    @Provides
    fun provideNotificationDueActionDispatcher(@RepoProxy repository: TODORepository)
            : NotificationDueActionDispatcher =
            NotificationDueActionDispatcherImpl(repository)

}