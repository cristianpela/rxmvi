package com.crskdev.rxmvi.presentation.list

import android.graphics.PorterDuff
import android.support.v4.graphics.ColorUtils
import android.support.v4.widget.CompoundButtonCompat
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import com.bluelinelabs.conductor.Router
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.env.CompleteTODOIntent
import com.crskdev.rxmvi.domain.env.DeleteTODOIntent
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.presentation.upsert.edit.EditTODOConductorController
import com.crskdev.rxmvi.util.getColorCompat
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.util.cast
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.reactivex.rxkotlin.cast
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 20.03.2018.
 */
class TODOListAdapter(private val router: Router) : ListAdapter<TODOModelViewEntity, TodoVH>(TodoDiffUtilCallback) {

    private val intentSubject = PublishSubject.create<Intent>()

    init {
        setHasStableIds(true)
    }

    fun setTodos(list: List<TODOModelViewEntity>) {
        submitList(list)
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoVH =
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_item_todo_list, parent, false)
                    .let { TodoVH(it) }
                    .apply {
                        RxView.clicks(itemView)
                                .takeUntil(RxView.detaches(parent))
                                .map { itemView.tag }
                                .filter { itemView.tag is Long }
                                .cast<Long>()
                                .subscribe {
                                    EditTODOConductorController.launch(router, it)
                                }
                    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(holder: TodoVH, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            val todo = getItem(position)
            with(holder) {
                payloads.map { it as List<Int> }.flatten().forEach {
                    when (it) {
                        TodoDiffUtilCallback.PAYLOAD_CHANGED_TITLE -> textTitle.text = todo.title
                        TodoDiffUtilCallback.PAYLOAD_CHANGED_COLOR -> updateBackgroundColor(todo)
                        TodoDiffUtilCallback.PAYLOAD_CHANGED_COMPLETED -> updateCheckCompleted(todo)
                        TodoDiffUtilCallback.PAYLOAD_CHANGED_DUE_DATE -> textDue.text = todo.dueDateFormatted
                    }
                }
            }
        }
    }

    override fun onBindViewHolder(holder: TodoVH, position: Int) {
        val todo = getItem(position)
        with(holder) {
            textTitle.text = todo.title
            textAuthor.text = todo.author
            textCreation.text = todo.creationDateFrmt
            textDue.text = todo.dueDateFormatted
            updateCheckCompleted(todo)
            updateDeleteButton(todo)
            updateBackgroundColor(todo)
            Unit
        }

    }

    private fun TodoVH.updateDeleteButton(todo: TODOModelViewEntity) {
        deleteButton.tag = todo.id
        deleteButton.setOnClickListener { v ->
            val adapteeId = getItem(adapterPosition).id
            val taggedId = v.tag as Long
            if (adapteeId == taggedId) {
                intentSubject.onNext(DeleteTODOIntent(adapteeId))
            }
        }
    }

    private fun TodoVH.updateCheckCompleted(todo: TODOModelViewEntity) {
        checkCompleted.isChecked = todo.completed
        checkCompleted.tag = todo.id
        checkCompleted.setOnCheckedChangeListener { v, isChecked ->
            //we make sure the checkbox tag id is the same as adapter holder id
            //because of recycling these ids might be different
            val adapteeId = getItem(adapterPosition).id
            val taggedId = v.tag as Long
            if (adapteeId == taggedId) {
                intentSubject.onNext(CompleteTODOIntent(adapteeId, isChecked))
            }
        }
    }

    private fun TodoVH.updateBackgroundColor(todo: TODOModelViewEntity): Unit? {
        val context = itemView.context
        val dueColor = itemView.context.getColorCompat(todo.dueDateColor)
        itemView.cast<CardView>().setCardBackgroundColor(dueColor)
        itemView.tag = todo.id
        return if (ColorUtils.calculateLuminance(dueColor) < 0.5) {
            val white = context.getColorCompat(android.R.color.white)
            textTitle.setTextColor(white)
            textAuthor.setTextColor(white)
            textCreation.setTextColor(white)
            textDue.setTextColor(white)
            deleteButton.setImageResource(R.drawable.ic_delete_white_24dp)
            CompoundButtonCompat
                    .getButtonDrawable(checkCompleted)
                    ?.setColorFilter(white, PorterDuff.Mode.SRC_ATOP)
        } else {
            val titleColor = itemView.context.getColorCompat(R.color.colorSecondary)
            val textColor = itemView.context.getColorCompat(android.R.color.primary_text_light)
            textTitle.setTextColor(titleColor)
            textAuthor.setTextColor(textColor)
            textCreation.setTextColor(textColor)
            textDue.setTextColor(textColor)
            deleteButton.setImageResource(R.drawable.ic_delete_black_24dp)
            deleteButton.drawable!!.setColorFilter(context.getColorCompat(R.color.colorAccent),
                            PorterDuff.Mode.SRC_ATOP)
            CompoundButtonCompat
                    .getButtonDrawable(checkCompleted)
                    ?.setColorFilter(context.getColorCompat(R.color.colorSecondary),
                            PorterDuff.Mode.SRC_ATOP)
        }
    }

    fun observeCompletion(): Observable<Intent> = intentSubject
            .throttleFirst(500, TimeUnit.MILLISECONDS)
}

object TodoDiffUtilCallback : DiffUtil.ItemCallback<TODOModelViewEntity>() {

    internal const val PAYLOAD_CHANGED_TITLE = 0

    internal const val PAYLOAD_CHANGED_COLOR = 2

    internal const val PAYLOAD_CHANGED_DUE_DATE = 4

    internal const val PAYLOAD_CHANGED_COMPLETED = 8

    override fun areItemsTheSame(oldItem: TODOModelViewEntity, newItem: TODOModelViewEntity): Boolean =
            oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: TODOModelViewEntity, newItem: TODOModelViewEntity): Boolean =
            oldItem == newItem

    override fun getChangePayload(oldItem: TODOModelViewEntity, newItem: TODOModelViewEntity): Any? {

        val changes = mutableListOf<Int>()

        if (oldItem.title != newItem.title) {
            changes.add(PAYLOAD_CHANGED_TITLE)
        }

        if (oldItem.dueDateColor != newItem.dueDateColor) {
            changes.add(PAYLOAD_CHANGED_COLOR)
        }

        if (oldItem.dueDateFormatted != newItem.dueDateFormatted) {
            changes.add(PAYLOAD_CHANGED_DUE_DATE)
        }

        if (oldItem.completed != newItem.completed) {
            changes.add(PAYLOAD_CHANGED_COMPLETED)
        }

        return changes.takeIf { it.isNotEmpty() }
    }
}

class TodoVH(view: View) : RecyclerView.ViewHolder(view) {
    val textTitle = view.findViewById<TextView>(R.id.textTodoTitle)!!
    val textAuthor = view.findViewById<TextView>(R.id.textTODOAuthor)!!
    val textDue = view.findViewById<TextView>(R.id.textTODODueDate)!!
    val textCreation = view.findViewById<TextView>(R.id.textTODOCreationDate)!!
    val checkCompleted = view.findViewById<CheckBox>(R.id.checkTODOBox)!!
    val deleteButton = view.findViewById<ImageButton>(R.id.imageButtonDelete)!!
}