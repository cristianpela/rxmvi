package com.crskdev.rxmvi.presentation.search

import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvicore.presentation.State
import com.crskdev.rxmvicore.presentation.Status

data class SearchState(override val status: Status = Status.IDLE, val todos: List<TODOModelViewEntity>? = null, val searchText: String? = null) : State