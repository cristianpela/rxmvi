package com.crskdev.rxmvi.presentation.aggregate

import com.crskdev.rxmvicore.presentation.MVIStreamOps
import dagger.Binds
import dagger.Module
import javax.inject.Named

/**
 * .
 * Created by Cristian Pela on 17.03.2018.
 */
@Module
abstract class AggregatorModule{

    @Binds
    @Named("AggregatorMVIStreamOps")
    abstract fun provideEditTODOStreamOps(mviController: AggregatorMVIController): MVIStreamOps<AggregatorState>

}