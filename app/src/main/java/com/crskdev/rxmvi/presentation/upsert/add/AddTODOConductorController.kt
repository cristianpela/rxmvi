package com.crskdev.rxmvi.presentation.upsert.add

import android.support.v7.app.AlertDialog
import android.support.v7.widget.AppCompatEditText
import android.view.View
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.env.GetUsernameIntent
import com.crskdev.rxmvi.domain.env.SaveUsernameIntent
import com.crskdev.rxmvi.platform.app.conductor.controller.MVIConductorController
import com.crskdev.rxmvi.presentation.upsert.Mode
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOControllerDelegate
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOState
import com.crskdev.rxmvi.util.asController
import com.crskdev.rxmvi.util.dpToPx
import com.crskdev.rxmvi.util.hideSoftInputKeyboard
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.Message
import com.crskdev.rxmvicore.env.ResourceIDError
import com.crskdev.rxmvicore.env.SyncStateIntent
import com.crskdev.rxmvicore.presentation.IntentSender
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import com.crskdev.rxmvicore.util.singletonList
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Named

/**
 * .
 * Created by Cristian Pela on 28.03.2018.
 */
class AddTODOConductorController : MVIConductorController<UpsertTODOState, MVIStreamOps<UpsertTODOState>>() {

    companion object {
        fun launch(router: Router) {
            router.pushController(RouterTransaction
                    .with(AddTODOConductorController())
                    .pushChangeHandler(HorizontalChangeHandler())
                    .popChangeHandler(HorizontalChangeHandler())
            )
        }
    }

    @Inject
    @field:[Named("AddTODOMVIStreamOps")]
    lateinit var injectedStreamOps: MVIStreamOps<UpsertTODOState>

    override val mviStreamOps: MVIStreamOps<UpsertTODOState> by lazy { MVIStreamOps.start(injectedStreamOps) }

    private val delegate = UpsertTODOControllerDelegate(this, Mode.ADD)

    override fun onAllowViewsToPrepare() {
        delegate.onAllowViewsToPrepare()
    }

    override fun layout(): Int = delegate.layout()

    override fun renderState(state: UpsertTODOState) {
        delegate.renderState(state)
    }

    override fun renderErrors(err: Throwable) {
        if (err is ResourceIDError) {
            err.idMessage.data.forEach {
                when (it.id) {
                    R.string.res_err_username_not_found -> showUserNameInputDialog()
                    else -> delegate.renderErrors(it)
                }
            }
        } else {
            super.renderErrors(err)
        }

    }

    override fun renderMessages(message: Message<*>) {
        if(!delegate.renderMessage(message))
            super.renderMessages(message)
    }

    private fun showUserNameInputDialog() {
        activity?.apply {
            val editText = AppCompatEditText(this)
            val padding = 8.dpToPx()
            AlertDialog.Builder(this)
                    .setTitle(getString(R.string.author))
                    .setMessage(getString(R.string.msg_input_dialog_username))
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        hideSoftInputKeyboard()
                        this@AddTODOConductorController
                                .postSendIntent(SaveUsernameIntent(editText.text.toString()))
                    }
                    .setNegativeButton(android.R.string.cancel) { _, _ ->
                        //current controller is the dialog, as long as this dialog is adapted to be a controller
                        this@AddTODOConductorController.router.popCurrentController()
                    }
                    .setOnCancelListener {
                        //current controller is the dialog, as long as this dialog is adapted to be a controller
                        this@AddTODOConductorController.router.popCurrentController()
                    }
                    .setCancelable(true)
                    .create()
                    .apply {
                        setView(editText, padding, padding, padding, padding)
                    }
                    .asController()
                    .show(router)
        }
    }



    override fun onAllowSendIntent(intentSender: IntentSender, isFirstTime: Boolean) {
        if (isFirstTime) {
            intentSender.sendIntent(GetUsernameIntent)
        }
    }

    override fun injectMe() {
        injector()?.component()?.inject(this)
    }


    override fun handleBack(): Boolean {
        delegate.goBack()
        return true
    }

    override fun onDestroyView(view: View) {
        postSendIntent(SyncStateIntent(delegate.gatherModel() as Any))
    }
}

