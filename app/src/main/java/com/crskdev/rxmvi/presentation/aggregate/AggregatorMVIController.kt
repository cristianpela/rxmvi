package com.crskdev.rxmvi.presentation.aggregate

import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.domain.usecase.AggregatorTODOUseCase
import com.crskdev.rxmvi.domain.usecase.ChangeFilterUseCase
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.presentation.*
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import java.util.*
import javax.inject.Inject

/**.
 * Created by Cristian Pela on 26.03.2018.
 */
class AggregatorMVIController
@Inject constructor(
        private val aggregatorTODOUseCase: AggregatorTODOUseCase,
        private val changeFilterUseCase: ChangeFilterUseCase
) : MVIController<AggregatorState>() {

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is ChangeFilterIntent -> ChangeFilterRequest(it.filter)
                    WatchAggregateIntent -> WatchAggregateRequest(false)
                    CancelIntent -> WatchAggregateRequest(true)
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<Carrier<AggregatorState>, StatefulResponse, Carrier<AggregatorState>> =
            BiFunction { carrier, response ->
                when (response) {
                    is WatchAggregateResponse -> carrier
                            .next(response.source(), AggregatorState(aggregation = response.aggregates))
                    else -> carrier
                }
            }

    override fun defaultStateCarrier(): Carrier<AggregatorState> =
            Carrier.init(AggregatorState()) { _, curr, status, _ ->
                AggregatorState(status, curr.aggregation)
            }

    override fun Dispatcher.useCaseDispatcher() {
        req<WatchAggregateRequest>() interactWith aggregatorTODOUseCase
        req<ChangeFilterRequest>() interactWith changeFilterUseCase
    }

}

class AggregatorState(override val status: Status = Status.IDLE, val aggregation: EnumMap<TODOFilter, Int>? = null) : State {

}