package com.crskdev.rxmvi.presentation.list

import com.crskdev.rxmvi.domain.PAGE_FILTER_UNKNOWN
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvicore.presentation.State
import com.crskdev.rxmvicore.presentation.Status
import com.crskdev.rxmvicore.util.Page

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
data class TODOListState(override val status: Status = Status.IDLE,
                         val filter: TODOFilter = TODOFilter.UNKNOWN,
                         val todos: List<TODOModelViewEntity>? = null) : State