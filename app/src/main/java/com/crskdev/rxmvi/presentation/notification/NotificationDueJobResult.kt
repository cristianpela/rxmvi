package com.crskdev.rxmvi.presentation.notification

import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created by Cristian Pela on 24.04.2018.
 */
interface NotificationDueJobResult {

    fun waitForComplete(): Boolean

    fun signal(done: Boolean)

}

class FutureNotificationDueJobResult(private val blockingThreadName: String) : NotificationDueJobResult {

    private val completed = AtomicBoolean(false)

    private val latch = CountDownLatch(1)

    override fun waitForComplete(): Boolean = latch.await().let { completed.get() }

    override fun signal(done: Boolean) {
        val signalThreadName = Thread.currentThread().name
        if(signalThreadName == blockingThreadName)
            throw Error("Signaling must be done in different thread!")
        latch.countDown()
        completed.compareAndSet(false, done)
    }

}