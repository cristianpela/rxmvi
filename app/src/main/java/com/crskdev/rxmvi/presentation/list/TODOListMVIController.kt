package com.crskdev.rxmvi.presentation.list

import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.env.*
import com.crskdev.rxmvi.domain.usecase.*
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvicore.env.*
import com.crskdev.rxmvicore.presentation.*
import com.crskdev.rxmvicore.util.singletonList
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
class TODOListMVIController
@Inject constructor(private val converter: TODOModelViewEntityConverter,
                    private val fetchTODOsUseCase: FetchTODOsUseCase,
                    private val filterSaveUseCase: FilterSaveUseCase,
                    private val watchTODOUseCase: WatchTODOUseCase,
                    private val watchTODODeletedUseCase: WatchDeletedTODOUseCase,
                    private val completeTODOUsecase: CompleteTODOUseCase,
                    private val deleteTODOUseCase: DeleteTODOUseCase,
                    private val watchChangeFilterUseCase: WatchChangeFilterUseCase,
                    private val changeFilterUseCase: ChangeFilterUseCase) : MVIController<TODOListState>() {

    override fun intentToRequestMapper(): Function<Intent, Request> =
            Function {
                when (it) {
                    is FetchTODOsIntent -> FetchTODOsRequest(it.filter, it.fromStart)
                    is SaveFilterIntent -> SaveFilterRequest(it.filter)
                    is CompleteTODOIntent -> CompleteTODORequest(it.id, it.complete)
                    is ChangeFilterIntent -> ChangeFilterRequest(it.filter)
                    is DeleteTODOIntent -> DeleteTODORequest(it.id)
                    WatchTODOIntent -> WatchTODORequest(false)
                    WatchChangeFilterIntent -> WatchChangeFilterRequest(false)
                    WatchTODODeletedIntent -> WatchTODODeletedRequest(false)
                //NOTE: i don't want to cancel the watchers when detach from view, let'em work in background
                    CancelIntent -> CompositeRequest(
                            WatchChangeFilterRequest(true),
                            WatchTODORequest(true),
                            WatchTODODeletedRequest(true)
                    )
                    else -> NoRequest
                }
            }

    override fun reducer(): BiFunction<Carrier<TODOListState>, StatefulResponse, Carrier<TODOListState>> =
            BiFunction { carrier, response ->
                when (response) {
                    is FetchTODOsResponse -> carrier.next(response.source(),
                            TODOListState(filter = response.filter,
                                    todos = response.todos.map { converter.convert(it) }),
                            Actions.FETCH)
                    is WatchTODOResponse -> carrier.next(response.source(),
                            carrier.getState().copy(todos = response.todo.let { converter.convert(it) }.singletonList()),
                            Actions.WATCH)

                    is WatchTODODeletedResponse -> carrier.next(response.source(),
                            carrier.getState().copy(todos = excludeDeletedTodos(carrier.getState().todos, response.ids)))
                    else -> carrier
                }
            }

    private fun excludeDeletedTodos(todos: List<TODOModelViewEntity>?, ids: List<Long>): List<TODOModelViewEntity>? =
            todos?.let { list ->
                val filtered = list.toMutableList()
                ids.forEach { id ->
                    val index = filtered.indexOfFirst { it.id == id }
                    if (index >= 0) {
                        filtered.removeAt(index)
                    }
                }
                filtered
            }

    override fun defaultStateCarrier(): Carrier<TODOListState> =
            Carrier.init(TODOListState(), StateCarrierHandler.provideAccumulator())

    //TODO: for now will disable this feature
//    override fun diffState(): (oldState: TODOListState?, newState: TODOListState) -> TODOListState = { old, new ->
//        TODOListState(
//                new.status,
//                new.page,
//                pickRightWhenDiff(old?.todos, new.todos))
//    }

    override fun Dispatcher.useCaseDispatcher() {
        req<FetchTODOsRequest>() interactWith fetchTODOsUseCase
        req<SaveFilterRequest>() interactWith filterSaveUseCase
        req<WatchTODORequest>() interactWith watchTODOUseCase
        req<CompleteTODORequest>() interactWith completeTODOUsecase
        req<ChangeFilterRequest>() interactWith changeFilterUseCase
        req<WatchChangeFilterRequest>() interactWith watchChangeFilterUseCase
        req<WatchTODODeletedRequest>() interactWith watchTODODeletedUseCase
        req<DeleteTODORequest>() interactWith deleteTODOUseCase
    }
}

enum class Actions : Action {
    FETCH, WATCH
}

object StateCarrierHandler {

    fun provideAccumulator(): (acc: TODOListState, curr: TODOListState, status: Status, action: Action) -> TODOListState =
            { acc, curr, status, action ->
                if (action is Actions) {
                    val newTodos: List<TODOModelViewEntity>? =
                            when (action) {
                                Actions.FETCH -> {
                                    val a: List<TODOModelViewEntity> = acc.todos ?: emptyList()
                                    val c: List<TODOModelViewEntity> = curr.todos ?: emptyList()
                                    if (acc.filter == curr.filter) {
                                        (a + c).distinctBy { it.id }
                                    } else {
                                        c
                                    }
                                }
                                Actions.WATCH -> {
                                    //we putting details to null because the listed todos are displaying the details
                                    //if we let detail to creep app will crash
                                    //because recycler view is set to have stable ids
                                    val todo = curr.todos?.get(0)!!.copy(detail = null)
                                    acc.todos?.let { a ->
                                        val mutA = a.toMutableList()
                                        val index = a.indexOfFirst { it.id == todo.id }
                                        val filter = acc.filter
                                        if (todo.completed) {
                                            if (index >= 0) {
                                                if (filter == TODOFilter.NOT_COMPLETED
                                                        || filter == TODOFilter.PASSED_DUE
                                                        || filter == TODOFilter.ABOUT_DUE) {
                                                    mutA.removeAt(index)
                                                } else if (filter == TODOFilter.COMPLETED && mutA[index] != todo) {
                                                    mutA.add(todo)
                                                } else {
                                                    mutA[index] = todo
                                                }
                                            }
                                        } else {
                                            if (index >= 0) {
                                                if (filter == TODOFilter.COMPLETED ||
                                                        (filter == TODOFilter.ABOUT_DUE && !todo.isAboutDue)) {
                                                    mutA.removeAt(index)
                                                } else {
                                                    mutA[index] = todo
                                                }
                                            } else {
                                                if (filter != TODOFilter.COMPLETED
                                                        && filter != TODOFilter.ABOUT_DUE) {
                                                    mutA.add(todo)
                                                } else if (filter == TODOFilter.ABOUT_DUE
                                                        && todo.isAboutDue) {
                                                    mutA.add(todo)
                                                }
                                            }
                                        }
                                        mutA.apply { sortByDescending { it.creationDate } }
                                    }
                                }
                            }

                    val filter = when (action) {
                        Actions.FETCH -> curr.filter
                        Actions.WATCH -> acc.filter
                    }
                    TODOListState(status, filter, newTodos)
                } else {
                    curr.copy(status = status)
                }
            }

}