package com.crskdev.rxmvi.util


/**
 * Created by Cristian Pela on 23.04.2018.
 */
@Suppress("unused")
abstract class HighlightedCharSequence(val source: CharSequence,
                                       val highlight: String,
                                       val ignoreCase: Boolean = true) : CharSequence by source {

    abstract fun highlights(): List<HighlightedCharSequence.Highlight>

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HighlightedCharSequence

        if (source != other.source || highlights() != other.highlights()) return false

        return true
    }

    override fun hashCode(): Int {
        val hash = 7
        return hash + (31 * hash + source.hashCode()) + highlights().sumBy { 31 * hash + it.hashCode() }
    }

    /**
     * [start] and [end] of the highlight in [source].
     *
     * [start] is inclusive -
     * [end] is exclusive
     */
    data class Highlight(val start: Int, val end: Int)


}