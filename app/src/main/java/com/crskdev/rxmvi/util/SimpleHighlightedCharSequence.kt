package com.crskdev.rxmvi.util

/**
 * Marks in a list where [highlight] appears in the provided [source]
 *
 *
 * Note: this is naive string search algorithm
 *
 *
 * Created by Cristian Pela on 19.04.2018.
 */
class SimpleHighlightedCharSequence(source: CharSequence, highlight: String,
        ignoreCase: Boolean = true): HighlightedCharSequence(source, highlight, ignoreCase) {


    private val highlights = mutableListOf<HighlightedCharSequence.Highlight>()

    init {
        if (highlight.isNotEmpty() && source.isNotEmpty()) {
            var start = 0
            var hasStarted = false
            var count = 0
            for (c in 0 until source.length) {
                if (source[c].equals(highlight[count], ignoreCase)) {
                    if (!hasStarted) {
                        hasStarted = true
                        start = c
                    }
                    if (count == highlight.length - 1) {
                        //we reached the end of highlight
                        val end = start + count + 1
                        highlights.add(HighlightedCharSequence.Highlight(start, end))
                        //reset
                        hasStarted = false
                        count = 0
                    } else {
                        count += 1
                    }
                } else {
                    //reset
                    hasStarted = false
                    count = 0
                }
            }
        }
    }

    override fun highlights(): List<HighlightedCharSequence.Highlight> = highlights

}