package com.crskdev.rxmvi.util

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CoordinatorLayout
import android.util.AttributeSet
import android.view.View
import android.widget.TextView


/**
 * .
 * Created by Cristian Pela on 22.03.2018.
 */
class ToolbarTitleTextBehaviour(context: Context, attrs: AttributeSet)
    : CoordinatorLayout.Behavior<TextView>(context, attrs) {

    init {

    }

    override fun layoutDependsOn(parent: CoordinatorLayout, child: TextView, dependency: View): Boolean {
        return dependency is AppBarLayout
    }


    override fun onDependentViewChanged(parent: CoordinatorLayout, child: TextView, dependency: View): Boolean {
        return false
    }


}

