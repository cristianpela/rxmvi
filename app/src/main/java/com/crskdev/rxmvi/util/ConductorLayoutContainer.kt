package com.crskdev.rxmvi.util

import android.view.View
import com.bluelinelabs.conductor.Controller
import com.crskdev.rxmvicore.util.cast
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.*

/**
 * Created by Cristian Pela on 27.03.2018.
 */
class ConductorLayoutContainer(private val controller: Controller) : LayoutContainer {

    override val containerView: View? by lazy { controller.view }

    init {
        controller.addLifecycleListener(object:Controller.LifecycleListener(){
            override fun postDestroyView(controller: Controller) {
                controller.cast<LayoutContainer>().clearFindViewByIdCache()
            }
        })

    }
}