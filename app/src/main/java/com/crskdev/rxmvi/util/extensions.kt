package com.crskdev.rxmvi.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.annotation.*
import android.support.constraint.ConstraintLayout
import android.support.constraint.Group
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.ColorUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.bluelinelabs.conductor.Controller
import com.crskdev.rxmvi.platform.TimeProvider
import com.crskdev.rxmvicore.util.cast
import java.util.*
import kotlin.math.roundToInt

/**
 * .
 *
 * Created by Cristian Pela on 20.03.2018.
 */
fun Context.getColorCompat(@ColorRes id: Int): Int = ContextCompat.getColor(this, id)

inline fun Context.obtainStyledAttributesExt(set: AttributeSet? = null, @StyleableRes attrs: IntArray, @AttrRes defStyleAttr: Int = 0,
                                             @StyleRes defStyleRes: Int = 0, aLamda: TypedArray.() -> Unit): Unit {
    with(obtainStyledAttributes(set, attrs, defStyleAttr, defStyleRes)) {
        aLamda()
        recycle()
    }
}

fun Int.toPx(resources: Resources, unit: Int): Float =
        TypedValue.applyDimension(unit, this.toFloat(), resources.displayMetrics)

fun Int.dpToPx(resources: Resources): Int = toPx(resources, TypedValue.COMPLEX_UNIT_DIP).roundToInt()
fun Int.spToPx(resources: Resources): Float = toPx(resources, TypedValue.COMPLEX_UNIT_SP)


fun Bundle?.toMap(): Map<String, Any> =
        mutableMapOf<String, Any>().apply {
            this@toMap?.keySet()?.forEach {
                this[it] = this@toMap.get(it)
            }
        }

inline fun bundleOf(bundle: Bundle.() -> Unit): Bundle =
        Bundle().apply(bundle)

fun <T : View> Controller.id(@IdRes id: Int): T? = view?.findViewById(id) as T

fun String.isEmptyOrBlank(): Boolean = isEmpty() || isBlank()

fun Dialog.asController(args: Bundle? = null): DialogController {
    //workaround for using kotlin's anonymous class
    //using anon class crashes app because kotlin is adding arguments to constructor,
    //making it unaligned with conductor's controller arguments specification
    //see Controller#ensureRequiredConstructor
    class AlignedDialogController(args: Bundle? = null) : DialogController(args) {
        lateinit var wrappedDialog: Dialog
        override fun onCreateDialog(savedViewState: Bundle?): Dialog = wrappedDialog
    }
    return AlignedDialogController(args).apply { wrappedDialog = this@asController }
}


fun Activity.hideSoftInputKeyboard() {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    this.currentFocus?.let {
        imm.hideSoftInputFromWindow(it.windowToken, 0)
    }

}

fun contrastWithBackgroundColor(@ColorInt color: Int, @ColorInt backgroundColor: Int,
                                   @ColorInt inverseColor: Int): Int {
    return if (ColorUtils.calculateLuminance(backgroundColor) < 0.5) {
        inverseColor
    } else {
        color
    }
}

fun TextView.setTextColorInContrast(@ColorInt color: Int, @ColorInt inverseColor: Int = Color.WHITE) {
    val contrastColor: Int = if (background is ColorDrawable) {
        val backgroundColor = background.cast<ColorDrawable>().color
        if (ColorUtils.calculateLuminance(backgroundColor) < 0.5) {
            inverseColor
        } else {
            color
        }
    } else {
        //fall back
        color
    }
    setTextColor(contrastColor)
}

/**
 * provides time in millisecond give the year,month of the year(1..12),day of the month
 * The hour and seconds will be filled by using the present hour and minute
 *
 * Example:
 *
 * if present hour is 14:30
 *
 *      timeFrom(2018, 4, 15)
 * will return 2018-04-15 14:30
 */
fun timeFrom(year: Int, month: Int, dayOfMonth: Int, timeProvider: TimeProvider? = null): Long {
    val now = timeProvider?.currentTimeMillis() ?: System.currentTimeMillis()
    return Calendar.getInstance().apply {
        timeInMillis = now
        set(year, month - 1, dayOfMonth)
    }.timeInMillis
}


fun Boolean?.toInt(): Int = this?.let { if (it) 1 else 0 } ?: 0

fun Group.startAnimationGroup(animation: Animation){
    val parent = getParent() as ConstraintLayout
    referencedIds.forEach {
        parent.findViewById<View>(it).startAnimation(animation)
    }
}