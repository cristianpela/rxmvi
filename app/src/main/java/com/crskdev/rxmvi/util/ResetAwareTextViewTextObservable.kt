package com.crskdev.rxmvi.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import com.crskdev.rxmvi.R
import com.crskdev.rxmvicore.util.cast
import com.jakewharton.rxbinding2.InitialValueObservable
import io.reactivex.Observer
import io.reactivex.android.MainThreadDisposable

/**
 * Created by Cristian Pela on 15.04.2018.
 */
class ResetAwareTextViewTextObservable(private val view: TextView) : InitialValueObservable<CharSequence>() {

    override fun getInitialValue(): CharSequence = view.text

    override fun subscribeListener(observer: Observer<in CharSequence>) {
        val listener = Listener(view, observer)
        observer.onSubscribe(listener)
        view.addTextChangedListener(listener)
    }


    internal class Listener(private val view: TextView, private val observer: Observer<in CharSequence>) : MainThreadDisposable(), TextWatcher {

        private var isInitText = false

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            isInitText = view.getTag(R.id.reset_aware_text_view_tag)?.cast<Boolean>() ?: false
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (!isDisposed && !isInitText) {
                observer.onNext(s)
            }
        }

        override fun afterTextChanged(s: Editable) {
            if(isInitText)
                view.setTag(R.id.reset_aware_text_view_tag, false)
        }

        override fun onDispose() {
            view.removeTextChangedListener(this)
        }
    }
}

fun TextView.resetAwareTextChanges(): ResetAwareTextViewTextObservable =
        ResetAwareTextViewTextObservable(this)


fun TextView.initialTextForAware(initText: CharSequence?) {
    setTag(R.id.reset_aware_text_view_tag, true)
    text = initText
}