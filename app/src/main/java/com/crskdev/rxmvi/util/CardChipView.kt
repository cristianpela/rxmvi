package com.crskdev.rxmvi.util

import android.content.Context
import android.os.Build
import android.support.v4.graphics.ColorUtils
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import com.crskdev.rxmvi.R


/**
 * .
 * Created by Cristian Pela on 21.03.2018.
 */
class CardChipView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : CardView(context, attrs, defStyleAttr) {

    private val textView = AppCompatTextView(context)

    var text: CharSequence
        set(value) {
            textView.text = value
        }
        get() = textView.text

    init {

        val resources = context.resources

        minimumHeight = 50.dpToPx(resources)
        minimumWidth = 50.dpToPx(resources)

        context.obtainStyledAttributesExt(attrs, R.styleable.CardChipView) {
            getString(R.styleable.CardChipView_android_text)?.let {
                textView.text = it
            }
            textView.setTextColor(context.getColorCompat(R.color.colorSecondary))
        }

        context.obtainStyledAttributesExt(attrs, R.styleable.CardView) {
            val backgroundColor = getColor(R.styleable.CardView_cardBackgroundColor, context.getColorCompat(android.R.color.white))
            if (ColorUtils.calculateLuminance(backgroundColor) < 0.5
                    && ColorUtils.calculateLuminance(textView.currentTextColor) < 0.5) {
                //background and text are both dark, so we make the text to be light
                textView.setTextColor(context.getColorCompat(android.R.color.white))
            }
        }

        //add text view to card
        with(textView) {
            layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    30.dpToPx(resources)).apply {
                gravity = Gravity.CENTER
            }

            minWidth = 40.dpToPx(resources)
            maxWidth = 40.dpToPx(resources)
            maxHeight = 50.dpToPx(resources)
            minHeight = 50.dpToPx(resources)
            textSize = 14.0f
            //  TextViewCompat.setAutoSizeTextTypeWithDefaults(this, AUTO_SIZE_TEXT_TYPE_UNIFORM)
            setSingleLine(true)
            ellipsize = TextUtils.TruncateAt.END
            gravity = Gravity.CENTER
            isClickable = true


        }
        addView(textView)
        useCompatPadding = true
        setContentPadding(8, 8, 8, 8)
        isClickable = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.obtainStyledAttributesExt(attrs = intArrayOf(R.attr.selectableItemBackground)) {
                val rippleId = getResourceId(0, 0)
                foreground = context.getDrawable(rippleId)
            }
        }

    }

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)
        textView.setOnClickListener(l)
    }
}