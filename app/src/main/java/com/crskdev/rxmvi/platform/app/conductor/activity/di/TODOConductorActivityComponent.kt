package com.crskdev.rxmvi.platform.app.conductor.activity.di

import com.crskdev.rxmvi.platform.app.conductor.activity.ConductorActivity
import com.crskdev.rxmvi.platform.app.conductor.controller.di.ControllerComponent
import dagger.Subcomponent

@Subcomponent(modules = [(TODOConductorActivityModule::class)])
@PerActivity
interface TODOConductorActivityComponent {

    fun inject(activity: ConductorActivity)

    fun controllerComponentBuilder(): ControllerComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun build(): TODOConductorActivityComponent
    }
}