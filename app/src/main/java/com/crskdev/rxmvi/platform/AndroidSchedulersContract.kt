package com.crskdev.rxmvi.platform

import com.crskdev.rxmvicore.rx.SchedulersContract
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
class AndroidSchedulersContract : SchedulersContract {

    override fun main(): Scheduler = AndroidSchedulers.mainThread()

    override fun io(): Scheduler = Schedulers.io()

    override fun computation(): Scheduler = Schedulers.computation()

}