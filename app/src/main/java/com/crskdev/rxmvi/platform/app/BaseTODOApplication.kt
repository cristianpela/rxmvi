package com.crskdev.rxmvi.platform.app

import android.app.Application

/**
 * .
 * Created by Cristian Pela on 19.03.2018.
 */
abstract class BaseTODOApplication: Application() {

    abstract fun appComponent(): TODOApplicationComponent

}