package com.crskdev.rxmvi.platform.app.conductor.activity.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity