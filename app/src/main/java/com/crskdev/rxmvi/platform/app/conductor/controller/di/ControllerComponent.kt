package com.crskdev.rxmvi.platform.app.conductor.controller.di

import com.crskdev.rxmvi.presentation.aggregate.AggregatorConductorController
import com.crskdev.rxmvi.presentation.aggregate.AggregatorModule
import com.crskdev.rxmvi.presentation.upsert.edit.EditTODOConductorController
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOModule
import com.crskdev.rxmvi.presentation.list.TODOListConductorController
import com.crskdev.rxmvi.presentation.list.TODOListModule
import com.crskdev.rxmvi.presentation.search.TODOSearchConductorController
import com.crskdev.rxmvi.presentation.search.TODOSearchModule
import com.crskdev.rxmvi.presentation.upsert.add.AddTODOConductorController
import com.crskdev.rxmvi.presentation.upsert.add.AddTODOMVIController
import dagger.Subcomponent

@PerController
@Subcomponent(modules = [
    (UpsertTODOModule::class),
    (TODOListModule::class),
    (AggregatorModule::class),
    (TODOSearchModule::class)
])
interface ControllerComponent {

    fun inject(controller: TODOListConductorController)

    fun inject(controller: EditTODOConductorController)

    fun inject(controller: AddTODOConductorController)

    fun inject(controller: AggregatorConductorController)

    fun inject(controller: TODOSearchConductorController)

    @Subcomponent.Builder
    interface Builder {
        fun build(): ControllerComponent
    }
}