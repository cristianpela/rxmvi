package com.crskdev.rxmvi.platform.app.conductor.activity.di

import com.crskdev.rxmvi.platform.app.conductor.controller.di.ControllerComponent
import dagger.Module

@Module(subcomponents = [(ControllerComponent::class)])
object TODOConductorActivityModule