package com.crskdev.rxmvi.platform.app.conductor.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.platform.app.appComponent
import com.crskdev.rxmvi.platform.app.conductor.ControllerInjector
import com.crskdev.rxmvi.platform.app.conductor.HasControllerInjector
import com.crskdev.rxmvi.platform.app.conductor.activity.di.TODOConductorActivityComponent
import com.crskdev.rxmvi.platform.app.conductor.controller.di.ControllerComponent
import com.crskdev.rxmvi.presentation.list.TODOListConductorController
import com.crskdev.rxmvi.presentation.notification.NotificationDueActions
import com.crskdev.rxmvi.presentation.search.TODOSearchConductorController
import com.crskdev.rxmvi.util.bundleOf
import kotlinx.android.synthetic.main.layout_main_activity.*


/**
 * .
 * Created by Cristian Pela on 05.03.2018.
 */
class ConductorActivity : AppCompatActivity(), HasControllerInjector {

    private lateinit var router: Router

    lateinit var activityComponent: TODOConductorActivityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_main_activity)
        activityComponent = application
                .appComponent()
                .activityComponentBuilder()
                .build()
                .apply {
                    inject(this@ConductorActivity)
                }


        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!router.hasRootController()) {

            //check if args can be created from notification action
            val args = if (intent.action == NotificationDueActions.DUE_ACTION_SHOW_ALL)
                bundleOf {
                    putInt(TODOListConductorController.KEY_FILTER, TODOFilter.ABOUT_DUE.ordinal)
                }
            else Bundle()

            //hide notification
            val notificationId = intent.getIntExtra(NotificationDueActions.DUE_EXTRA_NOTIFICATION_ID, -1)
            if (notificationId != -1) {
                NotificationManagerCompat.from(applicationContext).cancel(notificationId)
            }

            router.setRoot(RouterTransaction.with(TODOListConductorController(args)))
        }

        //check permission for accounts
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.GET_ACCOUNTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_CONTACTS),
                        101)
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            101 -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return
            }

        // Add other 'when' lines to check for other
        // permissions this app might request.

            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }

    override fun getControllerInjector(): ControllerInjector = object : ControllerInjector {
        override fun component(): ControllerComponent =
                activityComponent.controllerComponentBuilder().build()
    }
}

