package com.crskdev.rxmvi.platform

/**
 * Adapter for framework current time in millis
 * Created by Cristian Pela on 08.03.2018.
 */
interface TimeProvider {
    fun currentTimeMillis(): Long
}

class SystemTimeProvider : TimeProvider {
    override fun currentTimeMillis(): Long = System.currentTimeMillis()
}