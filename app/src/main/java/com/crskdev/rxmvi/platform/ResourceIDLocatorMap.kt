package com.crskdev.rxmvi.platform

import com.crskdev.rxmvi.R
import com.crskdev.rxmvi.domain.*

/**.
 * Created by Cristian Pela on 19.03.2018.
 */
object ResourceIDLocatorMap {

    private val resourceMap = mapOf(
            //ERRORS
            RES_ERR_EMPTY_AUTHOR to R.string.res_err_empty_author,
            RES_ERR_EMPTY_TITLE to R.string.res_err_empty_title,
            RES_ERR_EMPTY_CREATION_DATE to R.string.res_err_empty_creation_date,
            RES_ERR_DUE_DATE_BEFORE_NOW to R.string.res_err_due_date_before_now,
            RES_ERR_DUE_DATE_INVALID to R.string.res_err_due_date_invalid,
            RES_ERR_FETCH_EMPTY_SINGLE_TODO to R.string.res_err_fetch_empty_single_todo,
            RES_ERR_USERNAME_NOT_FOUND to R.string.res_err_username_not_found,
            //MESSAGES
            RES_OK to R.string.res_ok,
            RES_MSG_TODO_ADD to R.string.res_msg_todo_add,
            RES_MSG_TODO_UPDATE to R.string.res_msg_todo_update,
            RES_MSG_FETCH_EMPTY_TODOS to R.string.res_msg_fetch_empty_todos,
            RES_MSG_USERNAME_SAVED to R.string.res_msg_username_saved,
            RES_MSG_TODO_DELETED to R.string.res_msg_todo_deleted,
            //COLORS
            RES_COLOR_TODO_COMPLETED to R.color.res_color_todo_completed,
            RES_COLOR_TODO_NO_DUE to R.color.res_color_todo_no_due,
            RES_COLOR_TODO_ALMOST_DUE to R.color.res_color_todo_almost_due,
            RES_COLOR_TODO_PASSED_DUE to R.color.res_color_todo_passed_due
    )

    val locator = ResourceIDLocator(resourceMap)

}