package com.crskdev.rxmvi.platform.app.conductor.controller

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bluelinelabs.conductor.Controller
import com.crskdev.rxmvi.platform.app.conductor.ControllerInjector
import com.crskdev.rxmvi.platform.app.conductor.HasControllerInjector
import com.crskdev.rxmvi.util.toMap
import com.crskdev.rxmvi.util.toPx
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.Message
import com.crskdev.rxmvicore.env.ResourceIDError
import com.crskdev.rxmvicore.env.ResourceIDMessage
import com.crskdev.rxmvicore.presentation.*
import com.crskdev.rxmvicore.util.cast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlin.math.roundToInt


/**
 * .
 * Created by Cristian Pela on 16.03.2018.
 */
abstract class MVIConductorController<in S : State, out Ops : MVIStreamOps<*>>(args: Bundle? = null)
    : Controller(args),
        IntentRenderer<S>,
        HasMVIStreamOps<Ops> {

    @LayoutRes
    abstract fun layout(): Int

    abstract fun injectMe()

    private val mviStreamOpsHandler = HasMVIStreamOpsHandler<S>(AndroidSchedulers.mainThread())

    private var isFirstTime = true

    init {
        addLifecycleListener(object : LifecycleListener() {
            override fun preCreateView(controller: Controller) {
                injectMe()
            }

            override fun postCreateView(controller: Controller, view: View) {
                mviStreamOpsHandler.allowRendererToPrepareViews(this@MVIConductorController)
                mviStreamOpsHandler.subscribeStreams(
                        this@MVIConductorController,
                        this@MVIConductorController,
                        args?.toMap() ?: emptyMap(), isFirstTime
                )
            }

            override fun postDestroyView(controller: Controller) {
                mviStreamOpsHandler.disposeStreams(this@MVIConductorController)
            }
        })
    }


    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        isFirstTime = false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(layout(), container, false)

    override fun onAllowViewsToPrepare() = Unit

    override fun provideIntentStreams(): List<Observable<Intent>> = emptyList()

    override fun onArgumentsPassed(args: Map<String, Any>, isFirstTime: Boolean) = Unit

    override fun renderErrors(err: Throwable) {
        view?.let {
            val messageData = if (err is ResourceIDError) {
                composeMessageData(err.idMessage)
            } else {
                err.message ?: err.toString()
            }
            Snackbar.make(it, messageData, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun renderMessages(message: Message<*>) {
        view?.let {
            val messageData = if (message is ResourceIDMessage) {
                composeMessageData(message)
            } else {
                message.data.toString()
            }
            Snackbar.make(it, messageData, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun composeMessageData(msg: ResourceIDMessage): String {
        return buildString {
            val lastIndex = msg.data.size - 1
            msg.data.forEachIndexed { index, res ->
                append(getStringRes(res.id, *res.args))
                if (index < lastIndex)
                    append("\n")
            }
        }
    }

    protected fun injector(): ControllerInjector? =
            activity?.cast<HasControllerInjector>()?.getControllerInjector()

    fun postSendIntent(intent: Intent) {
        mviStreamOpsHandler.sendIntentMaybePending(intent)
    }

    inline fun <reified V : View> Int.v() = view?.findViewById(this) as V?

    fun Int.justV(): View? = view?.findViewById(this)

    fun Int.toPx(unit: Int): Float = this@MVIConductorController.resources?.let { this.toPx(it, unit) }
            ?: this.toFloat()

    fun Int.dpToPx(): Int = toPx(TypedValue.COMPLEX_UNIT_DIP).roundToInt()

    fun Int.spToPx(): Float = toPx(TypedValue.COMPLEX_UNIT_SP)

    fun getStringRes(@StringRes id: Int, vararg args: Any): CharSequence? = resources?.getString(id, *args)

}

