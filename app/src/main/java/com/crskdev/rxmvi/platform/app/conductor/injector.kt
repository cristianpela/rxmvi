package com.crskdev.rxmvi.platform.app.conductor

import com.crskdev.rxmvi.platform.app.conductor.controller.di.ControllerComponent

/**.
 * Created by Cristian Pela on 18.03.2018.
 */

interface ControllerInjector {

    fun component(): ControllerComponent
}

interface HasControllerInjector {

    fun getControllerInjector(): ControllerInjector

}