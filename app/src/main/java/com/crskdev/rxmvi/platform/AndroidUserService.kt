package com.crskdev.rxmvi.platform

import android.accounts.AccountManager
import android.content.Context
import android.content.Context.ACCOUNT_SERVICE
import android.content.SharedPreferences
import com.crskdev.rxmvi.domain.gateway.UserService
import com.crskdev.rxmvi.util.isEmptyOrBlank
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import android.accounts.Account



/**
 * .
 * Created by Cristian Pela on 28.03.2018.
 */
class AndroidUserService
@Inject constructor(
        private val ctx: Context,
        prefs: SharedPreferences
) : UserService {

    private val prefUserService = PreferenceUserService(prefs)

    override fun getUser(): Single<String> =
            Single.create<String> {
                val accManager = ctx.getSystemService(ACCOUNT_SERVICE) as AccountManager
                val username: String?
                val accounts = accManager.getAccountsByType("com.google")
                if (accounts != null && accounts.isNotEmpty()) {
                    username = accounts[0].name.split("@")[0]
                    it.onSuccess(username)
                } else {
                    it.onError(UserService.ERROR)
                }
            }.onErrorResumeNext {
                prefUserService.getUser()
            }

    override fun saveUser(username: String): Completable = prefUserService.saveUser(username)

    private class PreferenceUserService(private val prefs: SharedPreferences) : UserService {

        companion object {
            private const val KEY_USERNAME = "KEY_USERNAME"
        }

        override fun getUser(): Single<String> =
                Single.create<String> {
                    val username = prefs.getString(KEY_USERNAME, "")
                    if (username.isEmptyOrBlank()) {
                        it.onError(UserService.ERROR)
                    } else {
                        it.onSuccess(username)
                    }
                }

        override fun saveUser(username: String): Completable =
                Completable.create { emitter ->
                    val commited = prefs.edit().apply {
                        putString(KEY_USERNAME, username)
                    }.commit()
                    if (commited) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(Error("Could not commit username to preferences"))
                    }
                }

    }
}