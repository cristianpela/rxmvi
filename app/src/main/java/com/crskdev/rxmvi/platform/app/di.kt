package com.crskdev.rxmvi.platform.app

import android.content.Context
import android.content.SharedPreferences
import com.crskdev.rxmvi.data.InMemoryTODORepository
import com.crskdev.rxmvi.data.PreferencesTODOFilterPersistor
import com.crskdev.rxmvi.data.room.RoomTODORepository
import com.crskdev.rxmvi.data.room.TODODao
import com.crskdev.rxmvi.data.room.TODODatabase
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.domain.gateway.UserService
import com.crskdev.rxmvi.platform.*
import com.crskdev.rxmvi.platform.app.conductor.activity.di.PerActivity
import com.crskdev.rxmvi.platform.app.conductor.activity.di.TODOConductorActivityComponent
import com.crskdev.rxmvi.presentation.notification.NotificationDueComponent
import com.crskdev.rxmvi.presentation.notification.NotificationDueJobManager
import com.crskdev.rxmvi.presentation.notification.NotificationDueProcessorComponent
import com.crskdev.rxmvicore.rx.SchedulersContract
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton


/**
 * .
 * Created by Cristian Pela on 19.03.2018.
 */
@Singleton
@Component(modules = [(TODOApplicationModule::class)])
interface TODOApplicationComponent {

    fun activityComponentBuilder(): TODOConductorActivityComponent.Builder

    fun notificationDueComponentBuilder(): NotificationDueComponent.Builder

    fun notificationDueActionProcessor(): NotificationDueProcessorComponent.Builder

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun bindApplication(app: TODOApplication): Builder

        @BindsInstance
        fun bindNotificationDueJobManager(manager: NotificationDueJobManager): Builder

        fun build(): TODOApplicationComponent
    }

}

@Module(subcomponents = [TODOConductorActivityComponent::class],
        includes = [(ResourceModule::class), (DatabaseModule::class), (RxModule::class)])
object TODOApplicationModule {

    @JvmStatic
    @Provides
    fun provideContext(app: TODOApplication): Context = app.applicationContext

    @JvmStatic
    @Provides
    fun provideTimeProvider(): TimeProvider = SystemTimeProvider()

    @JvmStatic
    @Provides
    @Singleton
    fun providePreferences(context: Context): SharedPreferences =
            context.getSharedPreferences("APP_SHARED_PREFS", Context.MODE_PRIVATE)

    @JvmStatic
    @Provides
    fun provideFilterPersistor(prefs: SharedPreferences): TODOFilterPersistor =
            PreferencesTODOFilterPersistor(prefs)


    @JvmStatic
    @Provides
    @Singleton
    fun provideUserService(context: Context, prefs: SharedPreferences): UserService =
            AndroidUserService(context, prefs)

}

@Module
object RxModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideSchedulersContract(): SchedulersContract = AndroidSchedulersContract()

}

@Module
object ResourceModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideResourceLocator(): ResourceIDLocator = ResourceIDLocatorMap.locator

}

@Module
object DatabaseModule {

    @JvmStatic
    @Provides
    fun initialData(timeProvider: TimeProvider): List<TODOEntity> =
            listOf(
                    TODOEntity(0, timeProvider.currentTimeMillis(),
                            timeProvider.currentTimeMillis() + TimeUnit.DAYS.toMillis(2),
                            "Cristian",
                            "Trebuie sa termin proiectul TODO pana nu vorbesc cu cei de la google",
                            completed = true),
                    TODOEntity(1, timeProvider.currentTimeMillis(), timeProvider.currentTimeMillis() + TimeUnit.DAYS.toMillis(60),
                            "Cristian",
                            "Trebuie sa slabesc 10 kilograme"),
                    TODOEntity(2, timeProvider.currentTimeMillis() - TimeUnit.DAYS.toMillis(2),
                            timeProvider.currentTimeMillis() - TimeUnit.DAYS.toMillis(1),
                            "Cristian",
                            "Cumpar un televizor la mana 2"),
                    TODOEntity(3, timeProvider.currentTimeMillis(), null, "Cristian",
                            "Invata in continuare android si ultime tendinte in materie de software architecture")
            )


    @JvmStatic
    @Provides
    @RepoProxy
    fun provideRepository(@RepoRoom repository: TODORepository): TODORepository = repository

    @JvmStatic
    @Provides
    @Singleton // NOTE: keep the data in memory after activity destroyed, config changed etc
    @RepoMemo
    fun provideRepositoryMemo(timeProvider: TimeProvider, initialData: List<TODOEntity>): TODORepository =
            InMemoryTODORepository(timeProvider, initialData)

    @JvmStatic
    @Provides
    @RepoRoom
    fun provideRepositoryRoom(
            timeProvider: TimeProvider,
            dao: TODODao,
            schedulersContract: SchedulersContract): TODORepository =
            RoomTODORepository(timeProvider, dao, schedulersContract.io())


    @JvmStatic
    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): TODODatabase = TODODatabase.create(context)

    @JvmStatic
    @Provides
    @Singleton
    fun provideTODODao(db: TODODatabase): TODODao = db.todoDao()

}

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class RepoProxy

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class RepoRoom

@Qualifier
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class RepoMemo

