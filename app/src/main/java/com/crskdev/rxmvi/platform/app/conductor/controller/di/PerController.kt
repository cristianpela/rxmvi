package com.crskdev.rxmvi.platform.app.conductor.controller.di

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PerController