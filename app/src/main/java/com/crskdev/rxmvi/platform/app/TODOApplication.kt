package com.crskdev.rxmvi.platform.app

import android.app.Application
import android.content.Context
import com.crskdev.rxmvi.presentation.notification.NotificationDueJobManager
import com.crskdev.rxmvicore.util.cast


/**
 * .
 * Created by Cristian Pela on 16.03.2018.
 */
class TODOApplication : BaseTODOApplication() {

    private lateinit var appComponent: TODOApplicationComponent

    override fun onCreate() {
        super.onCreate()
        val jobManager = NotificationDueJobManager(this)
        appComponent = DaggerTODOApplicationComponent.builder()
                .bindApplication(this)
                .bindNotificationDueJobManager(jobManager)
                .build()
        jobManager.run {
            cancelAll()
            schedule()
        }
    }

    override fun appComponent(): TODOApplicationComponent = appComponent
}

fun Application.appComponent() = this.cast<BaseTODOApplication>().appComponent()

fun Context.appComponent(): TODOApplicationComponent =
        this.applicationContext.cast<BaseTODOApplication>().appComponent()

