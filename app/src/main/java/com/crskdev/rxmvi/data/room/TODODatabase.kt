package com.crskdev.rxmvi.data.room

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import android.content.Context

/**
 * Created by Cristian Pela on 12.04.2018.
 */
@Database(
        entities = [(TODORoomEntity::class), (TODODeleted::class)],
        version = 3,
        exportSchema = true
)
abstract class TODODatabase : RoomDatabase() {

    companion object {
        fun create(context: Context, inMemory: Boolean = false, name: String = "todo-db"): TODODatabase {
            val builder = if (inMemory)
                Room.inMemoryDatabaseBuilder(context, TODODatabase::class.java)
            else
                Room.databaseBuilder(context, TODODatabase::class.java, name)
            return builder
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                    .addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            db.execSQL("""
                                 /*create trigger which insert the todo id when deleted in deleted_todos*/
                                CREATE TRIGGER delete_todo_trigger AFTER DELETE ON todo
                                BEGIN
                                    INSERT INTO deleted_todos VALUES(null, OLD.id);
                                END
                             """.trimIndent())
                        }
                    }).build()
        }

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS  deleted_todos (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, todo_id INTEGER NOT NULL)")
            }

        }

        private val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execInTransaction {
                    //create backup
                    execSQL("CREATE TEMP TABLE deleted_todos_temp (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, todo_id INTEGER NOT NULL)")
                    //fill backup
                    execSQL("INSERT INTO deleted_todos_temp SELECT id, todo_id FROM deleted_todos")
                    //drop original
                    execSQL("DROP TABLE deleted_todos")
                    //recreate original without NON NULL for ID
                    execSQL("CREATE TABLE IF NOT EXISTS  deleted_todos (id INTEGER PRIMARY KEY AUTOINCREMENT, todo_id INTEGER NOT NULL)")
                    //repopulate the recreated
                    execSQL("INSERT INTO deleted_todos SELECT id, todo_id FROM deleted_todos_temp")
                    //drop backup
                    execSQL("DROP TABLE deleted_todos_temp")
                }
            }

        }

    }

    abstract fun todoDao(): TODODao
}

fun SupportSQLiteDatabase.execInTransaction(transactionExec: SupportSQLiteDatabase.()->Unit){
    try {
        beginTransaction()
        this.transactionExec()
        setTransactionSuccessful()
    }finally {
        endTransaction()
    }
}