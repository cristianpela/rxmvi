package com.crskdev.rxmvi.data

import com.crskdev.rxmvi.data.room.like
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.gateway.ClearableTODORepository
import com.crskdev.rxmvi.platform.TimeProvider
import com.crskdev.rxmvi.util.SimpleHighlightedCharSequence
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * .
 * Created by Cristian Pela on 16.03.2018.
 */
class InMemoryTODORepository
@Inject constructor(
        private val timeProvider: TimeProvider,
        data: List<TODOEntity> = emptyList()
) : ClearableTODORepository {

    override fun getAggregation(): Single<EnumMap<TODOFilter, Int>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val db = data.toMutableList()

    private val watcher: Subject<TODOEntity> = PublishSubject.create()

    private val deletedWatcher: Subject<List<Long>> = BehaviorSubject.createDefault(emptyList())

    private val pageAllTracker = PageTracker {
        val now = timeProvider.currentTimeMillis()
        val filteredDb = applyFilter(it.filter as TODOFilter, now)
        val (start, end) = it.totalItems().let { total -> total to Math.min(filteredDb.size, total + it.size) }
        if (end < start) emptyList() else filteredDb
                .subList(start, end).map { it.copy(detail = null) }
    }

    private val pageSearchTracker = PageTracker {
        it.extra?.let { extra ->
            db.filter { it.title.contains(extra.toString()) }
        } ?: emptyList()

    }

    override fun search(textLike: String, fromStart: Boolean): Single<List<TODOEntity>> =
            Single.create<List<TODOEntity>> {
                val result = pageSearchTracker
                        .nextPageResult(TODOFilter.UNKNOWN, fromStart, 10, textLike.like())
                        .map {
                            it.copy(title = SimpleHighlightedCharSequence(it.title, textLike))
                        }
                it.onSuccess(result)
            }

    override fun observeChangesInSearch(textLike: String): Observable<TODOEntity> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAll(filter: TODOFilter, fromStart: Boolean, size: Int, includeDetail: Boolean): Single<List<TODOEntity>> =
            Single.create { e ->
                val result = pageAllTracker.nextPageResult(filter, fromStart, size)
                e.onSuccess(result)
            }


    private fun applyFilter(filter: TODOFilter, now: Long): List<TODOEntity> =
            when (filter) {
                TODOFilter.NOT_COMPLETED -> db.filter { !it.completed }
                TODOFilter.COMPLETED -> db.filter { it.completed }
                TODOFilter.NO_DUE -> db.filter { !it.completed && it.dueDate == null }
                TODOFilter.ABOUT_DUE -> db.filter {
                    !it.completed && it.dueDate != null
                            && it.dueDate >= now
                            && it.dueDate - now <= TimeUnit.HOURS.toMillis(24)
                }
                TODOFilter.PASSED_DUE -> db.filter {
                    !it.completed && it.dueDate != null
                            && it.dueDate < now
                }
                else -> db
            }.sortedByDescending {
                it.creationDate
            }

    override fun complete(id: Long, complete: Boolean): Completable =
            Single.create<TODOEntity> { e ->
                db.indexOfFirst { id == it.id }
                        .takeIf { it >= 0 }
                        ?.let {
                            db[it] = db[it].copy(completed = complete)
                            e.onSuccess(db[it])
                        } ?: e.onError(Error("Id Not Found"))
            }.doOnSuccess { watcher.onNext(it) }.toCompletable()

    override fun observe(): Observable<TODOEntity> = watcher

    override fun observeAggregator(): Observable<EnumMap<TODOFilter, Int>> =
            watcher.map {
                computeAggregator()
            }.startWith(computeAggregator())

    override fun observeDeleted(): Observable<List<Long>> = deletedWatcher

    private fun computeAggregator(): EnumMap<TODOFilter, Int> {
        val now = timeProvider.currentTimeMillis()
        val aggmap = EnumMap<TODOFilter, Int>(TODOFilter::class.java)
        TODOFilter.values().forEach {
            aggmap[it] = applyFilter(it, now).size
        }
        return aggmap
    }

    override fun getOne(id: Long): Single<TODOEntity> {
        return db.indexOfFirst { id == it.id }.let {
            if (it == -1) Single.error(Error("Id Not Found")) else Single.just(db[it])
        }
    }

    override fun upsert(todo: TODOEntity): Single<Long> {
        return Single.create<Long> { e ->
            val existingIndex = db.indexOfFirst { todo.id == it.id }
            val id: Long
            if (existingIndex >= 0) {
                db[existingIndex] = todo
                id = todo.id
            } else {
                val nextId = db.last().id + 1
                db.add(todo.copy(id = nextId, creationDate = timeProvider.currentTimeMillis()))
                id = nextId
            }
            e.onSuccess(id)
        }.doOnSuccess { watcher.onNext(todo.copy(id = it)) }
    }

    override fun delete(id: Long): Single<Long> {
        return Single.create<Long> { e ->
            val existingIndex = db.indexOfFirst { id == it.id }
            if (existingIndex >= 0) {
                db.removeAt(existingIndex)
                e.onSuccess(id)
            } else {
                e.onError(Error("Id Not Found for Delete"))
            }
        }.doOnSuccess {
            deletedWatcher.scan { acc, curr ->
                acc + curr
            }
        }
    }

    override fun clear() {
        db.clear()
    }

    override fun refresh(data: List<TODOEntity>) {
        db.addAll(data)
    }


}
