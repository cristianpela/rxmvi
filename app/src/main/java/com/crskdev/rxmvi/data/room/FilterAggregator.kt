package com.crskdev.rxmvi.data.room

import android.arch.persistence.room.ColumnInfo
import com.crskdev.rxmvi.domain.TODOFilter
import java.util.*

/**
 * Created by Cristian Pela on 13.04.2018.
 */
data class FilterAggregator(
        var all: Int,
        var completed: Int,
        @ColumnInfo(name = "not_completed")
        var notCompleted: Int,
        @ColumnInfo(name = "about_due")
        var aboutDue: Int,
        @ColumnInfo(name = "passed_due")
        var passedDue: Int,
        @ColumnInfo(name = "no_due")
        var no_due: Int) {

    fun toFilterEnumMap(): EnumMap<TODOFilter, Int> =
            EnumMap<TODOFilter, Int>(TODOFilter::class.java).apply {
                TODOFilter.values().forEach {
                    when (it) {
                        TODOFilter.ALL -> put(it, all)
                        TODOFilter.NOT_COMPLETED -> put(it, notCompleted)
                        TODOFilter.COMPLETED -> put(it, completed)
                        TODOFilter.ABOUT_DUE -> put(it, aboutDue)
                        TODOFilter.PASSED_DUE -> put(it, passedDue)
                        TODOFilter.NO_DUE -> put(it, no_due)
                        else -> put(it, 0)
                    }
                }
            }
}