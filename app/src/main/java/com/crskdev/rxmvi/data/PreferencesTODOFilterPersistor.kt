package com.crskdev.rxmvi.data

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * .
 * Created by Cristian Pela on 17.03.2018.
 */
class PreferencesTODOFilterPersistor(private val prefs: SharedPreferences) : TODOFilterPersistor {

    companion object {
        private const val KEY_TODO_FILTER = "KEY_TODO_FILTER"
    }

    override fun load(defaultForEmpty: TODOFilter): Single<TODOFilter> =
            Single.just(getFilterFromKey(KEY_TODO_FILTER, defaultForEmpty))

    private fun getFilterFromKey(key: String, defaultForEmpty: TODOFilter) =
            TODOFilter.values()[prefs
                    .getInt(key, defaultForEmpty.ordinal)]

    override fun observerFilterChanges(defaultForEmpty: TODOFilter): Observable<TODOFilter> =
            Observable.create<TODOFilter> {
                //get
                val listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
                    it.onNext(getFilterFromKey(key, defaultForEmpty))
                }
                prefs.registerOnSharedPreferenceChangeListener(listener)
                it.setCancellable {
                    prefs.unregisterOnSharedPreferenceChangeListener(listener)
                }
            }

    @SuppressLint("ApplySharedPref")
    override fun save(filter: TODOFilter): Completable =
            Completable.create {
                //going sync here with commit, in order to signal the completion
                //correctly
                prefs.edit()
                        .putInt(KEY_TODO_FILTER, filter.ordinal)
                        .commit()
                it.onComplete()
            }
}