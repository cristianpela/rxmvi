package com.crskdev.rxmvi.data.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.crskdev.rxmvi.domain.entity.TODOEntity

/**
 * Created by Cristian Pela on 11.04.2018.
 */
@Entity(tableName = "todo")
data class TODORoomEntity(
        @PrimaryKey(autoGenerate = true)
        var id: Long?,
        @ColumnInfo(name = "creation_date")
        val creationDate: Long,
        @ColumnInfo(name = "due_date")
        val dueDate: Long? = null,
        val author: String,
        val title: String,
        val detail: String? = null,
        val completed: Boolean,
        val modified: Long
)

fun TODORoomEntity.toDomain(): TODOEntity = TODOEntity(
        id ?: -1L, creationDate, dueDate, author, title, detail, completed)

fun TODOEntity.toDb(modified: Long): TODORoomEntity = TODORoomEntity(
        if (id == -1L) null else id,
        if (creationDate <= 0L) modified else creationDate,
        dueDate,
        author,
        title.toString(),
        detail?.toString(),
        completed,
        modified)
