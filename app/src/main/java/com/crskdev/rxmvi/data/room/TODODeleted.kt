package com.crskdev.rxmvi.data.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by Cristian Pela on 16.04.2018.
 */
@Entity(tableName = "deleted_todos")
data class TODODeleted(@PrimaryKey(autoGenerate = true)var id: Long? = null, @ColumnInfo(name = "todo_id")var todoId: Long)
