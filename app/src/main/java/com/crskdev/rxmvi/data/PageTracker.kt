package com.crskdev.rxmvi.data

import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvicore.util.Page
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Created by Cristian Pela on 12.04.2018.
 */
class PageTracker<out T>(pageSize: Int = 10, private val dataCall: (page: Page) -> List<T>) {

    private val lock = ReentrantLock()

    @Volatile
    private var currentPage = Page(TODOFilter.UNKNOWN, pageSize)

    fun nextPageResult(filter: TODOFilter, fromStart: Boolean, size: Int = 10, extra: Any? = null): List<T> {
        if (filter == TODOFilter.UNKNOWN) {
            return emptyList()
        }
        lock.withLock {
            if (currentPage.filter != filter || fromStart)
                currentPage = currentPage.newFilter(filter, size, extra)
        }
        val result = dataCall(currentPage)
        lock.withLock {
            if (result.isNotEmpty())
                currentPage = currentPage.next()
        }
        return result
    }

}