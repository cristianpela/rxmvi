package com.crskdev.rxmvi.data.room

import com.crskdev.rxmvi.data.PageTracker
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.platform.TimeProvider
import com.crskdev.rxmvi.util.SimpleHighlightedCharSequence
import com.crskdev.rxmvi.util.isEmptyOrBlank
import com.crskdev.rxmvicore.util.cast
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

/**
 * Created by Cristian Pela on 10.04.2018.
 */
class RoomTODORepository
@Inject constructor(
        private val timeProvider: TimeProvider,
        private val dao: TODODao,
        private val scheduler: Scheduler) : TODORepository {
    private val pageFilterTracker = DaoPageTrackerFacade(10, timeProvider, dao)

    private val pageSearchTracker = PageTracker {
        it.extra?.let { extra ->
            val textLike = extra.cast<String>()
            if (textLike.isEmpty()) {
                emptyList()
            } else {
                dao.search(it.totalItems(), it.size, textLike.toUpperCase().like())
            }
        } ?: emptyList()

    }

    override fun getAll(filter: TODOFilter, fromStart: Boolean, size: Int, includeDetail: Boolean): Single<List<TODOEntity>> =
            Single.create<List<TODOEntity>> {
                val result = pageFilterTracker.nextPageResult(filter, fromStart, size, includeDetail)
                it.onSuccess(result.map { it.toDomain() })
            }.subscribeOn(scheduler)

    override fun getOne(id: Long): Single<TODOEntity> =
            Single.create<TODOEntity> { e ->
                dao.findOne(id)?.toDomain()?.let {
                    e.onSuccess(it)
                } ?: e.onError(Error("TODO not found"))
            }.subscribeOn(scheduler)

    override fun upsert(todo: TODOEntity): Single<Long> =
            Single.create<Long> {
                val now = timeProvider.currentTimeMillis()
                val creationDate = if (todo.creationDate <= 0) now else todo.creationDate
                val dbEntity = todo.toDb(now).copy(
                        creationDate = creationDate,
                        modified = now)
                val insertedId = dao
                        .upsert(dbEntity)
                        .let { if (it.isEmpty()) -1L else it.first() }
                if (insertedId != -1L) {
                    it.onSuccess(insertedId)
                } else {
                    it.onError(Error("Could not upsert the todo entity into database"))
                }
            }.subscribeOn(scheduler)

    override fun delete(id: Long): Single<Long> =
            Single.create<Long> {
                //TODO do these in transaction
                val deleted: Boolean = dao.delete(id) > 0
                if (deleted) {
                    dao.insertDeleted(TODODeleted(todoId = id))
                    it.onSuccess(id)
                } else {
                    it.onError(Error("Id Not Found for Delete"))
                }
            }.subscribeOn(scheduler)

    override fun search(textLike: String, fromStart: Boolean): Single<List<TODOEntity>> =
            Single.create<List<TODOEntity>> {
                val text = if (textLike.isEmptyOrBlank()) textLike else textLike.trim()
                val result = pageSearchTracker
                        .nextPageResult(TODOFilter.ALL, fromStart, 10, text)
                        .map {
                            it.toDomain().copy(title = SimpleHighlightedCharSequence(it.title, text))
                        }
                it.onSuccess(result)
            }.subscribeOn(scheduler)

    override fun observeChangesInSearch(textLike: String): Observable<TODOEntity> =
            dao.observe().toObservable()
                    .map { it.toDomain().copy(title = SimpleHighlightedCharSequence(it.title, textLike)) }


    override fun complete(id: Long, complete: Boolean): Completable =
            Completable.create {
                val isUpdated = dao.complete(id, complete, timeProvider.currentTimeMillis()) > 0
                if (isUpdated)
                    it.onComplete()
                else
                    it.onError(Error("Could not complete the todo with id $id. Invalid id?"))
            }.subscribeOn(scheduler)


    override fun observe(): Observable<TODOEntity> =
            dao.observe().distinctUntilChanged().map { it.toDomain() }.toObservable()

    override fun observeAggregator(): Observable<EnumMap<TODOFilter, Int>> =
            dao.observeAggregator()
                    .map { it.toFilterEnumMap() }
                    .toObservable()

    override fun getAggregation(): Single<EnumMap<TODOFilter, Int>> =
            Single.create<EnumMap<TODOFilter, Int>> {
                val map = dao.getAggregation().toFilterEnumMap()
                it.onSuccess(map)
            }.subscribeOn(scheduler)


    override fun observeDeleted(): Observable<List<Long>> =
            dao.observeDeleted().filter { it.isNotEmpty() }.toObservable()

}

class DaoPageTrackerFacade(size: Int, timeProvider: TimeProvider, dao: TODODao) {

    private val pageTracker = PageTracker(size) {
        val includeDetail = it.extra?.cast<Boolean>() ?: false
        when (it.filter) {
            TODOFilter.ALL -> dao.getAll(it.totalItems(), it.size).treatDetails(includeDetail)
            TODOFilter.NOT_COMPLETED -> dao.getAllCompleted(it.totalItems(), false, it.size)
                    .treatDetails(includeDetail)
            TODOFilter.COMPLETED -> dao.getAllCompleted(it.totalItems(), true, it.size)
                    .treatDetails(includeDetail)
            TODOFilter.NO_DUE -> dao.getAllNoDue(it.totalItems(), it.size)
                    .treatDetails(includeDetail)
            TODOFilter.ABOUT_DUE -> dao
                    .getAllAboutDue(it.current,
                            timeProvider.currentTimeMillis(),
                            TODORepository.WITHIN_DUE_TIME,
                            it.size)
                    .treatDetails(includeDetail)
            TODOFilter.PASSED_DUE -> dao.getAllPassedDue(it.totalItems(),
                    timeProvider.currentTimeMillis(),
                    it.size)
                    .treatDetails(includeDetail)
            else -> emptyList()
        }
    }

    private fun List<TODORoomEntity>.treatDetails(include: Boolean): List<TODORoomEntity> =
            if (include) this else this.map { it.copy(detail = null) }

    fun nextPageResult(todoFilter: TODOFilter, fromStart: Boolean, size: Int, extra: Any? = null): List<TODORoomEntity> =
            pageTracker.nextPageResult(todoFilter, fromStart, size, extra)

}
