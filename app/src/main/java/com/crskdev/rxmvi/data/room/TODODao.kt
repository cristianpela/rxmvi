package com.crskdev.rxmvi.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable

/**
 * Created by Cristian Pela on 10.04.2018.
 */
@Dao
interface TODODao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(vararg todos: TODORoomEntity): List<Long>

    @Query("SELECT * FROM todo WHERE id=:id")
    fun findOne(id: Long): TODORoomEntity?

    @Query("DELETE FROM todo WHERE id=:id")
    fun delete(id: Long): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDeleted(deletedTODO: TODODeleted)

    @Query("UPDATE todo SET completed = :complete, modified = :modified WHERE id = :id")
    fun complete(id: Long, complete: Boolean, modified: Long): Long

    @Query("""
        SELECT * FROM todo
        WHERE completed = :complete
            ORDER BY creation_date
            DESC LIMIT :limit OFFSET :pageOffset""")
    fun getAllCompleted(pageOffset: Int, complete: Boolean, limit: Int): List<TODORoomEntity>

    @Query("""
        SELECT * FROM todo
            ORDER BY creation_date
            DESC LIMIT :limit OFFSET :pageOffset""")
    fun getAll(pageOffset: Int, limit: Int): List<TODORoomEntity>

    @Query("""
        SELECT * FROM todo
            WHERE completed = 0
            AND due_date IS NULL
            ORDER BY creation_date
            DESC LIMIT :limit OFFSET :pageOffset""")
    fun getAllNoDue(pageOffset: Int, limit: Int): List<TODORoomEntity>

    @Query("""
        SELECT * FROM todo
        WHERE completed = 0 AND due_date < :now
        ORDER BY creation_date
        DESC LIMIT :limit OFFSET :pageOffset""")
    fun getAllPassedDue(pageOffset: Int, now: Long, limit: Int): List<TODORoomEntity>

    @Query("""
        SELECT * FROM todo
        WHERE completed = 0
        AND due_date BETWEEN :now AND :now + :within
        ORDER BY creation_date
        DESC LIMIT :limit OFFSET :pageOffset""")
    fun getAllAboutDue(pageOffset: Int, now: Long, within: Long, limit: Int): List<TODORoomEntity>

    @Query("SELECT * FROM todo ORDER BY modified DESC LIMIT 1")
    fun observe(): Flowable<TODORoomEntity>

    @Query("SELECT todo_id FROM deleted_todos ORDER BY id DESC")
    fun observeDeleted(): Flowable<List<Long>>

    @Query("""
        SELECT
            SUM(_all) as 'all',
            SUM(completed) as 'completed',
            SUM(not_completed) as 'not_completed',
            SUM(about_due) as 'about_due',
            SUM(passed_due) as 'passed_due',
            SUM(no_due) as 'no_due'
        FROM
        (SELECT
            COUNT(*) as '_all',
            COUNT(CASE completed WHEN 1 THEN 1 ELSE NULL END) as 'completed',
            COUNT(CASE completed WHEN 0 THEN 1 ELSE NULL END) as 'not_completed',
            null as 'about_due',
            null as 'passed_due',
            null as 'no_due'
        FROM todo
        UNION
        /*about due count*/
        SELECT
            null, null, null, COUNT(*), null, null
        FROM todo  WHERE completed = 0
            AND datetime(due_date/1000,'unixepoch','localtime')
                    BETWEEN datetime('now','localtime')
                    AND datetime('now','localtime','+24 hours')
        UNION
        /*passed due count*/
        SELECT
            null, null, null, null, COUNT(*), null
        FROM todo WHERE completed = 0 AND datetime(due_date/1000,'unixepoch','localtime') < datetime('now', 'localtime')
        UNION
        /*no due count*/
        SELECT
            null, null, null, null, null, COUNT(*)
        FROM todo WHERE completed = 0 AND due_date IS NULL)
    """)
    fun observeAggregator(): Flowable<FilterAggregator>

    @Query("""
        SELECT
            SUM(_all) as 'all',
            SUM(completed) as 'completed',
            SUM(not_completed) as 'not_completed',
            SUM(about_due) as 'about_due',
            SUM(passed_due) as 'passed_due',
            SUM(no_due) as 'no_due'
        FROM
        (SELECT
            COUNT(*) as '_all',
            COUNT(CASE completed WHEN 1 THEN 1 ELSE NULL END) as 'completed',
            COUNT(CASE completed WHEN 0 THEN 1 ELSE NULL END) as 'not_completed',
            null as 'about_due',
            null as 'passed_due',
            null as 'no_due'
        FROM todo
        UNION
        /*about due count*/
        SELECT
            null, null, null, COUNT(*), null, null
        FROM todo  WHERE completed = 0
            AND datetime(due_date/1000,'unixepoch','localtime')
                    BETWEEN datetime('now','localtime')
                    AND datetime('now','localtime','+24 hours')
        UNION
        /*passed due count*/
        SELECT
            null, null, null, null, COUNT(*), null
        FROM todo WHERE completed = 0 AND datetime(due_date/1000,'unixepoch','localtime') < datetime('now', 'localtime')
        UNION
        /*no due count*/
        SELECT
            null, null, null, null, null, COUNT(*)
        FROM todo WHERE completed = 0 AND due_date IS NULL)
    """)
    fun getAggregation():FilterAggregator

    @Query("SELECT COUNT(*) FROM todo")
    fun size(): Long
    
    @Query("""
        SELECT * FROM todo WHERE UPPER(title) LIKE :textLike ORDER BY creation_date
            DESC LIMIT :limit OFFSET :pageOffset
    """)
    fun search(pageOffset: Int, limit: Int, textLike: String): List<TODORoomEntity>

}

enum class SearchLike{
    SUFFIX, PREFIX, BOTH
}

/**
 * append wildcard sign "%" to searching term. See [SearchLike] flags
 */
fun String.like(searchLike: SearchLike = SearchLike.BOTH)=
        when(searchLike){
            SearchLike.PREFIX -> "%$this"
            SearchLike.SUFFIX -> "$this%"
            SearchLike.BOTH -> "%$this%"
        }