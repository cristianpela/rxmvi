package com.crskdev.rxmvi.presentation.list

import android.support.test.runner.AndroidJUnit4
import com.crskdev.rxmvi.*
import com.crskdev.rxmvi.domain.PAGE_FILTER_UNKNOWN
import com.crskdev.rxmvi.presentation.TODOModelViewEntity
import com.crskdev.rxmvi.util.TestTODOConductorActivity
import com.crskdev.rxmvicore.presentation.Status
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


/**
 * .
 * Created by Cristian Pela on 18.03.2018.
 */
//@RunWith(AndroidJUnit4::class)
class TODOListConductorControllerTest {

    init {
      //  RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

//    val todos = listOf<TODOModelViewEntity>(
//            TODOModelViewEntity(
//                    1,
//                    "2018.03.18 17:00",
//                    "",
//                    R.color.res_color_todo_no_due,
//                    "Cristian",
//                    "Some TODO",
//                    "",
//                    false))
//
//    @JvmField
//    @Rule
//    var applicationRule = ApplicationTestRule.defaultApplicationTestRule<TestTODOConductorActivity>()
//
//    @Before
//    fun setup() {
//        applicationRule.getActivity()?.provideController(TODOListConductorController().apply {
//            injectedStreamOps = object : SimpleMVIStreamOps<TODOListState>() {
//                override fun observeState(): Observable<TODOListState> =
//                        Observable.just(
//                                TODOListState(Status.IDLE, PAGE_FILTER_UNKNOWN, todos)
//                        )
//            }
//        })
//    }


//    @Test
//    fun should_show_todos() {
//      //  applicationRule.getActivity()?.findViewById<TextView>(R.id.textView)?.text.assertEqAndroid(todos.toString())
//    }
}

typealias AndroidIntent = android.content.Intent


