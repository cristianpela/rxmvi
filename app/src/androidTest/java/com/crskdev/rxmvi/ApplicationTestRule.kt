package com.crskdev.rxmvi

import android.app.Activity
import android.app.Application
import android.app.Instrumentation
import android.content.ComponentName
import android.content.pm.ActivityInfo
import android.os.Handler
import android.os.Looper
import android.support.test.InstrumentationRegistry
import com.crskdev.rxmvi.presentation.list.AndroidIntent
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import java.util.concurrent.CountDownLatch
import java.util.concurrent.atomic.AtomicReference

class ApplicationTestRule<out T : Application, out V : Activity>(private val activityClass: Class<V>,
                                                                 private val appClass: Class<T>, private val activityIntent: AndroidIntent? = null) : TestRule {

    companion object {
        inline fun <reified V : Activity> defaultApplicationTestRule(activityIntent: AndroidIntent? = null): ApplicationTestRule<Application, V>{
            return ApplicationTestRule(V::class.java, Application::class.java, activityIntent)
        }
    }

    private val applicationRef: AtomicReference<T> = AtomicReference()

    private val activityRef: AtomicReference<V> = AtomicReference()

    private val mainHandler = Handler(Looper.getMainLooper())

    fun getActivity(): V? = activityRef.get()

    @Suppress("UNCHECKED_CAST")
    override fun apply(base: Statement, description: Description?): Statement =
            object : Statement() {
                override fun evaluate() {

                    val latch = CountDownLatch(1)
                    val throwableRef = AtomicReference<Throwable>()

                    //prep and run test on main thread
                    mainHandler.post {
                        try {
                            val instrumentation = InstrumentationRegistry.getInstrumentation()
                            val ctx = instrumentation.targetContext.apply {
                                setTheme(R.style.AppTheme)
                            }
                            instrumentation.setInTouchMode(false)

                            val application = Instrumentation.newApplication(appClass, ctx) as T
                            applicationRef.set(application)

                            val activityName = activityClass.name
                            val componentName = ComponentName(ctx, activityClass)
                            val intent = activityIntent ?: AndroidIntent(ctx, activityClass)
                                    .setComponent(componentName)

                            val activity = instrumentation.newActivity(
                                    activityClass,
                                    ctx,
                                    null,
                                    application,
                                    intent,
                                    ActivityInfo(),
                                    activityName,
                                    null,
                                    null,
                                    null) as V
                            activityRef.set(activity)

                            application.onCreate()
                            instrumentation.callActivityOnCreate(activity, null)
                            //pre - test
                            base.evaluate()
                            //post - test
                            instrumentation.callActivityOnDestroy(activity)
                            application.onTerminate()
                        } catch (t: Throwable) {
                            throwableRef.set(t)
                        } finally {
                            latch.countDown()
                        }

                        throwableRef.get()?.let { throw  it }
                    }

                }
            }


}