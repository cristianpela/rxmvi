package com.crskdev.rxmvi.data.room

import android.arch.core.executor.ArchTaskExecutor
import android.arch.core.executor.TaskExecutor
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import com.crskdev.rxmvi.util.BaseTest
import org.junit.After
import org.junit.Before


/**
 * Created by Cristian Pela on 12.04.2018.
 */
abstract class BaseDBTest : BaseTest() {

    protected lateinit var db: TODODatabase

    protected lateinit var todoDao: TODODao


    @Before
    fun before() {
        ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
            override fun executeOnDiskIO(runnable: Runnable) {
                runnable.run()
            }

            override fun postToMainThread(runnable: Runnable) {
                runnable.run()
            }

            override fun isMainThread(): Boolean {
                return true
            }
        })

        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, TODODatabase::class.java).allowMainThreadQueries().build()
        todoDao = db.todoDao()
    }

    @After
    fun after() {
        db.close()
        ArchTaskExecutor.getInstance().setDelegate(null)
    }

}