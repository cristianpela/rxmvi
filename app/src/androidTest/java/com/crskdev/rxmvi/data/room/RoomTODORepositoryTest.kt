package com.crskdev.rxmvi.data.room

import android.support.test.filters.SmallTest
import android.support.test.runner.AndroidJUnit4
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.entity.TODOEntity.Companion.NO_ID
import com.crskdev.rxmvi.util.MockedTimeProviderAndroid
import com.crskdev.rxmvi.util.assertEqAndroid
import com.crskdev.rxmvi.util.generateTODOsAndroid
import com.crskdev.rxmvicore.util.singletonList
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit

/**
 * Created by Cristian Pela on 12.04.2018.
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class RoomTODORepositoryTest : BaseDBTest() {

    private var timeProvider = MockedTimeProviderAndroid(2018, 4, 12, 16, 30)

    @Test
    fun upsert() {
        val repo = RoomTODORepository(
                timeProvider,
                db.todoDao(),
                Schedulers.trampoline())
        repo.upsert(TODOEntity(-1,
                -1,
                null,
                "foo",
                "title"))
                .test()
                .assertNoErrors()
                .assertValue(1)

    }

    @Test
    fun upsertDao() {
        //insert
        todoDao.upsert(TODOEntity(-1,
                -1,
                null,
                "foo",
                "title")
                .toDb(timeProvider()))
                .assertEqAndroid(listOf(1L))
        //find
//        todoDao.findOne(1L).assertEqAndroid(
//                TODORoomEntity(1L, timeProvider(), null, "foo", "title",
//                        null, false, timeProvider()))
        //not found
        todoDao.findOne(100L).assertEqAndroid(null)
        //updated
        val todoUpdate = generateTODOsAndroid(timeProvider()).first()
        todoDao.upsert(todoUpdate.toDb(timeProvider()))
        todoDao.findOne(1L).assertEqAndroid(
                TODORoomEntity(1L, timeProvider(), null, "A", "A",
                        "A", false, timeProvider()))
    }

    @Test
    fun completeDao() {
        val todo = generateTODOsAndroid(timeProvider()).first()
        val id = todoDao.upsert(todo.toDb(timeProvider())).first()
        todoDao.complete(id, true, timeProvider() +1L)
        todoDao.findOne(id)?.let {
            it.id.assertEqAndroid(1L)
            it.modified.assertEqAndroid(timeProvider() +1L)
        }
        //try to complete a nonexistent to-do
        todoDao.complete(100L, true, timeProvider()).assertEqAndroid(0L)
    }

    @Test
    fun pagingGetAll() {
        //offset each creation date by nth hour
        val todos = generateTODOsAndroid(timeProvider(), 5)
                .mapIndexed { index, todo ->
                    todo.copy(creationDate = todo.creationDate + TimeUnit.HOURS.toMillis(index.toLong()))
                }

        //insert first [before insert, we remove ids just in case]
        todoDao.upsert(*todos.map { it.copy(id = NO_ID).toDb(timeProvider()) }.toTypedArray())
        todoDao.size().assertEqAndroid(5L)

        // expected page collection
        val expectedTodos = todos.reversed()

        val pageTracker = DaoPageTrackerFacade(2, timeProvider, todoDao)

        pageTracker.nextPageResult(TODOFilter.ALL, true)
                .map { it.toDomain() }
                .assertEqAndroid(expectedTodos.subList(0, 2))
        pageTracker.nextPageResult(TODOFilter.ALL, false)
                .map { it.toDomain() }
                .assertEqAndroid(expectedTodos.subList(2, 4))
        pageTracker.nextPageResult(TODOFilter.ALL, false)
                .map { it.toDomain() }
                .assertEqAndroid(expectedTodos.subList(4, 5))
        pageTracker.nextPageResult(TODOFilter.ALL, false)
                .map { it.toDomain() }
                .assertEqAndroid(emptyList())
        pageTracker.nextPageResult(TODOFilter.ALL, false)
                .map { it.toDomain() }
                .assertEqAndroid(emptyList())
    }

    @Test
    fun getAllCompletedAndNotCompleted() {

        timeProvider = MockedTimeProviderAndroid(System.currentTimeMillis())

        val todos = generateTODOsAndroid(timeProvider(), 2)
                .mapIndexed { index, todo ->
                    if (index == 0)
                        todo.copy(completed = true)
                    else
                        todo
                }.apply {
                    todoDao.upsert(*this.map { it.copy(id = NO_ID).toDb(timeProvider()) }.toTypedArray())
                }

        DaoPageTrackerFacade(2, timeProvider, todoDao).run {
            nextPageResult(TODOFilter.COMPLETED, true)
                    .map { it.toDomain() }
                    .takeIf { it.isNotEmpty() }
                    ?.first()
                    .assertEqAndroid(todos.first())

            nextPageResult(TODOFilter.NOT_COMPLETED, true)
                    .map { it.toDomain() }
                    .takeIf { it.isNotEmpty() }
                    ?.first()
                    .assertEqAndroid(todos[1])
        }

    }

    @Test
    fun getNoDue() {
        timeProvider = MockedTimeProviderAndroid(System.currentTimeMillis())
        generateTODOsAndroid(timeProvider(), 4).mapIndexed { index, todo ->
            when (index) {
                0 -> todo.copy(completed = true)
                1 -> todo.copy(dueDate = timeProvider() + TimeUnit.HOURS.toMillis(15))
                else -> todo
            }
        }.apply {
            todoDao.upsert(*map { it.copy(id = NO_ID) }.map { it.toDb(timeProvider()) }.toTypedArray())
        }.run {
            DaoPageTrackerFacade(2, timeProvider, todoDao).let {
                it.nextPageResult(TODOFilter.NO_DUE, true)
                        .takeIf { it.isNotEmpty() }
                        ?.map { it.toDomain() }
                        .assertEqAndroid(subList(2, 4))
            }
        }
    }


    @Test
    fun getPassedDue() {
        timeProvider = MockedTimeProviderAndroid(System.currentTimeMillis())
        generateTODOsAndroid(timeProvider(), 4).mapIndexed { index, todo ->
            when (index) {
                0 -> todo.copy(completed = true)
                1 -> todo.copy(dueDate = timeProvider() - TimeUnit.HOURS.toMillis(15))
                else -> todo
            }
        }.apply {
            todoDao.upsert(*map { it.copy(id = NO_ID) }.map { it.toDb(timeProvider()) }.toTypedArray())
        }.run {
            DaoPageTrackerFacade(2, timeProvider, todoDao).let {
                it.nextPageResult(TODOFilter.PASSED_DUE, true)
                        .takeIf { it.isNotEmpty() }
                        ?.map { it.toDomain() }
                        .assertEqAndroid(get(1).singletonList())
            }
        }
    }

    @Test
    fun getAboutDue() {
        timeProvider = MockedTimeProviderAndroid(System.currentTimeMillis())
        generateTODOsAndroid(timeProvider(), 4).mapIndexed { index, todo ->
            when (index) {
                0 -> todo.copy(dueDate = timeProvider() + TimeUnit.HOURS.toMillis(30))
                1 -> todo.copy(dueDate = timeProvider() + TimeUnit.HOURS.toMillis(15))
                else -> todo
            }
        }.apply {
            todoDao.upsert(*map { it.copy(id = NO_ID) }.map { it.toDb(timeProvider()) }.toTypedArray())
        }.run {
            DaoPageTrackerFacade(2, timeProvider, todoDao).let {
                it.nextPageResult(TODOFilter.ABOUT_DUE, true)
                        .takeIf { it.isNotEmpty() }
                        ?.map { it.toDomain() }
                        .assertEqAndroid(get(1).singletonList())
            }
        }
    }


    @Test
    fun aggregatorTesting() {
        timeProvider = MockedTimeProviderAndroid(System.currentTimeMillis())

        val todos = generateTODOsAndroid(timeProvider(), 5, true)
                .mapIndexed { index, todo ->
                    when (index) {
                    //due in 10 hours
                        0 -> todo.copy(dueDate = timeProvider() + TimeUnit.HOURS.toMillis(10))
                    //passed due
                        1 -> todo.copy(dueDate = timeProvider() - TimeUnit.HOURS.toMillis(10))
                    // completed
                        2 -> todo.copy(completed = true)
                        3 -> todo.copy(dueDate = timeProvider() + TimeUnit.HOURS.toMillis(30))
                        else -> todo
                    }
                }

        val test = todoDao.observeAggregator().test()

        todoDao.upsert(*todos.map { it.toDb(timeProvider()) }.toTypedArray())

        todoDao.size().assertEqAndroid(5)

        test.assertValueCount(2)
                .assertValueAt(1, FilterAggregator(5, 1, 4, 1, 1, 1))

    }

    @Test
    fun testObservation() {
        val todos = generateTODOsAndroid(timeProvider(), 4)
        val test = todoDao.observe().test()
        todoDao.upsert(*todos.map { it.copy(id = NO_ID) }.map { it.toDb(timeProvider()) }.toTypedArray())
        test.values().last().toDomain().assertEqAndroid(todos[0])
        todoDao.upsert(todos[1].copy(detail = "FOO").toDb(timeProvider() + 1))
        test.values().last().toDomain().assertEqAndroid(todos[1].copy(detail = "FOO"))
    }

    @Test
    fun testSearch() {
        val todos = generateTODOsAndroid(timeProvider(), 4)
        todoDao.upsert(*todos.map { it.copy(id = NO_ID) }.map { it.toDb(timeProvider()) }.toTypedArray())
        todoDao.search(0, 10, "A")
                .size.assertEqAndroid(1)
    }


    @Test
    fun observeDeleted(){
//        val todo = generateTODOsAndroid(timeProvider()).first() // is not passing since trigger is not working?
//        todoDao.upsert(todo.copy(id = NO_ID).toDb(timeProvider()))
//        val test = todoDao.observeDeleted().test()
//        todoDao.delete(1L)
//        test.assertValue(listOf(1L))
    }
}