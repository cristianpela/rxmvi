package com.crskdev.rxmvi

import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.Message
import com.crskdev.rxmvicore.presentation.MVIStreamOps
import com.crskdev.rxmvicore.presentation.State
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables

/**
 * .
 * Created by Cristian Pela on 19.03.2018.
 */
abstract class SimpleMVIStreamOps<S : State> : MVIStreamOps<S> {

    override fun bindIntents(intentStream: Observable<Intent>): Disposable = Disposables.empty()

    override fun observeState(): Observable<S> = Observable.empty()

    override fun observeErrors(): Observable<Throwable> = Observable.empty()

    override fun observeMessages(): Observable<Message<*>> = Observable.empty()

    override fun start() = Unit

}