@file:Suppress("unused")

package com.crskdev.rxmvi.util

import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.platform.TimeProvider
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.test.assertEquals

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
fun <T> T.assertEqAndroid(expected: T, msg: String? = null) =
        assertEquals(expected, this, msg)

fun generateTODOsAndroid(creationDate: Long? = null, size: Int = 1, noIds: Boolean = false): List<TODOEntity> {
    val letters = arrayOf("A", "B", "C", "D", "E")
    return generateSequence(0) { it + 1 }
            .take(size)
            .map {
                val id = if (noIds) -1L else it.toLong() + 1
                val passes = it / letters.size
                val txt = letters[it.rem(letters.size)].repeat(passes + 1)
                TODOEntity(id, creationDate ?: -1, null, txt, txt, txt, false)
            }.toList()

}

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
class MockedTimeProviderAndroid(private val seed: Long = 0) : TimeProvider {
    constructor(year: Int, month: Int, day: Int, hour: Int, minute: Int = 0, second: Int = 0) :
            this(Calendar.getInstance().apply {
                set(year, month - 1, day, hour, minute, second)
            }.timeInMillis)

    override fun currentTimeMillis(): Long = seed

    operator fun invoke() = currentTimeMillis()
}

abstract class BaseTest() {
    init {
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }
    }
}


