package com.crskdev.rxmvi.presentation.upsert.edit

import com.crskdev.rxmvi.domain.*
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.env.SelectTODOIntent
import com.crskdev.rxmvi.domain.env.UpsertTODOIntent
import com.crskdev.rxmvi.domain.usecase.DeleteTODOUseCase
import com.crskdev.rxmvi.domain.usecase.SelectDueDateUseCase
import com.crskdev.rxmvi.domain.usecase.SelectTODOUsecase
import com.crskdev.rxmvi.domain.usecase.UpsertTODOUseCase
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvi.presentation.upsert.Mode
import com.crskdev.rxmvi.presentation.upsert.UpsertTODOState
import com.crskdev.rxmvi.util.*
import com.crskdev.rxmvicore.env.Intent
import com.crskdev.rxmvicore.env.ResourceIDError
import com.crskdev.rxmvicore.env.ResourceIDMessage
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.util.cast
import io.reactivex.rxkotlin.cast
import io.reactivex.subjects.PublishSubject
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
class EditTODOControllerTest : Spek({

    describe("tests tor edit todo controller") {

        val timeProvider = MockedTimeProvider(2018, 3, 15, 14)
        val repository = MockedTODORepository.instance(timeProvider())

        val validator = TODOValidator(MOCKED_RESOURCE_LOCATOR, timeProvider)
        val converter = TODOModelViewEntityConverter(timeProvider, MOCKED_RESOURCE_LOCATOR, DateFormatter())

        val addUseCase = UpsertTODOUseCase(repository, validator, MOCKED_RESOURCE_LOCATOR)
        val selectUseCase = SelectTODOUsecase(repository, MOCKED_RESOURCE_LOCATOR)

        val selectDueDateUseCase = SelectDueDateUseCase(timeProvider, DateFormatter())
        val deletedTODOUseCase = DeleteTODOUseCase(repository, MOCKED_RESOURCE_LOCATOR)
        val controller = EditTODOMVIController(
                addUseCase,
                selectUseCase,
                deletedTODOUseCase,
                selectDueDateUseCase,
                converter
        ).apply { start() }

        beforeEachTest {
            repository.refresh(generateTODOs(timeProvider()))
        }

        afterEachTest {
            repository.clear()
        }

        it("should pass al scenarios select/add/update") {
            val test = controller.observeState().skip(1).cast<Any>().mergeWith(
                    controller.observeMessages()
            ).test()

            val selector = PublishSubject.create<SelectTODOIntent>()
            val updater = PublishSubject.create<UpsertTODOIntent>()
            controller.bindIntents(selector.cast<Intent>()
                    .mergeWith(updater))

            selector.onNext(SelectTODOIntent(0)) // select
            val todo = generateTODOs(timeProvider())[0]
            test.values()[0].cast<UpsertTODOState>().todo.assertEq(todo.let { converter.convert(it) })

            updater.onNext(UpsertTODOIntent(converter.convert(todo).copy(detail = "FOO"), Mode.EDIT)) //update
            test.values()[1].cast<UpsertTODOState>().todo.assertEq(todo.copy(detail = "FOO").let { converter.convert(it) })
            test.values()[2].cast<ResourceIDMessage>().data[0].id.assertEq(RES_MSG_TODO_UPDATE)

            val addTODO = generateTODOs(timeProvider(), 2)[1]
            updater.onNext(UpsertTODOIntent(addTODO.let { converter.convert(it) })) //add
            test.values()[3].cast<UpsertTODOState>().todo.assertEq(TODOEntity.EMPTY.copy(author = addTODO.author).let { converter.convert(it) })
            test.values()[4].cast<ResourceIDMessage>().data[0].id.assertEq(RES_MSG_TODO_ADD)
        }

        it("should get error message when todo due date is not after now") {
            val test = controller.observeErrors().test()
            val todo = generateTODOs(timeProvider())[0].copy(author = "", dueDate = timeProvider() - 1) // empty author and due date before now!
            controller.bindIntents(UpsertTODOIntent(converter.convert(todo)).toObservable())
            test.values()[0].cast<ResourceIDError>()
                    .idMessage.data.map { it.id }.assertEq(listOf(RES_ERR_EMPTY_AUTHOR, RES_ERR_DUE_DATE_BEFORE_NOW))
        }

        it("should get error message when todo due date is invalid") {
            val test = controller.observeErrors().test()
            val todo = generateTODOs(timeProvider())[0].copy(author = "", dueDate = -1) // empty author and due date before now!
            controller.bindIntents(UpsertTODOIntent(converter.convert(todo)).toObservable())
            test.values()[0].cast<ResourceIDError>()
                    .idMessage.data.map { it.id }.assertEq(listOf(RES_ERR_EMPTY_AUTHOR, RES_ERR_DUE_DATE_INVALID))
        }
    }
})