package com.crskdev.rxmvi.presentation

import com.crskdev.rxmvi.domain.DateFormatter
import com.crskdev.rxmvi.domain.RES_COLOR_TODO_ALMOST_DUE
import com.crskdev.rxmvi.domain.RES_COLOR_TODO_NO_DUE
import com.crskdev.rxmvi.domain.RES_COLOR_TODO_PASSED_DUE
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.util.MOCKED_RESOURCE_LOCATOR
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvi.util.assertEq
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 13.03.2018.
 */
class TODOModelViewEntityConverterTest : Spek({
    describe("entity to model-view todo conversion tests") {

        val resourceIDLocator = MOCKED_RESOURCE_LOCATOR
        val timeProvider = MockedTimeProvider(2018, 3, 8, 16)
        val now = timeProvider.currentTimeMillis()
        val converter = TODOModelViewEntityConverter(timeProvider, resourceIDLocator, DateFormatter())

        it("should convert to the correct model view") {
            listOf(
                    TODOEntity(1,
                            now - TimeUnit.DAYS.toMillis(2),
                            now, "User1", "TODO1", null),
                    TODOEntity(2,
                            now - TimeUnit.DAYS.toMillis(2),
                            null, "User1", "TODO1", null),
                    TODOEntity(3,
                            now - TimeUnit.DAYS.toMillis(2),
                            now + TimeUnit.DAYS.toMillis(1), "User1", "TODO1", null)
            ).map {
                converter.convert(it)
            }.assertEq(
                    listOf(
                            TODOModelViewEntity(1,
                                    "2018-03-06 16:00",
                                    now - TimeUnit.DAYS.toMillis(2),
                                    "2018-03-08 16:00",
                                    true,
                                    now,
                                    false,
                                    RES_COLOR_TODO_PASSED_DUE,
                                    "User1",
                                    "TODO1",
                                    null,
                                    false
                            ),
                            TODOModelViewEntity(2,
                                    "2018-03-06 16:00", now - TimeUnit.DAYS.toMillis(2),
                                    "",
                                    false,
                                    null,
                                    false,
                                    RES_COLOR_TODO_NO_DUE,
                                    "User1",
                                    "TODO1",
                                    null,
                                    false
                            ),
                            TODOModelViewEntity(3,
                                    "2018-03-06 16:00", now - TimeUnit.DAYS.toMillis(2),
                                    "2018-03-09 16:00",
                                    true,
                                    now + TimeUnit.DAYS.toMillis(1),
                                    true,
                                    RES_COLOR_TODO_ALMOST_DUE,
                                    "User1",
                                    "TODO1",
                                    null,
                                    false)
                    )
            )
        }

    }
})