package com.crskdev.rxmvi.presentation.list

import com.crskdev.rxmvi.domain.DateFormatter
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.presentation.TODOModelViewEntityConverter
import com.crskdev.rxmvi.util.MOCKED_RESOURCE_LOCATOR
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvi.util.assertEq
import com.crskdev.rxmvi.util.generateTODOs
import com.crskdev.rxmvicore.presentation.Carrier
import com.crskdev.rxmvicore.presentation.Status
import com.crskdev.rxmvicore.util.singletonList
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.util.concurrent.TimeUnit

/**
 * .
 * Created by Cristian Pela on 19.03.2018.
 */
class TODOListMVIControllerTest : Spek({

    val timeProvider = MockedTimeProvider(2018, 3, 20, 11)
    val converter = TODOModelViewEntityConverter(timeProvider, MOCKED_RESOURCE_LOCATOR, DateFormatter())

    describe("diffed state mvi controller tests") {
        //        it("should diff the state") {
//            val todos = generateTODOs(timeProvider())
//            val repository = MockedTODORepository.instance(timeProvider(), todos)
//            val controller = MVIStreamOps.start(TODOListMVIController(
//                    converter,
//                    FetchTODOsUseCase(MockedTODOFilterPersistor, repository, MOCKED_RESOURCE_LOCATOR),
//                    FilterSaveUseCase(MockedTODOFilterPersistor),
//                    WatchTODOUseCase(repository),
//                    CompleteTODOUseCase(repository)))
//            val test = controller.observeState().skip(1).test()
//            val intentPusher: Subject<Intent> = PublishSubject.create<Intent>()
//            controller.bindIntents(intentPusher as Observable<Intent>)
//            intentPusher.onNext(FetchTODOsIntent(PAGE_FILTER_UNKNOWN))
//            test.values()[0].assertEq(todos.map { it.copy(detail = null) }.map { converter.convert(it) }
//                    .let { TODOListState(Status.IDLE, PAGE_FILTER_UNKNOWN.copy(filter = TODOFilter.ALL), it) })
//            //retrieving same to-dos will trigger the diff state manager
//            intentPusher.onNext(FetchTODOsIntent(PAGE_FILTER_UNKNOWN))
//            test.values()[1].assertEq(todos.map { it.copy(detail = null) }.map { converter.convert(it) }
//                    .let { TODOListState(Status.IDLE, PAGE_FILTER_UNKNOWN.copy(filter = TODOFilter.ALL), null) })
//        }
    }

    describe("carrie state reducer mvi controller tests") {

        it("it should add a new page if is the same filter") {
            val newState = TODOListState(Status.IDLE, TODOFilter.ALL,
                    generateTODOs(0, 2).map { converter.convert(it) })
            Carrier.init(TODOListState(), StateCarrierHandler.provideAccumulator())
                    .next("", newState, Actions.FETCH)
                    .getState().assertEq(newState)
        }

        it("it should add a new page") {
            val todos = generateTODOs(0, 4).map { converter.convert(it) }
            val initialState = TODOListState(Status.IDLE, TODOFilter.ALL, todos.subList(0, 2))
            Carrier.init(initialState, StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.ALL, todos.subList(2, 4)), Actions.FETCH)
                    .getState().assertEq(TODOListState(Status.IDLE, TODOFilter.ALL, todos))
        }

        it("it should change the filter and replace the with new todos") {
            val todos = generateTODOs(0, 4).map { converter.convert(it) }
            val completedTodos = generateTODOs(0, 4).map { it.copy(completed = true).let { converter.convert(it) } }
            val initialState = TODOListState(Status.IDLE, TODOFilter.ALL, todos)
            Carrier.init(initialState, StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.COMPLETED, completedTodos), Actions.FETCH)
                    .getState().assertEq(TODOListState(Status.IDLE, TODOFilter.COMPLETED, completedTodos))
        }

        it("it should update todo when FILTER is ALL and todo is changed to completed from uncompleted or vice-versa") {
            val todos = generateTODOs(0, 2).map { converter.convert(it) }
            val initialState = TODOListState(Status.IDLE, TODOFilter.ALL, todos)

            val updatedTodo = todos[0].copy(completed = true)
            Carrier.init(initialState, StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.ALL, updatedTodo.singletonList()), Actions.WATCH)
                    .getState()
                    .todos!![0]
                    .assertEq(updatedTodo)
        }


        it("it should not add a TODO after a WATCH action if that to todo is already in the list and is the same") {
            val todos = generateTODOs(0, 2).map {
                it.copy(completed = true)
            }.map { converter.convert(it) }
            val initialState = TODOListState(Status.IDLE, TODOFilter.COMPLETED, todos)

            val updatedTodo = todos[0].copy(completed = false)
            Carrier.init(initialState, StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.COMPLETED, updatedTodo.singletonList()), Actions.WATCH)
                    .getState()
                    .todos
                    .assertEq(todos.subList(1, 2))
        }

        it("it should not add a TODO newly created when Filter is Completed") {
            val newTodo = generateTODOs(timeProvider()).first().let { converter.convert(it) }
            Carrier.init(TODOListState(filter = TODOFilter.COMPLETED), StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.COMPLETED, newTodo.singletonList()), Actions.WATCH)
                    .getState()
                    .todos
                    .assertEq(null)
        }

        it("it should add a TODO newly created when Filter is NOT Completed") {
            val gen = generateTODOs(timeProvider(), size = 1)
            val newTodo = gen.first().copy(id = 222222).let { converter.convert(it) }
            Carrier.init(TODOListState(filter = TODOFilter.NOT_COMPLETED, todos = emptyList()), StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.NOT_COMPLETED, newTodo.singletonList()), Actions.WATCH)
                    .getState()
                    .todos
                    ?.first()
                    .assertEq(newTodo)
        }

        it("it should not add a TODO without a due or a due larger than " +
                " accepted when Filter is ABOUT_DUE") {
            val gen = generateTODOs(timeProvider(), size = 1)
            val newTodo = gen.first().let { converter.convert(it) }
            Carrier.init(TODOListState(filter = TODOFilter.ABOUT_DUE, todos = emptyList()), StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.ABOUT_DUE, newTodo.singletonList()), Actions.WATCH)
                    .getState()
                    .todos!!
                    .isEmpty()
                    .assertEq(true)
        }


        it("it should add a TODO with a due when Filter is ABOUT_DUE") {
            val gen = generateTODOs(timeProvider(), size = 1)
            val newTodo = gen.first().copy(dueDate = timeProvider() + TimeUnit.HOURS.toMillis(15)).let { converter.convert(it) }
            Carrier.init(TODOListState(filter = TODOFilter.ABOUT_DUE, todos = emptyList()), StateCarrierHandler.provideAccumulator())
                    .next("", TODOListState(Status.IDLE, TODOFilter.ABOUT_DUE, newTodo.singletonList()), Actions.WATCH)
                    .getState()
                    .todos!!
                    .isEmpty()
                    .assertEq(false)
        }


    }
})