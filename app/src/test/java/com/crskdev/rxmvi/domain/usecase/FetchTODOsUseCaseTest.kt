package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.env.FetchTODOsRequest
import com.crskdev.rxmvi.domain.env.FetchTODOsResponse
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvi.domain.gateway.TODORepository
import com.crskdev.rxmvi.util.MockedTODOFilterPersistor
import com.crskdev.rxmvi.util.MockedTODORepository
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvicore.util.cast
import io.reactivex.Observable
import io.reactivex.Single
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 13.03.2018.
 */
class FetchTODOsUseCaseTest : Spek({

    describe("fetch todos tests") {

        val emptyTODO = TODOEntity(1, 1L, null, "", "", null)

        val filterPersistor = object : MockedTODOFilterPersistor() {}

        it("should return an empty message when are no result fetched") {

            val repository: TODORepository = object : MockedTODORepository(MockedTimeProvider()) {
                override fun getAll(filter: TODOFilter, fromStart: Boolean, size: Int, includeDetail: Boolean): Single<List<TODOEntity>> {
                    return Single.just(emptyList())
                }
            }

            val useCase = FetchTODOsUseCase(filterPersistor, repository)

            Observable.just(FetchTODOsRequest(TODOFilter.ALL, true))
                    .compose(useCase.execute())
                    .test()
                    .assertNoErrors()
                    .assertValueAt(0) {
                        it.cast<FetchTODOsResponse>()
                                .todos == emptyList<TODOEntity>()
                    }
//                    .assertValueAt(1) {
//                        it.cast<MessageResponse>()
//                                .message
//                                .cast<ResourceIDMessage>()
//                                .data[0]
//                                .id == RES_MSG_FETCH_EMPTY_TODOS
//                    }
        }

        it("should return a list of ALL todos when UNKNOWN filter requested and when no previous filter is saved") {

            val repository: TODORepository = object : MockedTODORepository(MockedTimeProvider()) {
                override fun getAll(filter: TODOFilter, fromStart: Boolean, size: Int, includeDetail: Boolean): Single<List<TODOEntity>> = Single.just(listOf(emptyTODO))
            }

            val useCase = FetchTODOsUseCase(filterPersistor, repository)

            Observable.just(FetchTODOsRequest(TODOFilter.ALL, true))
                    .compose(useCase.execute())
                    .test()
                    .assertNoErrors()
                    .assertValue { it.cast<FetchTODOsResponse>().todos == listOf(emptyTODO) }
        }

        it("should return a list of COMPLETED todos when an UNKNOWN filter is requested and COMPLETED filter is previously saved") {

            val repository: TODORepository = object : MockedTODORepository(MockedTimeProvider()) {

                override fun getAll(filter: TODOFilter, fromStart: Boolean, size: Int, includeDetail: Boolean): Single<List<TODOEntity>> = Single.create {
                    when (filter) {
                        TODOFilter.COMPLETED -> it.onSuccess(listOf(emptyTODO))
                        else -> it.onError(Error("Bad Filter: Must Provide ${TODOFilter.COMPLETED}"))
                    }
                }
            }

            val customFilterPersistor: TODOFilterPersistor = object : MockedTODOFilterPersistor() {
                override fun load(defaultForEmpty: TODOFilter): Single<TODOFilter> = Single.just(TODOFilter.COMPLETED)
            }

            val useCase = FetchTODOsUseCase(customFilterPersistor, repository)

            Observable.just(FetchTODOsRequest(TODOFilter.COMPLETED, true))
                    .compose(useCase.execute())
                    .test()
                    .assertNoErrors()
                    .assertValue { it.cast<FetchTODOsResponse>().todos == listOf(emptyTODO) }
        }

    }
})