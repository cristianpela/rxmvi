package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_ERR_FETCH_EMPTY_SINGLE_TODO
import com.crskdev.rxmvi.domain.env.SelectTODORequest
import com.crskdev.rxmvi.domain.env.SelectTODOResponse
import com.crskdev.rxmvi.util.MOCKED_RESOURCE_LOCATOR
import com.crskdev.rxmvi.util.MockedTODORepository
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvi.util.generateTODOs
import com.crskdev.rxmvicore.env.ErrorResponse
import com.crskdev.rxmvicore.env.ResourceIDError
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.util.cast
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
class SelectTODOUsecaseTest : Spek({
    describe("select todo") {

        val timeProvider = MockedTimeProvider(2018, 3, 15, 12)
        val repository = MockedTODORepository.instance(timeProvider(), generateTODOs(timeProvider(), 1))
        val useCase = SelectTODOUsecase(repository, MOCKED_RESOURCE_LOCATOR)

        it("should select todo by id") {
            SelectTODORequest(0).toObservable()
                    .compose(useCase.execute())
                    .test()
                    .assertNoErrors()
                    .assertValue { it.cast<SelectTODOResponse>().todo == generateTODOs(timeProvider(), 1).first() }
        }
        it("should message if todo was not found") {
            SelectTODORequest(-1).toObservable()
                    .compose(useCase.execute())
                    .test()
                    .assertNoErrors()
                    .assertValue {
                        it.cast<ErrorResponse>()
                                .throwable
                                .cast<ResourceIDError>()
                                .idMessage
                                .data[0].id == RES_ERR_FETCH_EMPTY_SINGLE_TODO
                    }
        }
    }
})