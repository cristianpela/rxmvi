package com.crskdev.rxmvi.domain

import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.util.MOCKED_RESOURCE_LOCATOR
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvi.util.assertEq
import com.crskdev.rxmvi.util.identityMap
import com.crskdev.rxmvicore.util.Validator
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */

class TODOValidatorTest : Spek({

    describe("todo check tests") {

        val now = System.currentTimeMillis()

        val bareTODO = TODOEntity(
                1L,
                -1L,
                null,
                "",
                "",
                "")
        val validator: Validator<TODOEntity> = TODOValidator(MOCKED_RESOURCE_LOCATOR,
                MockedTimeProvider(now))

        it("should have title, author, creation date") {
            val todo = bareTODO.copy(author = "Foo", title = "Bar", creationDate = 1L)
            validator.check<List<Int>>(todo)
                    .onPass { it.data.map { it.id } }
                    .get()
                    .assertEq(listOf<Int>(RES_OK))
        }

        it("should fail if no title, author, creation date") {
            validator.check<List<Int>>(bareTODO)
                    .onFail { it.data.map { it.id } }
                    .get()
                    .assertEq(listOf(RES_ERR_EMPTY_AUTHOR, RES_ERR_EMPTY_TITLE))
        }

        it("should fail if due date is before now") {
            val todo = bareTODO.copy(author = "Foo", title = "Bar", creationDate = 1L, dueDate = now - 1000)
            validator.check<List<Int>>(todo)
                    .onFail { it.data.map { it.id } }
                    .get()
                    .assertEq(listOf(RES_ERR_DUE_DATE_BEFORE_NOW))
        }
    }

})