package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.env.WatchTODORequest
import com.crskdev.rxmvi.domain.env.WatchTODOResponse
import com.crskdev.rxmvi.util.MockedTODORepository
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvi.util.generateTODOs
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.util.cast
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 23.03.2018.
 */
class WatchTODOUseCaseTest : Spek({

    describe("testing watching repository changes") {
        it("should trigger a change") {
            val timeProvider = MockedTimeProvider(2018, 3, 23, 16, 0)
            val data = generateTODOs(timeProvider())
            val repo = MockedTODORepository.instance(timeProvider(), data)
            val useCase = WatchTODOUseCase(repo)

            val test = WatchTODORequest(false).toObservable()
                    .compose(useCase.execute())
                    .doOnNext{
                        println(it)
                    }
                    .take(1)
                    .test()

            repo.complete(0, true).subscribe()

            println(test.values())

            test.assertValueCount(1)
                    .assertValue { it.cast<WatchTODOResponse>().todo == data.first().copy(id = 0, completed = true) }
        }
    }
})