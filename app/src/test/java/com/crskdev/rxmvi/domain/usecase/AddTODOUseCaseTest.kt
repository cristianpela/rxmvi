package com.crskdev.rxmvi.domain.usecase

import com.crskdev.rxmvi.domain.RES_MSG_TODO_ADD
import com.crskdev.rxmvi.domain.RES_MSG_TODO_UPDATE
import com.crskdev.rxmvi.domain.TODOValidator
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.env.UpsertTODORequest
import com.crskdev.rxmvi.domain.env.SelectTODOResponse
import com.crskdev.rxmvi.presentation.upsert.Mode
import com.crskdev.rxmvi.util.MOCKED_RESOURCE_LOCATOR
import com.crskdev.rxmvi.util.MockedTODORepository
import com.crskdev.rxmvi.util.MockedTimeProvider
import com.crskdev.rxmvi.util.generateTODOs
import com.crskdev.rxmvicore.env.MessageResponse
import com.crskdev.rxmvicore.env.ResourceIDMessage
import com.crskdev.rxmvicore.rx.toObservable
import com.crskdev.rxmvicore.util.cast
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * .
 * Created by Cristian Pela on 15.03.2018.
 */
class AddTODOUseCaseTest : Spek({

    val timeProvider = MockedTimeProvider(2018, 3, 15, 13)
    val validator = TODOValidator(MOCKED_RESOURCE_LOCATOR, timeProvider)
    val repository = MockedTODORepository.instance(timeProvider())
    val useCase = UpsertTODOUseCase(repository, validator, MOCKED_RESOURCE_LOCATOR)

    beforeEachTest {
        repository.refresh(generateTODOs(timeProvider(), 1))
    }

    afterEachTest {
        repository.clear()
    }

    describe("adding or updating tests") {
        it("should add new todo") {
            val todo = generateTODOs(timeProvider(), 2, true)[1] // NEW ONE
            UpsertTODORequest(todo)
                    .toObservable()
                    .compose(useCase())
                    .test()
                    .assertValueAt(0) {
                        it.cast<SelectTODOResponse>().todo == TODOEntity.EMPTY.copy(author = todo.author)
                    }
                    .assertValueAt(1) {
                        it.cast<MessageResponse>()
                                .message
                                .cast<ResourceIDMessage>()
                                .data[0].id == RES_MSG_TODO_ADD
                    }
            repository.getOne(1).test()
                    .assertValue(todo.copy(id = 1))
        }
        it("should update the todo") {
            val todo = generateTODOs(timeProvider(), 1, true)[0].copy(id = 0, detail = "FOO")
            UpsertTODORequest(todo, Mode.EDIT)
                    .toObservable()
                    .compose(useCase())
                    .test()
                    .assertValueAt(0) {
                        it.cast<SelectTODOResponse>().todo == todo
                    }
                    .assertValueAt(1) {
                        it.cast<MessageResponse>()
                                .message
                                .cast<ResourceIDMessage>()
                                .data[0].id == RES_MSG_TODO_UPDATE
                    }
        }
    }
})