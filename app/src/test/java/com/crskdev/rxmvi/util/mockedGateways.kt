package com.crskdev.rxmvi.util

import com.crskdev.rxmvi.data.InMemoryTODORepository
import com.crskdev.rxmvi.domain.ResourceIDLocator
import com.crskdev.rxmvi.domain.TODOFilter
import com.crskdev.rxmvi.domain.entity.TODOEntity
import com.crskdev.rxmvi.domain.gateway.ClearableTODORepository
import com.crskdev.rxmvi.domain.gateway.TODOFilterPersistor
import com.crskdev.rxmvi.domain.getAllResourcesIDs
import com.crskdev.rxmvi.platform.TimeProvider
import io.reactivex.Observable
import java.util.*

/**
 * .
 * Created by Cristian Pela on 13.03.2018.
 */

val MOCKED_RESOURCE_LOCATOR = ResourceIDLocator(identityMap(*getAllResourcesIDs()))

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
class MockedTimeProvider(private val seed: Long = 0) : TimeProvider {
    constructor(year: Int, month: Int, day: Int, hour: Int, minute: Int = 0, second: Int = 0) :
            this(Calendar.getInstance().apply {
                set(year, month - 1, day, hour, minute, second)
            }.timeInMillis)

    override fun currentTimeMillis(): Long = seed

    operator fun invoke() = currentTimeMillis()
}

abstract class MockedTODOFilterPersistor : TODOFilterPersistor{
    override fun observerFilterChanges(defaultForEmpty: TODOFilter): Observable<TODOFilter> =
            Observable.just(defaultForEmpty)

}

/**
 * .
 * Created by Cristian Pela on 08.03.2018.
 */
abstract class MockedTODORepository(
        private val timeProvider: TimeProvider,
        data: List<TODOEntity> = emptyList()
) : ClearableTODORepository by InMemoryTODORepository(timeProvider, data) {

    companion object {
        fun instance(now: Long = 0, data: List<TODOEntity> = emptyList()): ClearableTODORepository =
                object : MockedTODORepository(MockedTimeProvider(now), data) {}
    }
}


fun generateTODOs(creationDate: Long, size: Int = 1, noIds: Boolean = false): List<TODOEntity> {
    val letters = arrayOf("A", "B", "C", "D", "E")
    return generateSequence(0) { it + 1 }
            .take(size)
            .map {
                val id = if (noIds) -1L else it.toLong()
                val passes = it / letters.size
                val txt = letters[it.rem(letters.size)].repeat(passes + 1)
                TODOEntity(id, creationDate, null, txt, txt, txt, false)
            }.toList()

}