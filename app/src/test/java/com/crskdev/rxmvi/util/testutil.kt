@file:Suppress("unused")

package com.crskdev.rxmvi.util

import com.crskdev.rxmvicore.rx.SchedulersContract
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

/**
 * .
 * Created by Cristian Pela on 04.03.2018.
 */
fun <T> T.assertEq(expected: T, msg: String? = null) =
        assertEquals(expected, this, msg)


class TestSchedulers(numbers: Int = 1) : Iterable<TestScheduler> {

    companion object {
        fun single(): TestScheduler {
            val (scheduler) = TestSchedulers()
            return scheduler
        }
    }

    private val schedulers =
            if (numbers >= 1)
                generateSequence(0) { it + 1 }
                        .take(numbers)
                        .map { TestScheduler() }
                        .toList()
            else
                throw IllegalStateException("Number of schedulers must be at least greater than 1")


    fun schedulerAt(index: Int) = schedulers[index]

    fun advanceTimeBy(delayTime: Long, unit: TimeUnit) = schedulers.forEach { it.advanceTimeBy(delayTime, unit) }

    fun advanceTimeTo(delayTime: Long, unit: TimeUnit) = schedulers.forEach { it.advanceTimeTo(delayTime, unit) }

    override fun iterator(): Iterator<TestScheduler> = schedulers.iterator()

    operator fun component1() = schedulers[0]
    operator fun component2() = schedulers[1]
    operator fun component3() = schedulers[2]
    operator fun component4() = schedulers[3]
    operator fun component5() = schedulers[4]
    operator fun component6() = schedulers[5]
    operator fun component7() = schedulers[6]
    operator fun component8() = schedulers[7]
    operator fun component9() = schedulers[7]
    operator fun component10() = schedulers[9]
}

class TestSchedulersSpek : Spek({

    describe("testing test-scheduler batching") {
        it("should trigger assert when numbers are invalid") {
            assertFailsWith<IllegalStateException> { TestSchedulers(-1) }
        }
        it("should advance by time in batch") {
            TestSchedulers(2).apply {
                advanceTimeBy(10, TimeUnit.SECONDS)
            }.forEach {
                it.now(TimeUnit.SECONDS).assertEq(10L)
            }
        }
        it("should advance to time in batch") {
            TestSchedulers(2).apply {
                advanceTimeBy(10, TimeUnit.SECONDS)
                advanceTimeTo(50, TimeUnit.SECONDS)
            }.forEach {
                it.now(TimeUnit.SECONDS).assertEq(50L)
            }
        }
    }

})

fun testSchedulersContractForEach(): SchedulersContract =
        TestSchedulers(3).run {
            SchedulersContract.create(schedulerAt(0), schedulerAt(1), schedulerAt(2))
        }

fun testSchedulersContractForAll(): SchedulersContract =
        TestScheduler().run {
            SchedulersContract.create(this, this, this)
        }

fun testSchedulersContractTrampolineAll(): SchedulersContract =
        SchedulersContract.create(Schedulers.trampoline(), Schedulers.trampoline(), Schedulers.trampoline())

fun <T> identityMap(vararg items: T): Map<T, T> = mutableMapOf<T, T>().apply {
    items.forEach { put(it, it) }
}