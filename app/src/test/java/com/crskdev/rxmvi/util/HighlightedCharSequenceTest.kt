package com.crskdev.rxmvi.util

import com.crskdev.rxmvi.util.HighlightedCharSequence.Highlight
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it

/**
 * Created by Cristian Pela on 19.04.2018.
 */
class HighlightedCharSequenceTest : Spek({
    describe("testing highlighting portions in a char sequence") {
        it("should return a list of correct highlights") {
            SimpleHighlightedCharSequence("Hello World", "World")
                    .highlights().assertEq(listOf(Highlight(6, 11)))
            SimpleHighlightedCharSequence("Hello World", "Foo")
                    .highlights().assertEq(emptyList())
            SimpleHighlightedCharSequence("Hello Herld", "He")
                    .highlights().assertEq(listOf(Highlight(0, 2), Highlight(6, 8)))
            SimpleHighlightedCharSequence("HeHello", "He")
                    .highlights().assertEq(listOf(Highlight(0, 2), Highlight(2, 4)))
            SimpleHighlightedCharSequence("H", "He")
                    .highlights().assertEq(emptyList())
            SimpleHighlightedCharSequence("He", "He")
                    .highlights().assertEq(listOf(Highlight(0, 2)))
            SimpleHighlightedCharSequence("He", "")
                    .highlights().assertEq(emptyList())
            SimpleHighlightedCharSequence("", "")
                    .highlights().assertEq(emptyList())
            SimpleHighlightedCharSequence("He", "Hello")
                    .highlights().assertEq(emptyList())
        }
    }
})