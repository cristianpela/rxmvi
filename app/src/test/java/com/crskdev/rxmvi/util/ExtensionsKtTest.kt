package com.crskdev.rxmvi.util

import com.crskdev.rxmvi.domain.DateFormatter
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Cristian Pela on 02.04.2018.
 */
class ExtensionsKtTest : Spek({
    describe("testing extension functions") {
        describe("testing timeFrom") {
            val dateFormat = SimpleDateFormat(DateFormatter.FORMAT, Locale.getDefault())
            val timeProvider = MockedTimeProvider(2018, 4, 2, 14, 0)
            it("should have the right hour and minute") {
                timeFrom(2018, 4, 15, timeProvider)
                        .let { dateFormat.format(it) }
                        .assertEq("2018-04-15 14:00")
            }
        }
    }
})