package com.crskdev.rxmvi.util

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Controller
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.crskdev.rxmvi.platform.app.conductor.controller.di.ControllerComponent
import com.crskdev.rxmvi.platform.app.conductor.ControllerInjector
import com.crskdev.rxmvi.platform.app.conductor.HasControllerInjector
import com.crskdev.rxmvi.presentation.aggregate.AggregatorConductorController
import com.crskdev.rxmvi.presentation.upsert.edit.EditTODOConductorController
import com.crskdev.rxmvi.presentation.list.TODOListConductorController
import com.crskdev.rxmvi.presentation.search.TODOSearchConductorController
import com.crskdev.rxmvi.presentation.upsert.add.AddTODOConductorController


/**
 * .
 * Created by Cristian Pela on 18.03.2018.
 */
class TestTODOConductorActivity : AppCompatActivity(), HasControllerInjector {

    private lateinit var router: Router

    private lateinit var controllerContainer: ViewGroup

    private var savedInstanceState: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        controllerContainer = LinearLayout(this).apply { id = 1 }
        setContentView(controllerContainer)
        this.savedInstanceState = savedInstanceState
    }

    fun provideController(controller: Controller) {
        router = Conductor.attachRouter(this, controllerContainer, savedInstanceState)
        if (!router.hasRootController()) {
            router.setRoot(RouterTransaction.with(controller))
        }
    }


    override fun onBackPressed() {
        if (!router.handleBack()) {
            super.onBackPressed()
        }
    }

    /**
     * stubbing the ControllerInjector for test environment
     */
    override fun getControllerInjector(): ControllerInjector =
            object : ControllerInjector {
                override fun component(): ControllerComponent =

                        object : ControllerComponent {
                            override fun inject(controller: AddTODOConductorController) = Unit

                            override fun inject(controller: AggregatorConductorController) = Unit

                            override fun inject(controller: TODOListConductorController) = Unit

                            override fun inject(controller: EditTODOConductorController) = Unit

                            override fun inject(controller: TODOSearchConductorController) = Unit

                        }

            }

}
